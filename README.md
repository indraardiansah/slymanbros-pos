# pos slymanbros frontend

> My impeccable Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

For new project / cloning, please create `nuxt.config.js` & `package.json`. Copy all content from `nuxt.config.js.example` & `package.json.example` into a new file. 

## Git Flow
There is 4 branchs:

1. `Master`: Production code
2. `Staging`: Pre-Production (develop a new feature) code
3. `develop/username/feature`: new feature before merge into staging
4. `hotfix/username/bug-name`: bug fixing before merge into master & staging

## Branch Rule

### Branch Name Format: develop/taskcode-taskname or hotfix/taskcode-taskname. Example: develop/3341-Dashboard

After staging merge into master, do not editing directly on branch staging or master. 

### Create branch `develop/xx/xx` from `staging`. 

```
$ git checkout staging
$ git checkout -b develop/xx/xx
```

### Create branch `hotfix/xx/xx` from `master`

```
$ git checkout master
$ git checkout -b hotfix/xx/xx
```

## Branch Flow

1. Do not merge new feature branch into staging. But, if you want to create new branch for new feature, make it from staging `git checkout -b develop/taskcode-taskname`.
2. Do not merge development into staging or master.
3. Merge branch of new feature into development but do not close branch. 