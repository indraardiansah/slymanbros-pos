export default function (context) {
    if (context.app.$auth.$state.user != null && !context.app.$auth.$state.user.is_admin) {
        let route_name = context.route.name
        if (route_name.includes('admin')) {
            return context.redirect('/')
        }
    }
    return
}