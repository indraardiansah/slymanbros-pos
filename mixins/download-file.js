export default {
    methods: {
        downloadFile(payload) {
            this.$axios
                .get(payload.url, {
                    responseType: "blob", // important
                })
                .then((response) => {
                    const url = window.URL.createObjectURL(
                        new Blob([response.data])
                    );
                    const link = document.createElement("a");
                    link.href = url;
                    link.setAttribute("download", payload.name);
                    document.body.appendChild(link);
                    link.click();
                });
        },
    }
}
