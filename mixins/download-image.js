export default {
    methods: {
        downloadImage(payload) {
            this.$store.dispatch("order/downloadImage", payload);
        },
    }
}
