const pkg = require('./package')

module.exports = {
	mode: 'universal',

	/*
	** Headers of the page
	*/
	head: {
		title: 'POS - Slyman Brothers Appliance Centers of Missouri',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: pkg.description }
		],
		link: [
			{ rel: 'apple-touch-icon', sizes:"180x180", href: '/favicon/apple-touch-icon.png' },
			{ rel: 'icon', type:"image/png", sizes:"32x32", href: '/favicon/favicon-32x32.png' },
			{ rel: 'icon', type:"image/png", sizes:"16x16", href: '/favicon/favicon-16x16.png' },
			{ rel: 'manifest', href: '/favicon/site.webmanifest' }
		],
		script: [
			{ src: '/js/userback.js?version=1' },
			{ src: '/js/qrcode.min.js' },
			{ src: '/js/JsBarcode.all.min.js' },
			{ src: '/js/oms.js' },
		]
	},

	server: {
		port: process.env.NUXT_PORT,
	},

	subFolders: true,
	loading: {
		color: '#1d86fc',
		height: '3px'
	},

	/*
	** Global CSS
	*/
	css: [
		'./assets/style/app.scss'
	],
	/*
	** Plugins to load before mounting the App
	*/
	plugins: [
		'./plugins/global.js',
		'./plugins/directive.js',
		'./plugins/axios.js',
		{
            src: './plugins/vue-carousel.js',
            ssr: false,
        },
		{
            src: './plugins/vue-toggle-button.js',
            ssr: false,
        },
		{
			src: '~/plugins/laravel-echo.js', // disable while chat in progress to avoid error causes different pusher key
			ssr: false
		},
		{
			src: '~/plugins/vue-select',
			ssr: false
		},
		{
			src: '~/plugins/chart',
			ssr: false
		},
		{
			src: '~/plugins/lazyload',
			ssr: false
		},
		{
			src: '~/plugins/v-calendar',
			ssr: false
		}
	],

	/*
	** Nuxt.js modules
	*/
	modules: [
		'@nuxtjs/axios',
		'bootstrap-vue/nuxt',
		'@nuxtjs/auth',
		'@nuxtjs/laravel-echo',
		'nuxt-basic-auth-module',
		'@nuxtjs/sentry'
	],
	buildModules: [
		['@nuxtjs/dotenv']
	],
	basic: {
        name: process.env.BASIC_NAME,
        pass: process.env.BASIC_PASS,
        enabled: process.env.BASIC_ENABLED === 'true'
	},
	echo: {
		authModule: true,
		connectOnLogin: true,
		disconnectOnLogout: true
	},
	auth: {
		redirect: {
            logout: '/' ,
            login: '/login',
            home: '/'
        },
		strategies: {
			local: {
				endpoints: {
					login: { url: 'login', method: 'post', propertyName: '' },
					logout: { url: 'logout', method: 'post' },
					user: { url: 'user/store', method: 'get', propertyName: '' }
				},
				tokenType: 'Bearer',
			}
		},
		cookie: {
			prefix: 'auth.',
			options: {
				path: '/',
				expires: 1
			}
		},
		resetOnError: true
	},
	router: {
		middleware: [
			'auth',
		]
	},
	/*
	** Axios module configuration
	*/
	axios: {
		baseURL: process.env.BASE_URL,
		progress: true
	},

	/*
	** Build configuration
	*/
	build: {
		/*
		** You can extend webpack config here
		*/
		extend(config, ctx) {

		},
		extractCSS: {
            ignoreOrder: true
        },
		cssSourceMap: false
	}
}
