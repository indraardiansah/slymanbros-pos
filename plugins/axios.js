export default function ({ $axios, store, route, $sentry }) {
    $axios.setHeader('Content-Type', 'application/json')
    
    $axios.setToken(store.state.api_token, 'Bearer')

    $axios.onRequest(config => {
        let url = typeof config.url != 'undefined' ? config.url:''
        let explode = url.split('?')
        if(url.includes('chat') || url.includes('user')) store.commit('SET_LOADING_TABLE', false)
        else store.commit('SET_LOADING_TABLE', true)
    })

    $axios.onResponse(config => {
        let url = typeof config.url != 'undefined' ? config.url:''
        let explode = url.split('?')
        if (explode[0] != '/product/order' && url != '/customer/matched' && url != '/search'  && url != '/user/employees/sales-person')  {
            store.commit('SET_LOADING_TABLE', false)
        }
    })

    $axios.onError(error => {
        store.commit('SET_LOADING_TABLE', false)
        if(error.response.status != 401){
            $sentry.captureException(error)
        }
    })
}