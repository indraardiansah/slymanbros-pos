import Vue from 'vue'
import { Line, Pie } from 'vue-chartjs'

Vue.component('line-chart-esl', {
    extends: Line,
    props: ['chartData', 'options'],
    mounted () {
        this.renderChart(this.chartData, {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                mode: 'x-axis'
            },
        })
    }
})

Vue.component('pie-chart-esl', {
    extends: Pie,
    props: ['chartData', 'options'],
    mounted () {
        this.renderChart(this.chartData, {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,
            }             
        })
    }
})

