import Vue from 'vue'
import {
  Line
} from 'vue-chartjs'

Vue.component('line-chart', {
  extends: Line,
  props: ['chartData', 'options'],
  data() {
    return {
      convertedLables: [],
      convertedValues: [],
      gradient: null
    }
  },
  watch:{
      chartData: {
        handler: function(newValue) {
            let vm = this
            vm.clearData().then(() =>{
                vm.convertData(newValue)
            })
        },
        deep: true
    },
    convertedValues(){      
      this.loadChart()
    },
  },
  mounted() {
    this.convertData(this.chartData)
    this.loadChart()
  },
  methods:{
    loadChart(){
      this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450)
      this.gradient.addColorStop(0, 'rgba(13, 95, 181, 0.9)')
      this.gradient.addColorStop(0.5, 'rgba(13, 95, 181, 0.5)');
      this.gradient.addColorStop(1, 'rgba(13, 95, 181, 0)');
      this.renderChart({
        labels: this.convertedLables,
        datasets: [{
            label: 'Data One',
            borderColor: '#0d5fb5',
            pointBackgroundColor: 'white',
            pointRadius: 4,
            borderWidth: 2,
            datasetStrokeWidth: 3,
            pointDotStrokeWidth: 4,
            pointBorderColor: '#0d5fb5',
            backgroundColor: this.gradient,
            data: this.convertedValues
        }]
      }, {
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          tooltips: {
            callbacks: {
              label: function (tooltipItem) {
                return '$' + _.toNumber(tooltipItem.yLabel).toLocaleString(undefined, {minimumFractionDigits: 2});
              }
            }
          },
          scales: {
            yAxes: [{
                ticks: {
                    callback: function(value, index, values) {
                        return '$' + _.toNumber(value).toLocaleString(undefined, {minimumFractionDigits: 2});
                    },
                    autoSkip: true,
                    maxTicksLimit: 8
                },
            }]
        }
      })
    },
    clearData(){
      return new Promise(resolve =>{
        this.convertedLables = []
        this.convertedValues = []
        resolve()
      })
    },
    convertData(data){
      return new Promise(resolve =>{
          if(this.convertedLables.length == 0 && this.convertedValues.length == 0){
            data.forEach(item => {
                item.month ? this.convertedLables.push(item.month) : this.convertedLables.push(item.date)
                this.convertedValues.push(item.value)
            })
          }
          resolve() 
      })
    }
  }
})


