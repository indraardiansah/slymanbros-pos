import Vue from 'vue'

Vue.directive('width', {
    bind(el, binding, vnode) {
        el.style.width = binding.value
    }
})

Vue.directive('height', {
    bind(el, binding, vnode) {
        el.style.height = binding.value
    }
})

Vue.directive('money', {
    bind(el, binding, vnode) {
        el.innerText = '$' + _.toNumber(el.innerText).toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
    },
    componentUpdated(el, binding, vnode) {
        if (!_.includes(el.innerText, '$')) {
            el.innerText = '$' + _.toNumber(el.innerText).toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
        }
    },
})

Vue.directive('uppercase', {
    bind(el, binding, vnode) {
        el.innerText = el.innerText.toUpperCase()
    },
    componentUpdated(el, binding, vnode) {
        el.innerText = el.innerText.toUpperCase()
    }
})

Vue.directive('bg', {
    bind(el, binding, vnode) {
        el.style.backgroundColor = binding.value
    }
})

Vue.directive('color', {
    bind(el, binding, vnode) {
        el.style.color = binding.value
    }
})

let handleOutsideClick
Vue.directive('closable', {
    bind(el, binding, vnode) {
        handleOutsideClick = (e) => {
            e.stopPropagation()
            const { handler, exclude } = binding.value
            let clickedOnExcludedEl = false
            exclude.forEach(refName => {
                if (!clickedOnExcludedEl) {
                    const excludedEl = vnode.context.$refs[refName]
                    clickedOnExcludedEl = excludedEl.contains(e.target)
                }
            })
            if (!el.contains(e.target) && !clickedOnExcludedEl) {
                vnode.context[handler]()
            }
        }
        document.addEventListener('click', handleOutsideClick)
        document.addEventListener('touchstart', handleOutsideClick)
    },

    unbind() {
        document.removeEventListener('click', handleOutsideClick)
        document.removeEventListener('touchstart', handleOutsideClick)
    }
})


Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
            // here I check that click was outside the el and his childrens
            if (!(el == event.target || el.contains(event.target))) {
                // and if it did, call method provided in attribute value
                vnode.context[binding.expression](event);
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
    },
});
