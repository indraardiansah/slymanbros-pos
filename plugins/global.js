import Vue from 'vue'
import VeeValidate from 'vee-validate'
import 'element-ui/lib/theme-chalk/index.css';
import { DatePicker, TimePicker } from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import * as VueGoogleMaps from '@/node_modules/vue2-google-maps'

locale.use(lang)

Vue.use(DatePicker)
Vue.use(TimePicker)

Vue.use(VeeValidate, {
    fieldsBagName: 'vvFields'
 });

Vue.prototype._ = require('lodash')
Vue.prototype.moment = require('moment')

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAKdBSA5fDXNwl3vdTh4ngLZUlH3U5pwpw',
        libraries: 'geometry,places',
        language: 'en'
    },
})

Vue.filter('truncate', function (text, stop, clamp) {
    return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
})
Vue.filter('prettyBytes', function (num) {
    if (typeof num !== 'number' || isNaN(num)) {
      throw new TypeError('Expected a number');
    }

    var exponent;
    var unit;
    var neg = num < 0;
    var units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    if (neg) {
      num = -num;
    }

    if (num < 1) {
      return (neg ? '-' : '') + num + ' B';
    }

    exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
    num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
    unit = units[exponent];

    return (neg ? '-' : '') + num + ' ' + unit;
});

Vue.mixin({
    filters: {
        lineBreak(value){
            return value.replace(/↵/, '\n');
        }
    },
    methods: {
        getIconCode(name){
            let element = document.createElement('i')
            element.className = name
            document.body.appendChild(element)

            const char = window.getComputedStyle(
                element, ':before'
            ).content.replace(/^"|"$/g, '');

            element.remove()
            return char
        },
        addSvg(data, svg){
            Object.values(data.items).forEach(item => {
                let textNode, element;

                if(data.type == 'grapcodes' && item.type == 'qr') element = QRCode(item[data.content]);
                else element = document.createElementNS('http://www.w3.org/2000/svg', data.dynamic ? item[data.built] : data.built);

                if(data.built && data.built == 'text') textNode = document.createTextNode(item[data.content] != undefined ? item[data.content] : '');

                for(const index in item) {

                    if(data.type && data.type === 'icon') element.setAttributeNS(null, 'font-family', 'IcoFont');
                    if(data.type && data.type === 'photos') element.setAttributeNS(null, 'href', 'data:image/png;base64,' + item[index]);
                    if(data.type && data.type === 'barcode') element.setAttributeNS(null, 'href', item['content']);
                    if(index === 'bold' && item[index]) element.setAttributeNS(null, 'font-weight', 'bold');
                    else if(index === 'color') element.setAttributeNS(null, 'fill', item[index]);
                    else if(data.type && data.type === 'shapes') {
                        if(index === 'x1') element.setAttributeNS(null, 'x', Math.round(item[index]) );
                        else if(index === 'y1') element.setAttributeNS(null, 'y', Math.round(item[index]) );
                        else if(index === 'x2') element.setAttributeNS(null, 'width', Math.round(item[index]) );
                        else if(index === 'y2') element.setAttributeNS(null, 'height', Math.round(item[index]) );
                    }
                    else element.setAttributeNS(null, index.replace(/([A-Z])/g, '-$1').toLowerCase(), item[index]);
                }

                if(data.built && data.built == 'text') element.appendChild(textNode);

                svg.appendChild(element);
            });
        },
        generateSvg(data, element){
            const {size, ...rest} = data

            const svg = document.createElementNS('http://www.w3.org/2000/svg', "svg")
            svg.setAttribute('width', data.size.width)
            svg.setAttribute('height', data.size.height)
            let dataSvg = []
            Object.entries(rest).map(([key, value]) => {
                if(key === 'grapcodes'){
                    Object.entries(value).forEach(([keyGrap, valueGrap]) => {
                        if(keyGrap === 'qrcode'){
                            let data = {
                                items:  {
                                    qrcode: valueGrap
                                },
                                type: 'grapcodes',
                                content: 'content'
                            }
                            dataSvg.push(data)
                        }
                        if(keyGrap === 'barcode'){
                            let data = {
                                items:  {
                                    barcode: valueGrap
                                },
                                type: 'barcode',
                                built: 'image',
                                content: 'content'
                            }
                            dataSvg.push(data)
                        }
                    })
                }else{
                    let data = {
                        items: value,
                        type: key === 'icons' ? 'icon': key,
                        built: key === 'shapes' ? 'spec' : key === 'icons' ? 'text' : key === 'photos' ? 'image' : key,
                        content: 'content'
                    }
                    if(key == 'shapes'){
                        data['dynamic'] = true
                    }
                    dataSvg.push(data)
                }
            })
            let sortedData = _.sortBy(dataSvg, [function(o) { return o.type != 'shapes' }]);
            sortedData.forEach(v => {
                this.addSvg({...v}, svg)
            })
            element.appendChild(svg);
        },
        textToBase64Barcode(text){
            var canvas = document.createElement("canvas");
            JsBarcode(canvas, text, {
                displayValue: false,
                width: 2,
                height: 40,
                margin: 0,
            });
            return canvas.toDataURL("image/png");
        },
        generateFontSize(fontSize){
            let generatedFont = (fontSize * 4) / 3.2
            return Math.round(generatedFont)
        },
        isNumber($event) {
            $event = ($event) ? $event : window.event;
            var charCode = ($event.which) ? $event.which : $event.keyCode;
            if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
                $event.preventDefault();
            } else {
                return true;
            }
        },
        getAddress($event) {
            let address_components = $event.address_components
            let index_postal = _.findIndex(address_components, function(val) {
                return val.types[0] == 'postal_code'
            })
            let index_number = _.findIndex(address_components, function(val) {
                return val.types[0] == 'street_number'
            })
            let index_street = _.findIndex(address_components, function(val) {
                return val.types[0] == 'route'
            })
            let index_city = _.findIndex(address_components, function(val) {
                // return val.types[0] == 'administrative_area_level_2'
                return val.types[0] == 'locality'
            })
            if (index_city == -1){
                index_city = _.findIndex(address_components, function(val) {
                    return val.types[0] == 'neighborhood'
                })
            }
            let index_state = _.findIndex(address_components, function(val) {
                return val.types[0] == 'administrative_area_level_1'
            })
            let index_country = _.findIndex(address_components, function(val) {
                return val.types[0] == 'country'
            })

            let formatted_address = $event.formatted_address

            return {
                number: index_number == -1 ? '' : address_components[index_number].long_name,
                street: index_street == -1 ? '' : address_components[index_street].short_name,
                city: index_city == -1 ? '' : address_components[index_city].long_name,
                state: index_state == -1 ? '' : address_components[index_state].long_name,
                postal_code: index_postal == -1 ? '' : address_components[index_postal].long_name,
                country: index_country == -1 ? '' : address_components[index_country].long_name,
                formatted_address: formatted_address
            }
        },
        initialIntercom(){
            if(this.$auth.loggedIn){
                let auth = this.$auth.$state.user
                let timestamp = new Date(this.$auth.$state.user.last_login).getTime() / 1000

                window.intercomSettings = {
                    app_id: "vzzyua92",
                    name: auth.fullname,
                    email: auth.email,
                    created_at: timestamp
                }
            }
        },
        watchPermissionUpdate(){
            if(this.$auth.loggedIn){
                Echo.private(`permission-update-${this.$auth.user.user_id}`)
                .listen('PermissionUpdate', (e) => {
                this.$auth.fetchUser()
                })
            }
        },
        async initialAnnouncement(){
            if(this.$auth.loggedIn){
                let auth = this.$auth.$state.user
                if(auth.pop_up_annoucement == 1 && this.$store.state.announcement.popup_data.length == 0){
                    await this.$store.dispatch('announcement/getAnnouncements', {
                        perpage : 10,
                        type: 'all',
                        pop_up: 1
                    })
                }
            }
        },
        checkNetworkStatus(){
            window.addEventListener('offline', () => {
                this.$store.commit('SET_OFFLINE', true)
            })
            window.addEventListener('online', () => {
                this.$store.commit('SET_OFFLINE', false)
            })
        }
    }
})

const ignoreWarnMessage = 'The .native modifier for v-on is only valid on components but it was used on <a>.'
Vue.config.warnHandler = function (msg, vm, trace) {
  if (msg === ignoreWarnMessage) {
    msg = null;
    vm = null;
    trace = null;
  }
}
