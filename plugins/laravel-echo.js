import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');
let getToken = localStorage.getItem('auth._token.local') 
let token = getToken ? localStorage.getItem('auth._token.local').split(' '):[]
let setToken = token.length > 0 ? token[1]:''

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '718e5a967ff59aa4595c',
    cluster: 'us2',
    encrypted: true,
    authEndpoint:  process.env.PUSHER_ENDPOINT,
    auth: {
        headers: {
            Authorization: `Bearer ${setToken}`
        },
    },
});

export default(context, inject) => {
    let user_id = context.nuxtState.state.auth.user === null ? "" :context.nuxtState.state.auth.user.user_id
    window.Echo.private('report')
    .listen('MessageSent', (e) => {
        context.app.store.commit('liveFeed/ADD_NEW_DATA', e.data)
        context.app.store.commit('liveFeed/SET_NEW_ITEM_COUNT', 1)
    })
    window.Echo.private('message-center-' + user_id)
    .listen("MessageCenter", (e) => {
        context.app.store.state.notification.data.counter = e.counter
    })    
    window.Echo.private('progress-rws')
    .listen('ProgressCrawl', (e) => {
        context.app.store.commit('rws/UPDATE_PROGRESS_DATA', e.data)
    })
    // window.Echo.channel('user-chat-list-' + user_id)
    // .listen("UserChatList", (e) => {
    //     e.data.data.forEach(id => {
    //         if(id != user_id){
    //             context.app.store.dispatch('chat/getConversation', {
    //                 sender_id: user_id,
    //                 receiver_id: id,
    //                 page: 1,
    //                 with_conv: false
    //             })
    //         }
    //     });
    //     context.app.store.dispatch('chat/getChatList', {
    //         id: context.app.$auth.state.user.user_id,
    //         q: ''
    //     })
    // })
}