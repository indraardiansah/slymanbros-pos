import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: '/placeholder-images-error.jpg',
    loading: '/placeholder-images-background.jpg',
    attempt: 1
})
