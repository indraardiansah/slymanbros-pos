import Vue from 'vue'
import { setupCalendar, DatePicker } from 'v-calendar'

setupCalendar({
    datePicker: {
        popover: {
            visibility: 'click'
        }
    }
})

Vue.component('app-datepicker', DatePicker)
