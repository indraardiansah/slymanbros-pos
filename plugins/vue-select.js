import Vue from 'vue'
import vSelect from 'vue-select'
import MonthPicker from "vuejs-datepicker/dist/vuejs-datepicker.esm.js"

Vue.component('v-select', vSelect)
Vue.component('app-monthpicker', MonthPicker)