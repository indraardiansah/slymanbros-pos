let url = window.location.href

Userback = window.Userback || {};
let check_api_token = url.includes('slymanbros.slymanmedia.com') ? '7871|12949|yfnQMpl8IxkqO2cRRLNYqEQhDjgzRWqsQnfYgv6833gConJH0j':'7871|12913|yfnQMpl8IxkqO2cRRLNYqEQhDjgzRWqsQnfYgv6833gConJH0j';
Userback.access_token = check_api_token;

(function(id) {
  var s = document.createElement('script');
  s.async = 1;s.src = 'https://static.userback.io/widget/v1.js';
  var parent_node = document.head || document.body;parent_node.appendChild(s);
})('userback-sdk');

var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    if(mutation.addedNodes.length){
      if(mutation.addedNodes[0].classList && mutation.addedNodes[0].classList.contains('userback-button-container')){
        let buttonIcon = mutation.addedNodes[0].querySelector('ubdiv.userback-button > svg')
        if(buttonIcon){
          buttonIcon.style.position = 'relative'
          buttonIcon.style.zIndex = '-1'
        }
        mutation.addedNodes[0].querySelector('ubdiv.userback-button').addEventListener('click', (e) => {
          let email = localStorage.getItem('email-login')
          let form = e.target.nextSibling.querySelector('.userback-controls-options')
          let title = form.querySelector('input[placeholder="Your name"]')
          let emailForm = form.querySelector('input[name="email"]')
          title.placeholder = 'Your Feedback Title'
          title.value = ''
          if(email) emailForm.setAttribute("disabled", "")
          emailForm.value = email || null
        })
      }
    }
  });
});
 
document.addEventListener('DOMContentLoaded', () => {
  observer.observe(document.body, {
    childList: true,
    subtree: true,
    attributes: false,
    characterData: false
  })
});