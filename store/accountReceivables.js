export const state = () => ({
    fields: [
        {
            key: 'customer_name',
            label: 'Customer Name'
        },
        {
            key: 'sales_person',
            label: 'Sales Person'
        },
        {
            key: 'store',
            label: 'Store'
        },
        {
            key: 'invoice',
            label: 'Invoice'
        },
        {
            key: 'invoice_date',
            label: 'Invoice Date'
        },
        {
            key: 'balance_due',
            label: 'Current'
        },
        {
            key: 'firstMonth',
            label: '1-30'
        },
        {
            key: 'secondMonth',
            label: '31-60'
        },
        {
            key: 'thirdMonth',
            label: '61-90'
        },
        {
            key: 'moreMonths',
            label: 'Over 90'
        },
        {
            key: 'total',
            label: 'Total'
        },
        {
            key: 'status',
            label: 'Status'
        },
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    perpage: 10,
})

export const mutations = {
    SET_ACCOUNT_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_ACCOUNT_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_ACCOUNT_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_ACCOUNT_PAGE(state, payload) {
        state.page = payload
    },
    SET_ACCOUNT_PERPAGE(state, payload) {
        state.perpage = payload
    },
}

export const actions = {
    GET_ACCOUNT_RECEIVABLE({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/account-receivable-aging?page=${state.page}&perpage=${state.perpage}`,{
                params: payload
            })
            .then(response => {
                commit('SET_ACCOUNT_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
                reject(error.response)
            })
        })
        
    }
}