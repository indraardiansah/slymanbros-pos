export const state = () => ({
    item_daily: [],
    item_sell: [],
    item_tax: [],
    item_sales_by_product: [],
    item_profit_report: [],
    item_install_report: [],
    item_warranty_report: [],
    item_warranty_epic_report: [],
    item_summary_report: [],
    item_revenue_report: [],
    item_jon_report: [],
    item_part_report: [],
    item_damage_allowance_report: [],
    item_outstanding_orders: [],
    item_rebate: [],
    field_jon_report: ['date', { key: 'quote_no', label: 'Quote Number' }, 'deposit_amount', { key: 'total', label: 'Quote Total' }],
    field_daily: ['date_paid_in_full', 'order_number', 'sales_person', 'location', 'subtotal', 'tax', 'total'],
    field_sell: [{ key: 'product_model', thStyle: { width: '200px' } }, 'brand', 'order_no', 'order_date', 'customer_name', 'customer_zip', 'quantity'],
    field_tax: [{ key: 'order_number' }, { key: 'order_date' }, 'total_revenue', 'gross_receipt', 'adjustments', 'taxable_sales', 'tax_rate', 'amount_of_tax'],
    field_sales_by_product: ['date_sold', 'order_number', 'brand', 'model_number', 'store_location', 'sales_person', 'quantity', 'revenue', 'delivery_date'],
    field_profit_report: [{ key: 'date_sold', sortable: true }, { key: 'order_number', sortable: true }, { key: 'brand', sortable: true }, { key: 'model_number', sortable: true }, { key: 'store_location', sortable: true }, { key: 'sales_person', sortable: true }, { key: 'quantity', sortable: true }, { key: 'cost', thStyle: { width: '200px' }, sortable: true }, { key: 'price', sortable: true }, { key: 'total_price', sortable: true }, { key: 'profit_margin', sortable: true }],
    field_profit_child_report: ['brand', 'model_number', 'quantity'],
    field_install_report: ['order_date', 'order_number', 'install_name', 'install_price', 'model_number', 'store_location', 'sales_person', 'customer_name', 'delivery_method', 'delivery_date'],
    field_warranty_report: [{ key: 'order_date', thStyle: { width: '110px' } }, { key: 'order_number', thStyle: { width: '135px' } }, { key: 'warranty_name', thStyle: { width: '250px' } }, 'cost', { key: 'warranty_price', thStyle: { width: '140px' } }, 'profit', { key: 'model_number', thStyle: { width: '180px' } }, { key: 'store_location', thStyle: { width: '180px' } }, 'sales_person', 'customer_name', 'delivery_date'],
    field_warranty_epic_report: ['no', { key: 'formatted_delivery_date', label: 'Delivery Date' }, { key: 'formatted_order_number', label: 'Dealer Gen 1' }, 'customer_name', { key: 'sku', label: 'Model Number' }, 'warranty_code', { key: 'warranty_price', label: 'Total Warranty Price' }, 'product_description', { key: 'store_location', label: 'Location' }, { key: 'sales_person', label: 'Sales Clerk' }],
    field_damage_allowance_report: [
        {
            key: 'order_no',
            label: 'Order #',
            filter: ['open', 'resolved'],
        },
        {
            key: 'product_model',
            label: "Model",
            filter: ['open', 'resolved'],
        },
        {
            key: 'serial_number',
            filter: ['open', 'resolved'],
        },
        {
            key: 'description',
            label: 'Description',
            filter: ['open', 'resolved'],
        },
        {
            key: 'amount',
            filter: ['open', 'resolved'],
        },
        {
            key: 'amount_received',
            filter: ['open'],
        },
        {
            key: 'driver_name',
            filter: ['open', 'resolved'],
        },
        {
            key: 'date_accepted_formatted',
            label: 'Date Accepted',
            filter: ['open', 'resolved'],
        },
        {
            key: 'notes',
            label: 'Notes',
            thStyle: {
                width: '15%',
            },
            filter: ['open', 'resolved'],
        },
        {
            key: 'status',
            thStyle: {
                width: '90px',
            },
            filter: ['open', 'resolved'],
        }
    ],
    field_part_report: [{ key: 'order_date_formatted', label: 'Ordered Date' }, { key: 'customer_name', label: 'Customer' }, { key: 'order_no', label: "Order Number" }, { key: 'product_model', label: 'Product' }, 'description', { key: 'quantity', label: "Qty" }, { key: 'part_number', label: "Part#" }, { key: 'order_by', label: 'Ordered By' }, 'confirmation_number', { key: 'date_delivered_formatted', label: "Date Delivered to Customer" }],
    field_outstanding_orders: [{ key: 'order_date', sortable: true }, { key: 'order_no', sortable: true }, { key: 'sales_person', sortable: true }, { key: 'customer_name', sortable: true }, { key: 'store_name', label: 'Location', sortable: true }, { key: 'model_number', sortable: true }, { key: 'quantity', sortable: true }, { key: 'price', sortable: true }, { key: 'total', label: 'Total Price', sortable: true }, { key: 'cost', sortable: true }, { key: 'total_cost', sortable: true }],
    field_rebate: [{key: 'order_date'}, {key: 'order_no', label: 'Order #'}, {key: 'customer_name'}, {key: 'rebate_name', label: 'Rebate'}, {key: 'amount', label: 'Rebate Amount'}],
    // field_summary_report: ['order_date', 'order_detail.', 'revenue', 'tax' , 'item_total'],
    field_summary_report: [
        {
            key: 'order_date'
        },
        {
            key: 'total_qty',
            label: 'Qty'
        },
        {
            key: 'revenue',
            label: 'Revenue'
        },
        {
            key: 'return',
            label: 'Return'
        },
        {
            key: 'total_revenue',
            label: 'Revenue Total'
        },
        {
            key: 'tax_price',
            label: 'Tax'
        },
        {
            key: 'total',
            label: 'Item Total'
        }
    ],
    total_summary_report: null,
    field_revenue_report: ['order_date', 'order_number', 'sales_person', 'location', 'customer', 'revenue', 'note'],
    meta_outstanding_orders: null,
    outstanding_orders_total : {
        total_price: 0,
        total_cost: 0
    },
    meta_sell: null,
    selected: null,
    pdfUrl: null,
    floor_model: null,
    raw_sell: null,
    meta_rebate: {
        total: null,
        current_page: null,
        per_page: null,
        last_page: null,
    }
})

export const mutations = {
    ASSIGN_DAILY_SALES(state, payload) {
        state.item_daily = payload
    },
    ASSIGN_SELL(state, payload) {
        const {data, ...meta} = payload
        state.item_sell = data
        state.meta_sell = meta
    },
    ASSIGN_SELL_RAW(state, payload) {
        state.raw_sell = payload
    },
    ASSIGN_JON_SLYMAN_REPORT(state, payload) {
        state.item_jon_report = payload
    },
    ASSIGN_TAX(state, payload) {
        state.item_tax = payload
    },
    ASSIGN_SALES_BY_PRODUCT(state, payload) {
        state.item_sales_by_product = payload
    },
    ASSIGN_PROFIT_REPORT(state, payload) {
        state.item_profit_report = payload
    },
    ASSIGN_INSTALL_REPORT(state, payload) {
        state.item_install_report = payload
    },
    ASSIGN_WARRANTY_REPORT(state, payload) {
        state.item_warranty_report = payload
    },
    ASSIGN_WARRANTY_EPIC_REPORT(state, payload) {
        state.item_warranty_epic_report = payload
    },
    ASSIGN_PART_REPORT(state, payload) {
        state.item_part_report = payload
    },
    ASSIGN_DAMAGE_ALLOWANCE_REPORT(state, payload) {
        state.item_damage_allowance_report = payload
    },
    ASSIGN_SUMMARY_REPORT(state, payload) {
        state.item_summary_report = payload
    },
    SET_TOTAL_SUMMARY(state, payload) {
        state.total_summary_report = payload
    },
    SET_TAX_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_DAILY_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_PDF_URL(state, payload) {
        state.pdfUrl = payload
    },
    SET_REVENUE_FIELD(state, payload) {
        state.field_revenue_report = payload
    },
    ASSIGN_REVENUE_REPORT(state, payload) {
        state.item_revenue_report = payload
    },
    ASSIGN_OUTSTANDING_ORDERS(state, payload) {
        state.item_outstanding_orders = payload.data
        state.meta_outstanding_orders = payload.meta
    },
    ASSIGN_TOTAL_OUTSTANDING_ORDERS(state, payload){
        state.outstanding_orders_total.total_price = payload.data.total_price
        state.outstanding_orders_total.total_cost = payload.data.total_cost
    },
    SET_FLOOR_MODEL_DATA(state, payload) {
        state.floor_model = payload
    },
    SET_DAMAGE_ALLOWANCE_REPORT_EACH_KEY(state, payload) {
        state.item_damage_allowance_report.data.data[payload.index][payload.key] = payload.value;
    },
    SET_REBATE_DATA(state, payload){
        state.item_rebate = payload.data
        state.meta_rebate = payload.meta
    }
}

export const actions = {
    getDaily({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/report/daily-sales?from=${payload.from}&to=${payload.to}&sales_person=${payload.user_id}&store=${payload.location}`)
                .then((response) => {
                    commit('ASSIGN_DAILY_SALES', response.data)
                    resolve(response.data)
                })
        })
    },
    getDelivery({ rootState }) {
        return new Promise((resolve, reject) => {
            return this.$axios.post('/order/delivery-method', '')
                .then((response) => {
                    resolve(response.data)
                })
        })
    },
    getSellThroughs({ commit }, payload) {
        console.log(payload)
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/sell-throughs`, { params: payload })
                .then((response) => {
                    commit('ASSIGN_SELL', response.data)
                    resolve(response.data)
                })
        })
    },
    getJonSlymanReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/for-jon-slyman`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_JON_SLYMAN_REPORT', response.data)
                    resolve(response.data)
                })
        })
    },
    getTax({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/tax`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_TAX', response.data)
                    resolve(response.data)
                })
        })
    },
    getSalesByProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/sales-by-product`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_SALES_BY_PRODUCT', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getProfitReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/profit-report`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_PROFIT_REPORT', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    updateCostProfitReport({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.post(`/order/cost/update`, payload)
                .then((response) => {
                    resolve(response.data)
                })
                .catch(error => {
                    new Error(error)
                })
        })
    },
    getInstallReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/install-report`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_INSTALL_REPORT', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getWarrantyReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/warranty-report`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_WARRANTY_REPORT', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getWarrantyEpicReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/warranty-epic-report`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_WARRANTY_EPIC_REPORT', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getRevenueReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/revenue-report`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_REVENUE_REPORT', response.data)
                    commit('SET_REVENUE_FIELD', response.data.field)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getSummary({ commit }, payload) {
        return new Promise((resolve, reject) => {
            // ?page=${payload.page}&per_page=${payload.perpage}&location=${payload.store_id}
            this.$axios.get(`/report/summary`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_SUMMARY_REPORT', response.data.data)
                    commit('SET_TOTAL_SUMMARY', response.data.total)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getPartReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/part-report`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_PART_REPORT', response.data)
                    resolve(response)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getOutstangingOrders({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/outstanding-orders`, {
                params: payload
            })
            .then((response) => {
                if(payload.total) commit('ASSIGN_TOTAL_OUTSTANDING_ORDERS', response.data)
                else commit('ASSIGN_OUTSTANDING_ORDERS', response.data)
                resolve(response)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getDamageAllowanceReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/damage-allowance-report`, {
                params: payload
            })
                .then((response) => {
                    commit('ASSIGN_DAMAGE_ALLOWANCE_REPORT', response.data)
                    resolve(response)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getPDF({ commit }, payload) {
        return new Promise((resolve, reject) => {
            // let url = `from=${payload.from}&to=${payload.to}`
            // if (payload.type == 'tax') {
            //     url = `from=${payload.from}&to=${payload.to}&store_id=${payload.store_id}`
            // } else if (payload.type == 'daily-sales') {
            //     url = `from=${payload.from}&to=${payload.to}&user_id=${payload.user_id}&store=${payload.store_id}`
            // } else if (payload.type == 'account-receivable') {
            //     url = `start=${payload.start}&end=${payload.end}&sales_person=${payload.sales_person}&store=${payload.store}`
            // } else if (payload.type == 'live-feed') {
            //     url = `with_warranty=${payload.with_warranty}`
            //     payload.search ? url += `&search=${payload.search}` : ''
            //     payload.start ? url += `&start=${payload.start}&end=${payload.end}` : ''
            // }

            this.$axios.get(`/report/${payload.type}/get-pdf`, {
                params: payload,
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
            }).then((res) => {
                commit('SET_PDF_URL', res.data.data)
                resolve(res.data)
            })
        })
    },
    getCSV({ commit }, payload) {
        return new Promise((resolve, reject) => {
            // let url = `from=${payload.from}&to=${payload.to}`
            // if (payload.type == 'tax') {
            //     url = `from=${payload.from}&to=${payload.to}&store_id=${payload.store_id}`
            // } else if (payload.type == 'daily-sales') {
            //     url = `from=${payload.from}&to=${payload.to}&user_id=${payload.user_id}&store=${payload.store_id}`
            // } else if (payload.type == 'account-receivable') {
            //     url = `start=${payload.start}&end=${payload.end}&sales_person=${payload.sales_person}&store=${payload.store}`
            // }else if (payload.type == 'live-feed') {
            //     url = `with_warranty=${payload.with_warranty}`
            //     payload.search ? url += `&search=${payload.search}` : ''
            //     payload.start ? url += `&start=${payload.start}&end=${payload.end}` : ''
            // }

            this.$axios.get(`/report/${payload.type}/get-csv`, {
                params: payload,
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
            }).then((res) => {
                commit('SET_PDF_URL', res.data.data)
                resolve(res.data)
            })
        })
    },
    downloadCSV({ commit }, payload) {
        let name = payload.name ? payload.name : 'live-feed'
        return new Promise((resolve, reject) => {
            this.$axios.get(payload.url, {
                responseType: 'blob', // important
            }
            ).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                if (payload.start) {
                    link.setAttribute('download', payload.start + '-' + payload.end + '.csv');
                } else {
                    link.setAttribute('download', name + '.csv');
                }
                document.body.appendChild(link);
                link.click();
            });
        })
    },
    getFloorModel({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('report/floor-model', { params: payload })
                .then(response => {
                    commit('SET_FLOOR_MODEL_DATA', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    new Error(error.response)
                    reject(error.response)
                })
        })
    },
    async getInstantRebateReport({commit}, payload){
        const response = await this.$axios.get('report/instant-rebate', {params: payload})
        commit('SET_REBATE_DATA', response.data)
        return response
    },
    async getInstantRebateList({}) {
        return new Promise((resolve, reject) => {
            this.$axios.get('report/instant-rebate-list')
                .then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    new Error(error.response)
                    reject(error.response)
                })
        })
    }
}
