import { __endingNumber } from "@/utils/ending-number";

export const state = () => ({
    items: {
        accountReceive: null,
        deposit: null,
        inventory: null,
    },
    yearList: [],
})

export const mutations = {
    SET_YEAR_LIST(state, payload) {
        state.yearList = payload;
    },
    SET_ITEMS(state, payload) {
        state.items.deposit = payload.deposit;
        state.items.accountReceive = payload.account_receivable;
        state.items.inventory = payload.inventory;
    }
}

export const actions = {
    getDateList({ commit }, payload) {
        return this.$axios.get('/report/summary-report/date', {
            params: payload
        }).then(({ data }) => {
            const mapEndingnNumber = data.map((date) => {
                return {
                    ...date,
                    monthFormatted: date.formatted.split(' ')[0],
                    dateFormatted: date.formatted.split(' ')[1],
                    endingNumber: __endingNumber(date.formatted)
                }
            })
            commit('SET_YEAR_LIST', mapEndingnNumber);
        }).catch((error) => {
            console.log(error.response);
        })
    },
    getReport({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('/report/summary-report', {
                params: payload
            }).then(({ data }) => {
                commit('SET_ITEMS', data.data);
                resolve(data);
            }).catch((error) => {
                reject(error.response);
            })
        })
    },
    getReportPdf(_, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('/report/summary-report/pdf', {
                params: payload
            }).then(({ data }) => {
                resolve(data);
            }).catch((error) => {
                reject(error.response);
            })
        })
    },
    getReportCsv(_, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('/report/summary-report/csv', {
                params: payload
            }, {
                responseType: 'blob', // important
            }).then(({ data }) => {
                resolve(data);
            }).catch((error) => {
                reject(error.response);
            })
        })
    },
}
