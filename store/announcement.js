export const state = () => ({
    data:  null,
    meta : null,
    page : 1,
    dashboard : [],
    fields : [
        {
            key: 'picture',
        },
        {
            key: 'title',
            label : 'Title',
            thStyle : {
                width : '250px'
            }
        },
        {
            key: 'announcement',
            label : 'Announcement',
            thStyle : {
                width : '400px'
            }
        },
        {
            key: 'start_date',
            label : 'Start Date'
        },
        {
            key: 'end_date',
            label : 'End Date'
        },
        {
            key: 'status',
            label : 'Status'
        },
        {
            key: 'user.fullname',
            label : 'Creator'
        }
    ],
    items: [],
    popup_data: [],
    popup: false
})

export const mutations = {
    SET_ANNOUNCEMENT_DATA(state, payload){
        state.data = payload
    },
    SET_ANNOUNCEMENT_ITEMS(state, payload){
        state.items = payload.data,
        state.meta = {
            current_page : payload.current_page,
            total : payload.total,
            per_page : payload.per_page,
            last_page : payload.last_page
        }
    },
    SET_ANNOUNCEMENT_PAGE(state, payload){
        state.page = payload
    },
    SET_ANNOUNCEMENT_DASHBOARD(state, payload){
        state.dashboard = payload
    },
    SET_ANNOUNCEMENT_POPUP_DATA(state, payload){
        state.popup_data = payload
    },
    SET_ANNOUNCEMENT_POPUP(state, payload){
        state.popup = payload
    }
}

export const actions = {
    postAnnouncement({state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`announcement`, payload)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getAnnouncement({state, commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`announcement/${payload}/edit`)
            .then(response =>{
                commit('SET_ANNOUNCEMENT_DATA', response.data.data)
                resolve(response.data.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getAnnouncements({state, commit}, payload){
        let params = null
        if(payload.type){
            params = {
                type : payload.type,
                pop_up: payload.pop_up
            }
        }else{
            params =  {
                page : state.page,
                perpage : payload.perpage,
                q : payload.q
            }
        }
        return new Promise((resolve, reject) =>{
            this.$axios.get('announcement', {params})
            .then(response =>{
                if(payload.type && payload.pop_up){
                    commit('SET_ANNOUNCEMENT_POPUP_DATA', response.data)
                    if(response.data.data.length > 0) commit('SET_ANNOUNCEMENT_POPUP', true)
                }else if(payload.type){
                    commit('SET_ANNOUNCEMENT_DASHBOARD', response.data)
                }else{
                    commit('SET_ANNOUNCEMENT_ITEMS', response.data)
                }
                resolve(response.data.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    updateAnnouncement({state}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) =>{
            this.$axios.post(`announcement/${id}`, rest.formData)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    deleteAnnouncement({state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.delete(`announcement/${payload}`)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    updateCloseStatus({}){
        return new Promise((resolve, reject) => {
            this.$axios.post('announcement/close-status', {status: 1})
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}