export const state = () => ({
    data: [],
    page: 1,
    store_hours: []
})

export const mutations = {
    ASSIGN_APPOINTMENT(state, payload) {
        state.data = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_STORE_HOURS(state, payload){
        state.store_hours = payload
    }
}

export const actions = {
    getAppointment({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            let method = payload ? typeof payload.method != 'undefined' ? payload.method:'':''
            let type = payload ? typeof payload.type != 'undefined' ? payload.type:'':''
            this.$axios.get(`/appointment?page=${ type != 'all' ? state.page : ''}&method=${method}&type=${type}`)
            .then((response) => {
                commit('ASSIGN_APPOINTMENT', response.data)
                resolve(response.data)
            })
        })
    },
    assignSalesPerson({commit}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) =>{
            this.$axios.put(`/appointment/${id}`, rest)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    getStoreHours({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`v2/store-hours`, {params: payload})
            .then(response => {
                commit('SET_STORE_HOURS', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    confirmAppointment({}, paylaod){
        return new Promise((resolve, reject) => {
            this.$axios.get(`appointment/confirm/${paylaod}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}