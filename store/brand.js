export const state = () => ({
    brands: [],
    groupBrands: [],
    availableBrands: [],
    selectedBrand: null,
})

export const mutations = {
    ASSIGN_BRANDS(state, payload) {
        state.brands = payload
    },
    ASSIGN_GROUP_BRANDS(state, payload) {
        state.groupBrands = payload
    },
    ASSIGN_AVAILABLE_BRANDS(state, payload) {
        state.availableBrands = payload
    },
    ASSIGN_SELECTED_BRAND(state, payload) {
        state.selectedBrand = payload;
    }
}

export const actions = {
    getBrands({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/brand`,{
                params: {
                    search: payload ? payload : ''
                }
            })
            .then((response) => {
                commit('ASSIGN_BRANDS', response.data.data)
                resolve(response.data)
            })
        })
    },
    updateBrands({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/brand', payload).then((response) => {
                resolve()
            })
        })
    },
    updateDetail({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/brand/detail', payload).then((response) => {
                resolve()
            })
        })
    },
    getBrandNames({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/brand/brand-name`)
            .then((response) => {
                commit('ASSIGN_BRANDS', response.data.data)
                resolve(response.data)
            })
        })
    },
    getBrandGroup({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/brand-group`)
            .then((response) => {
                commit('ASSIGN_GROUP_BRANDS', response.data)
                resolve(response.data)
            })
        })
    },
    getAvaiableBrand({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/brand-group/available-brand`)
            .then((response) => {
                commit('ASSIGN_AVAILABLE_BRANDS', response.data)
                resolve(response.data)
            })
        })
    },
    postBrandGroup({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.post(`/brand-group`, payload)
            .then((response) => {
                resolve(response.data);
            })
        })
    },
    updateBrandGroup({ dispatch }, {id, form}) {
        return new Promise((resolve, reject) => {
            return this.$axios.put(`/brand-group/update/${id}`, form)
            .then((response) => {
                resolve(response.data);
            })
        })
    },
    deleteBrandGroup({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.delete(`/brand-group/delete/${payload}`)
            .then((response) => {
                dispatch('getBrandGroup');
                resolve(response.data);
            })
        })
    },

}
