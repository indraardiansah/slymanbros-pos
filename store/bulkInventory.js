import { reject } from "lodash"

export const state = () => ({
    fields: [
        {
            key: 'check',
            thStyle: {
                width: '50px'
            }
        },
        {
            key: 'model',
            label: 'Model',
            thStyle: {
                width: '200px'
            }
        },
        // {
        //     key: 'product_detail.category',
        //     label: 'Category',
        // },
        // {
        //     key: 'product_detail.brand',
        //     label: 'Brand',
        // },
        {
            key: 'type',
            label: 'Type',
            thStyle: {
                width: '150px'
            }
        },
        {
            key: 'estimated_availability',
            thStyle: {
                width: '200px'
            }
        },
        {
            key: 'location',
            label: 'Location',
            thStyle: {
                width: '200px'
            }
        },
        {
            key: 'condition',
            label: 'Condition',
            thStyle: {
                width: '200px'
            }
        },
        {
            key: 'stock',
            label: 'Stock',
            thStyle: {
                width: '100px'
            }
        },
        // {
        //     key: 'new_cost',
        //     label: 'New Cost',
        //     thStyle: {
        //         width: '215px'
        //     }
        // },
        {
            key: 'action',
            label: '',
            thStyle: {
                width: '50px'
            }
        }
    ],
    items: [],
    items_scanout: [],
    items_order: null,
    scanout_report: {
        data: [],
        meta: null
    },
    po_number_list: [],
    manual_scanout_data: []
})

export const mutations = {
    SET_BULK_INVENTORY_ITEMS(state, payload) {
        for(var i in payload){
            payload[i].estimated_availability != null ? payload[i].estimated_availability = new Date(payload[i].estimated_availability) : payload[i].estimated_availability
        }
        state.items = payload
    },
    SET_SCANOUT_ITEMS(state, payload){
        state.items_scanout = payload
    },
    SET_SCANED_ORDER(state, payload){
        state.items_order = payload
    },
    SET_SCAN_OUT_REPORT(state, payload){
        state.scanout_report = payload
    },
    SET_PO_NUMBER_LIST(state, payload){
        state.po_number_list = payload
    },
    SET_MANUAL_SCANOUT_DATA(state, payload){
        state.manual_scanout_data = payload
    }
}

export const actions = {
    postBulkInventory({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/add-bulk', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    getBulkInventory({commit}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get('inventory/get-bulk', {
                params: {
                    invalid: payload.invalid,
                    type: payload.type
                }
            })
            .then(response => {
                payload.type == 2 ? commit('SET_SCANOUT_ITEMS', response.data.data) : commit('SET_BULK_INVENTORY_ITEMS', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    updateBulkInventory({commit}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put('inventory/update-bulk', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    deleteBulkInventory({commit}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/delete-bulk', {
                id: payload.id
            })
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    saveBulkInventory({commit}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/save-bulk', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    postScanOut({commit}, {params, payload}){
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/scan-out', payload, {
                params: params
            })
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    updateScanOut({commit}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put('inventory/scan-out/update', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    getScanOrder({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`inventory/scan-order`, {params: payload})
            .then(response => {
                commit('SET_SCANED_ORDER', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    registerUpc({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.post('register-upc', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    registerModel({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.post('register-model', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    scanDeliveryItems({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/individual-scanout/${id}`, {params: rest})
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getScanLog({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/scan-log`, {params: payload})
            .then(response => {
                commit('SET_SCAN_OUT_REPORT', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getPoNumber({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/get-po-number`, {params: payload})
            .then(response => {
                commit('SET_PO_NUMBER_LIST', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getManualScanOut({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/manual-scanout`, {params: payload})
            .then(response => {
                commit('SET_MANUAL_SCANOUT_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    postManualScanOut({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/manual-scanout`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateScanLog({}, { id, payload }) {
        return new Promise((resolve) => {
            this.$axios.put(`inventory/scan-log/update/${id}`, { name: payload })
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
}
