export const state = () => ({
    events: []
})

export const mutations = {
    ASSIGN_EVENTS(state, payload) {
        state.events = payload
    }
}

export const actions = {
    getCalendar({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/event-calendar`)
            .then((response) => {
                commit('ASSIGN_EVENTS', response.data)
                resolve()
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    addNewEvent({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.post(`/event-calendar`, payload)
            .then((response) => {
                dispatch('getCalendar')
                resolve()
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    updateEvent({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.put(`/event-calendar/${payload.id}`, payload)
            .then((response) => {
                resolve()
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    deleteEvent({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.delete(`/event-calendar/${payload}`)
            .then((response) => {
                resolve()
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
}