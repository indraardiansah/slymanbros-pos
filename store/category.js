export const state = () => ({
    categories: [],
    subcategories: [],
    detailcategories: [],
    allcategories: []
})

export const mutations = {
    ASSIGN_CATEGORIES(state, payload) {
        state.categories = payload
    },
    ASSIGN_SUBCATEGORIES(state, payload) {
        state.subcategories = payload
    },
    ASSIGN_DETAILCATEGORIES(state, payload) {
        state.detailcategories = payload
    },
    SET_ALL_CATEGORIES_DATA(state, payload){
        state.allcategories = payload
    }
}

export const actions = {
    getCategories({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/category`)
            .then(response => {
                commit('ASSIGN_CATEGORIES', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getSubCategories({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/category/sub/${payload}`)
            .then(response => {
                commit('ASSIGN_SUBCATEGORIES', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getDetailCategories({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/category/detail/${payload.category_name}/${payload.subcategory_name}`)
            .then(response => {
                commit('ASSIGN_DETAILCATEGORIES', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getAllCategories({commit}){
        return new Promise((resolve, reject) => {
            this.$axios.get(`category/all`)
            .then(response => {
                commit('SET_ALL_CATEGORIES_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}