export const state = () => ({
    data : null
})
export const mutations = {
    SET_CHANGELOG_DATA(state, payload){
        state.data = payload
    }
}
export const actions = {
    postChangelog({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post('change-log', payload)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getChangelog({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`change-log/${payload.id}?type=${payload.type}`)
            .then(response =>{
                resolve(response.data)
                commit('SET_CHANGELOG_DATA', response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    postOnlineChangelog({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post('quote-request/change-log', payload)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    }
}