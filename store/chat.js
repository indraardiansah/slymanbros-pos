export const state = () => ({
    chatList: [],
    chatKey: [],
    conversation: [],
    tempDataChat: [],
    userOnline:  0,
    groupSelected: null
})
export const mutations = {
    SET_CHAT_LIST_DATA(state, payload){
        state.chatList = payload
    },
    SET_USER_ONLINE_DATA(state, payload){
        state.userOnline = payload
    },
    SET_CHAT_KEY(state, payload){
        state.chatKey = payload
    },
    SET_CONVERSATION_DATA(state, payload){
        if(state.conversation.length > 0){
            if(state.conversation.find(item => item.user_id == payload.user_id)){
                let index = state.conversation.findIndex(function(item) {
                    return item.user_id == payload.user_id
                })
                payload.data.reverse().forEach(element => {
                    state.conversation[index].data.unshift(element)
                })
            } else {
                state.conversation.push(payload)    
            }
        } else {
            state.conversation.push(payload)
        }
    },
    SET_COVERSATION_DATA_EDITED(state, payload){
        if(state.conversation.length > 0){
            state.conversation.forEach(item =>{
                if(item.user_id == payload.user_id){
                    item.name = payload.name
                    item.member = payload.member
                    item.status = payload.status
                }
            })
        }
    },
    ADD_NEW_MESSAGE_DATA(state, payload){
        if(state.conversation.length > 0){
            if(state.conversation.find(item => item.user_id == payload.sender_id || item.user_id == payload.receiver_id)){
                let index = state.conversation.findIndex(function(item) {
                    return (item.user_id == payload.sender_id || item.user_id == payload.receiver_id) && item.is_group == payload.is_group
                })
                state.conversation[index].data.push(payload)
                // if(payload.attachment == null) {
                //     let findIndexTime = state.conversation[index].data.findIndex(v => {return v.sent_time == payload.sent_time && v.attachment == null && v.receiver_id == payload.receiver_id})
                //     if(findIndexTime != -1){
                //         state.conversation[index].data[findIndexTime].body += '@#@' + payload.body
                //     }else{
                //         state.conversation[index].data.push(payload)
                //     }
                // }else{
                //     state.conversation[index].data.push(payload)
                // }
            } else {
                // state.conversation.push(payload)    
            }
        } else {
            // state.conversation.push(payload)
        }
    },
    SET_SEEN_STATUS(state, payload){
        if(state.conversation.length > 0 && payload.length > 0){
            if(state.conversation.find(item => item.user_id == payload[0].sender_id || item.user_id == payload[0].receiver_id)){
                let index = state.conversation.findIndex(function(item) {
                    return item.user_id == payload[0].sender_id || item.user_id == payload[0].receiver_id
                })
                state.conversation[index].data.forEach(item =>{
                    payload.forEach(v =>{
                        if(item.sender_id == v.sender_id){
                            item.status = v.status
                        }
                    })
                })
            }
        }
    },
    REMOVE_SELECTED_CONVERSATION_DATA(state, payload){
        if(state.conversation.length > 0){
            state.conversation = state.conversation.filter(function(el) {
                return el.user_id != payload.user_id
            })
        }
    },
    SET_GROUP_SELECTED(state, payload){
        state.groupSelected = payload
    },
    SET_ATTACHMENT_DATA(state, payload){
        if(state.conversation.length > 0){
            let index = state.conversation.findIndex(v => {return v.user_id == payload.user_id})
            let data = state.conversation[index].attachment[0]
            data.id = payload.data.id
            data.file_name = payload.data.file_name
            data.file_type = payload.data.file_type
            data.file_size = payload.data.file_size
            data.thumbnail = payload.data.thumbnail
            // Object.assign(data, payload.data);
        } 
    },
    SET_TEMP_CHAT_DATA(state, payload){
        state.tempDataChat.push({
            page: 1,
            isLoading: false,
            isUpload: false,
            isShowEmoji: false
        })
    },
    REMOVE_TEMP_CHAT_DATA(state, payload){
        state.tempDataChat.splice(payload, 1)
    }
}
export const actions = {
    getChatList({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`chat/chat-list/${payload.id}?q=${payload.q}`, {progress: false})
            .then(response =>{
                commit('SET_CHAT_LIST_DATA', response.data.data)
                commit('SET_USER_ONLINE_DATA', response.data.user_online)
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getChatKey({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`chat/get-key/${payload}` , {progress: false})
            .then(response =>{
                commit('SET_CHAT_KEY', response.data)
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getConversation({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`chat/conversation/${payload.sender_id}`,
                {
                    params: {
                        receiver_id: payload.receiver_id,
                        page: payload.page,
                        with_conv: payload.with_conv,
                        // skip: payload.skip ? payload.skip : null
                    },
                },
                {progress: false}
            )
            .then(response =>{
                payload.with_conv ? commit('SET_CONVERSATION_DATA', response.data) : commit('SET_COVERSATION_DATA_EDITED', response.data)
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    postMessage({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post('chat/send', payload, {progress: false})
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getOnlineStatus({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`chat/online-status/${payload}`, {progress: false})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    chatSeenStatus({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`chat/seen`, payload, {progress: false})
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        }) 
    },
    chatTypingStatus({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`chat/typing`, payload, {progress: false})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    createGroupChat({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`chat/group/create`, payload, {progress: false})
            .then(response =>{
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    updateGroup({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.put(`chat/group/update/${payload.user_id}`, 
            {
                group_name : payload.group_name,
                member: payload.member
            }, 
            {progress: false})
            .then(response => {
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    addPeopleGroup({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`chat/group/invite/${payload.user_id}`, 
            {
                member: payload.member
            }, 
            {progress: false})
            .then(response => {
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    deleteGroupChat({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.delete(`chat/group/delete/${payload}`)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    leaveGroupChat({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.put(`chat/group/left/${payload.group_id}` ,{
                user_id : payload.user_id
            })
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    addNewChat({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            commit('ADD_NEW_MESSAGE_DATA', payload)
            resolve(payload)
        })
    },
    uploadFile({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post('chat/attachment', payload.attachment, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                onUploadProgress: function( progressEvent ) {
                    let progress = parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total))
                    let index = state.conversation.findIndex(v => {return v.user_id == payload.data.user_id})
                    state.conversation[index].attachment[0].progress = progress
                },
                progress: false
            })
            .then(response =>{
                commit('SET_ATTACHMENT_DATA', {user_id: payload.data.user_id, data: response.data})
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    }
}