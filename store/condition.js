export const state = () => ({
    conditions: {
        data: [],
        meta: null
    },
    conditionSelected: null,
})

export const mutations = {
    SET_CONDITION_DATA(state, payload){
        state.conditions = payload
    },
    SET_CONDITION_SELECTED(state, payload){
        state.conditionSelected = payload
    }
}

export const actions = {
    async getConditions({commit}, payload){
        const response = await this.$axios.get('condition', {params: payload})
        const {data, ...meta} = response.data
        let condition = {
            data,
            meta: meta
        }
        commit('SET_CONDITION_DATA', condition)
        return response
    },
    async getCondition({commit}, payload){
        const response = await this.$axios.get(`condition/${payload}`)
        commit('SET_CONDITION_SELECTED', response.data)
        return response
    },
    async postCondition({}, payload){
        const response = await this.$axios.post('condition', payload)
        return response
    },
    async putCondition({}, payload){
        const response = await this.$axios.put(`condition/${payload.id}`, payload)
        return response
    },
    async deleteCondition({}, payload){
        const response = await this.$axios.delete(`condition/${payload}`)
        return response
    }
}
