export const state = () => ({
    data: {
        items: [],
        meta: null
    }
})

export const mutations = {
    SET_CUSTOM_ITEMS(state, payload){
        const {meta, data} = payload
        state.data.meta = meta
        state.data.items = data
    }
}

export const actions = {
    getCustomItemReport({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get('report/custom-product', {params: payload})
            .then(response => {
                commit('SET_CUSTOM_ITEMS', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    changeStatus({}, payload){
        const {id, status} = payload
        return new Promise((resolve, reject) => {
            return this.$axios.put(`order/custom-product-status/${id}?status=${status}`)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    }
}