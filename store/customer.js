export const state = () => ({
    data: {
        firstName: null,
        lastName: null,
        company: null,
        phoneNumber: null,
        email: null,
        billingAddress: null,
        shippingDefault: null,
        formatted_billing_address: {
            number: '',
            street: '',
            city: '',
            state: '',
            postal_code: '',
            country: ''
        },
        shippingAddress: [],
        phoneNumber: [],
        shipPhoneNumber: [],
        tax_exempt: false,
        sameAsDelivery: true
    },
    dataQuote: {
        firstName: null,
        lastName: null,
        company: null,
        phoneNumber: null,
        email: null,
        billingAddress: null,
        shippingDefault: null,
        formatted_billing_address: {
            number: '',
            street: '',
            city: '',
            state: '',
            postal_code: '',
            country: ''
        },
        shippingAddress: [],
        phoneNumber: [],
        shipPhoneNumber: [],
        tax_exempt: false,
        sameAsDelivery: true
    },
    fields: [
        {
            key: 'id',
            label: 'Customer Code',
            thStyle:{
                width: '150px !important'
            }
        },
        {
            key: 'name',
            label: 'Name',
            thStyle:{
                width: '150px !important'
            }
        },
        {
            key: 'phone_number',
            label: 'Phone Number',
            thStyle:{
                width: '150px !important'
            }
        },
        {
            key: 'billingAddress',
            label: 'Billing Address	'
        },
        {
            key: 'shippingAddress',
            label: 'Delivery Address (Default)'
        },
        {
            key: 'email',
            label: 'Email'
        },
    ],
    items: [],
    creditLimit: null,
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    errors: null,
    exists_data: []
})

export const mutations = {
    SET_CUSTOMER_DATA(state, payload) {
        state.data = payload
    },
    SET_CUSTOMER_DATA_QUOTE(state, payload){
        state.dataQuote = payload
    },
    SET_CUSTOMER_DATA_DEFAULT(state, payload) {
        state.data = {
            firstName: null,
            lastName: null,
            company: null,
            email: null,
            billingAddress: null,
            formatted_billing_address: {
                number: '',
                street: '',
                city: '',
                state: '',
                postal_code: '',
                country: ''
            },
            shippingAddress: [],
            phoneNumber: [],
            tax_exempt: false
        }
    },
    SET_CUSTOMER_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_CUSTOMER_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'id',
                label: 'Customer Code'
            },
            {
                key: 'name',
                label: 'Name'
            },
            {
                key: 'phoneNumber',
                label: 'Phone Number'
            },
            {
                key: 'billingAddress',
                label: 'Billing Address	'
            },
            {
                key: 'shippingAddress',
                label: 'Delivery Address (Default)'
            },
            {
                key: 'email',
                label: 'Email'
            },
        ]
    },
    SET_CUSTOMER_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_CUSTOMER_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_CUSTOMER_TABLE_FILLTERED(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_CUSTOMER_SELECTED(state, payload) {
        state.selected = payload
    },
    CLEAR_CUSTOMER_SELECTED(state) {
        state.selected = null
    },
    SET_CUSTOMER_PAGE(state, payload) {
        state.page = payload
    },
    SET_CUSTOMER_ERRORS(state, payload) {
        state.errors = payload
    },
    SET_EXISTS_DATA(state, payload) {
        state.exists_data = payload
    },
    SET_CUSTOMER_CREDIT(state, payload){
        state.creditLimit = payload
    }
}

export const actions = {
    getCustomers({ state, commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/customer`, {
                params: {
                    page: state.page,
                    name: payload != null ? payload.search == null ? '' : payload.search : '',
                    company: payload != null ? payload.company == null ? '' : payload.company : '',
                    isCompany: payload != null ? payload.isCompany == null ? '' : payload.isCompany : '',
                    credit: payload != null ? payload.credit == null ? '' : payload.credit : '',
                    perpage: payload == null ? '' : payload.perpage,
                    has_credit_line: payload == null ? '' : payload.has_credit_line
                }
            })
            .then(response => {
                commit('SET_CUSTOMER_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getCustomer({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`customer/${payload}`)
            .then(response => {
                commit('SET_CUSTOMER_DATA', response.data.data)
                resolve(response.data.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    checkMatchedData({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/customer/matched', payload)
            .then((response) => {
                commit('SET_EXISTS_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    postCustomer({ state, commit }) {
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            Object.keys(state.data).forEach((key) => {
                if (key == 'tax') {
                    formData.append('tax_id_number', state.data.tax.id_number)
                    formData.append('tax_document', state.data.tax.file)
                } else if (key == 'shippingAddress') {
                    formData.append(key, JSON.stringify(state.data.shippingAddress))
                } else if (key == 'phoneNumber') {
                    formData.append(key, JSON.stringify(state.data.phoneNumber))
                } else if(key == 'shippingPhoneNumber') {
                    formData.append(key, JSON.stringify(state.data.shippingPhoneNumber))
                } else {
                    formData.append(key, state.data[key])
                }
            });

            this.$axios.post('/customer', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                commit('SET_CUSTOMER_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_CUSTOMER_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    putCustomer({ state, commit }, payload) {
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            Object.keys(state.data).forEach((key) => {
                if (key == 'tax') {
                    formData.append('tax_id_number', state.data.tax.id_number)
                    formData.append('tax_document', state.data.tax.file)
                } else if (key == 'shippingAddress') {
                    formData.append(key, JSON.stringify(state.data.shippingAddress))
                } else if (key == 'phoneNumber') {
                    formData.append(key, JSON.stringify(state.data.phoneNumber))
                } else if (key == 'shippingPhoneNumber') {
                    formData.append(key, JSON.stringify(state.data.shippingPhoneNumber))
                } else {
                    formData.append(key, state.data[key])
                }
            });

            this.$axios.post(`customer/${payload.customer_code}`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(response => {
                commit('SET_CUSTOMER_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_CUSTOMER_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    searchCustomer({ commit, state }, payload) {
        let name = payload.name ? payload.name: ''
        let company = payload.company ? payload.company: ''
        let isCompany = payload.isCompany ? payload.isCompany: ''
        let buildersCode = payload.buildersCode ? payload.buildersCode: ''
        let credit = payload.credit ? payload.credit : ''
        let has_credit_line = payload.has_credit_line ? payload.has_credit_line : ''
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/customer?page=${state.page}&credit=${credit}&name=${name}&company=${company}&isCompany=${isCompany}&has_credit_line=${has_credit_line}&buildersCode=${buildersCode}&phone_number=${payload.phoneNumber}&billing_address=${payload.billingAddress}&shipping_address=${payload.shippingAddress}&email=${payload.email}&perpage=${payload.perpage}`)
            .then((response) => {
                commit('SET_CUSTOMER_TABLE_FILLTERED', response.data)
                resolve(response.data)
            })
        })
    },
    destroyCustomer({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/customer/${payload}`).then((response) => {
                dispatch('getCustomers').then(() => {
                    resolve(response.data)
                })
            })
        })
    },

    //DASHBOARD
    findCustomer({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/customer?name=${payload.name}&phone_number=${payload.phoneNumber}`)
            .then((response) => {
                commit('SET_CUSTOMER_SELECTED', response.data.data)
                resolve(response.data)
            })
        })
    },
    postCreditLimit({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`/customer-credit`, payload)
            .then((response) =>{
                resolve(response.data)
            })
        })
    },
    editCreditLimit({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.put(`/customer-credit/${payload.customer_code}`, {
                term: payload.term,
                credit_limit : payload.credit_limit
            })
            .then((response) =>{
                resolve(response.data)
            })
        })
    },
    postCustomerAddress({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`/customer/address`, payload)
            .then((response) =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    updateCustomerAddress({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`/customer/address/edit`, payload)
            .then((response) =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    selectCustomerAddress({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`/customer/address/status`, payload)
            .then((response) =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    deleteCustomerAddress({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.delete(`/customer/address/${payload}`)
            .then((response) =>{
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    updateAccountCredit({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`customer-account-credit/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}