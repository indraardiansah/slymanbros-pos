import { TYPES } from '@/types/store/customerNotification.js';


export const state = () => ({
    email: {
        data: [],
    },
    sms: {
        data: [],
    },
    form: {
        subject: '',
        body: '',
        status: 1,
    },
    formTime: {
        hour: null,
    },
})

export const mutations = {
    [TYPES.SET_CUSTOMER_NOTIFICATION_EMAIL](state, payload) {
        state.email.data = payload;
    },
    [TYPES.SET_CUSTOMER_NOTIFICATION_SMS](state, payload) {
        state.sms.data = payload;
    },
    [TYPES.SET_FORM_CUSTOMER_EMAIL_EACH](state, payload) {
        state.form[payload.key] = payload.value;
    },
    [TYPES.SET_FORM_CUSTOMER_NOTIFICATION](state, payload) {
        state.form = payload;
    },
    [TYPES.SET_FORM_TIME_CUSTOMER_NOTIFICATION](state, payload) {
        state.formTime = payload;
    },

}

export const getters = {
    customNotificationEmailList(state) {
        return state.email.data;
    },
    customNotificationSmsList(state) {
        return state.sms.data;
    }
}

export const actions = {
    getCustomerNotificationEmail({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('customer-notification/email')
                .then((response) => {
                    const mappedResponse = response.data.map((email) => {
                        if (email.id === 15) {
                            return {
                                ...email,
                                hour: email.hour / 168,
                                status: email.status.toString(),
                            }
                        }

                        if (email.id !== 15) {
                            return {
                                ...email,
                                status: email.status.toString(),
                            }
                        }
                    })

                    commit(TYPES.SET_CUSTOMER_NOTIFICATION_EMAIL, mappedResponse);

                    resolve(response);
                })
                .catch(error => {
                    new Error(error.response)
                    reject(error.response)
                })
        });
    },
    getCustomerNotificationSms({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('customer-notification/sms')
                .then((response) => {
                    const mappedResponse = response.data.map((sms) => {
                        return {
                            ...sms,
                            status: sms.status.toString(),
                        }
                    })

                    commit(TYPES.SET_CUSTOMER_NOTIFICATION_SMS, mappedResponse);

                    resolve(response);
                })
                .catch(error => {
                    new Error(error.response)
                    reject(error.response)
                })
        });
    },
    saveCustomerNotification({ dispatch, state }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.put(`customer-notification/update/${payload.id}`, state.form)
                .then((response) => {
                    if (payload.type === 'email') dispatch('getCustomerNotificationEmail');
                    if (payload.type === 'sms') dispatch('getCustomerNotificationSms');

                    resolve(response);
                })
                .catch(error => {
                    new Error(error.response)
                    reject(error.response)
                })
        });
    },
    updateTimeCustomerNotification({ dispatch, state }, payload) {
        return new Promise((resolve, reject) => {
            const form = {
                hour: payload.id === 15 ? state.formTime.hour * 168 : state.formTime.hour,
            }

            return this.$axios.put(`customer-notification/update-time/${payload.id}`, form)
                .then((response) => {
                    if (payload.type === 'email') dispatch('getCustomerNotificationEmail');
                    if (payload.type === 'sms') dispatch('getCustomerNotificationSms');

                    resolve(response);
                })
                .catch(error => {
                    new Error(error.response)
                    reject(error.response)
                })
        });
    }
}
