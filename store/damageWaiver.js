import { TYPES } from '@/types/store/damageWaiver';
import { damageWaiverKey } from '@/model/store/damageWaiver.js';
import { __filterObjectKeys } from '@/utils/filterObjectKeys.js';

export const state = () => ({
  form: {
    title: '',
    text: '',
    status: false,
  },
})

export const mutations = {
  [TYPES.SET_DAMAGE_WAIVER](state, payload) {
    state.form = { ...state.form, ...payload };
  },
  [TYPES.SET_DAMAGE_WAIVER_TITLE](state, payload) {
    state.form.title = payload;
  },
  [TYPES.SET_DAMAGE_WAIVER_TEXT](state, payload) {
    state.form.text = payload;
  },
  [TYPES.SET_DAMAGE_WAIVER_STATUS](state, payload) {
    state.form.status = payload;
  }
}

export const actions = {
  getDamageWaiverSetting({ commit }) {
    return new Promise((resolve, reject) => {
      return this.$axios.get('damage-waiver')
        .then((response) => {
          const damageWaiver = __filterObjectKeys(response.data, damageWaiverKey);

          commit(TYPES.SET_DAMAGE_WAIVER, damageWaiver);
          resolve(response);
        })
        .catch(error => {
          new Error(error.response)
          reject(error.response)
        })
    })
  },
  saveDamageWaiverSetting({ }, payload) {
    return new Promise((resolve, reject) => {
      return this.$axios.post('damage-waiver', payload)
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          new Error(error)
          reject(error)
        })
    })
  }
}
