export const state = () => ({
    monthly_revenue: [],
    today_revenue: [],
    store_active: [],
    searchData: [],
    monthly_revenue_chart : null,
    total_net:0,
    schedule_delivery: {
        data: [],
        meta: null
    },
    daily_compared_data: [],
    monthly_compared_data: []
})

export const mutations = {
    ASSIGN_MONTHLY_REVENUE(state, payload) {
        state.monthly_revenue = payload
    },
    ASSIGN_TODAY_REVENUE(state, payload) {
        state.today_revenue = payload
    },
    ASSIGN_STORE_ACTIVE(state, payload) {
        state.store_active = payload
    },
    SEARCH_RESULT(state, payload){
        state.searchData = payload
    },
    SEARCH_RESULT_DEFAULT(state, payload){
        state.searchData = []
    },
    SET_CHART_DATA(state, payload){
        state.monthly_revenue_chart = payload
    },
    SET_SCHEDULE_DATA(state, payload){
        state.schedule_delivery = payload
    },
    SET_TOTAL_NET(state, payload){
        state.total_net = payload
    },
    SET_DAILY_COMPARED(state, payload){
        let formatedPayload = payload.map(o => { return { ...o, date: new Date(o.date), is_custom: o.name == 'Custom' ? true : false } })
        state.daily_compared_data = formatedPayload
    },
    SET_MONTHLY_COMPARED(state, payload){
        let formatedPayload = payload.map(o => { return { ...o, date: new Date(o.date), is_custom: o.name == 'Custom' ? true : false } })
        state.monthly_compared_data = formatedPayload
    }
}

export const actions = {
    getMonthlyRevenue({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/monthly-revenue`,{
                params: payload
            })
            .then((response) => {
                commit('ASSIGN_MONTHLY_REVENUE', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getTodayRevenue({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/today-revenue`, {params: payload})
            .then((response) => {
                commit('ASSIGN_TODAY_REVENUE', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getStoreActive({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/user/store`)
            .then((response) => {
                commit('ASSIGN_STORE_ACTIVE', response.data)
                resolve(response.data)
            })
        })
    },
    changeStoreLocation({ state }, payload) {
        return new Promise((resolve, reject) => {
            let data = {
                user_id: payload.user_id,
                default_store: payload.store_id
            }
            return this.$axios.post(`/user/change-location?type_update=location`, data)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    searchAction({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post('/search', payload,{
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response =>{
                commit('SEARCH_RESULT', response.data.data)
                resolve(response.data.data)
            })
        })
    },
    getMonthlyChart({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`monthly-revenue`, {
                params: payload
            })
            .then((response) => {
                commit('SET_CHART_DATA', response.data.data)
                resolve(response.data.data)
            })
        })
    },
    getScheduleDeliveryDashboard({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`deliveries/schedule`, {params: payload})
            .then((response) => {
                commit('SET_SCHEDULE_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    getNetProductRevenue({commit}){
        return new Promise((resolve, reject) => {
            this.$axios.get(`net-revenue`)
            .then(response => {
                commit('SET_TOTAL_NET', response.data.total_net)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getDailyRevenueCompared({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`today-revenue-compared-date`, {params:  payload})
            .then(response => {
                commit('SET_DAILY_COMPARED', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getMonthlyRevenueCompared({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`monthly-revenue-compared-date`, {params:  payload})
            .then(response => {
                commit('SET_MONTHLY_COMPARED', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    updateDashboardLayout({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`change-priority-menu-dashboard`, payload)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    }
}
