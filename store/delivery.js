export const state = () => ({
    tab: 0,
    data: {
        date: null,
        time: {
            selected: null,
            data: [
                'AM',
                'PM',
                'All Day'
            ]
        }
    },
    fields: [
        {
            key: 'order',
            label: 'Order #'
        },
        {
            key: 'customer',
            label: 'Customer'
        },
        {
            key: 'deliveryDate',
            label: 'Delivery Date'
        }
    ],
    trucks: [],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    deliveryRouting: {
        average_time: null,
        data: []
    },
    deliveryRoutingLog: {
        data: [],
        is_exist: false,
        meta: {
            current_page: 1,
            last_page: 1
        }
    },
    deliveryLog: [],
    deliveryMeta: null,
    deliveryContent: [],
    deliveryCapacity: null,
    deliveryCapacitySelected: null,
    deliveryMethodsRouting: [],
    deliveryTimeSelected: null,
    savedRoute: null,
    unscannedItems: null,
    deliveryStatus: [],
    routingFilter: {
        date: null,
        delivery_method: null,
        num_vehicle: null
    },
    modelNumberByTrucks: [],
    optionsSearch: [],
})

export const mutations = {
    SET_TAB(state, payload) {
        state.tab = payload;
    },
    SET_DELIVERY_DATA(state, payload) {
        state.data = payload
    },
    SET_DELIVERY_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_DELIVERY_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'order',
                label: 'Order #'
            },
            {
                key: 'customer',
                label: 'Customer'
            },
            {
                key: 'deliveryDate',
                label: 'Delivery Date'
            },
            {
                key: 'action',
                label: 'Action'
            }
        ]
    },
    SET_DELIVERY_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_DELIVERY_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_DELIVERY_PAGE(state, payload) {
        state.page = payload
    },
    SET_DELIVERY_ROUTING(state, payload) {
        state.deliveryRouting = payload
    },
    SET_SELECTED_DELIVERY_ROUTING(state, payload) {
        state.deliveryRouting.data[payload.truck -1] = payload
    },
    SET_DELIVERY_LOG(state, payload){
        state.deliveryLog = payload.data,
        state.deliveryMeta = payload.meta
    },
    SET_DELIVERY_CONTENT(state, payload){
        state.deliveryContent = payload
    },
    SET_DELIVERY_CAPACITY(state, payload){
        state.deliveryCapacity = payload.data
    },
    SET_DELIVERY_CAPACITY_SELECTED(state, payload){
        state.deliveryCapacitySelected = payload
    },
    SET_DELIVERY_METHODS_ROUTING(state, payload){
        state.deliveryMethodsRouting = payload
    },
    SET_DELIVERY_TIME_ITEM(state, payload){
        state.deliveryTimeSelected = payload
    },
    SET_SAVED_ROUTE(state, payload){
        if(payload.message) state.savedRoute = null
        else state.savedRoute = payload
    },
    CLEAR_SAVED_ROUTE(state){
        state.savedRoute = null
    },
    SET_UNSCANNED_ITEMS(state, payload){
        state.unscannedItems = payload
    },
    SET_DELIVERY_STATUS(state, payload){
        state.deliveryStatus = payload
    },
    SET_ROUTING_LOG(state, payload){
        const {data, meta} = payload
        if(state.deliveryRoutingLog.meta.current_page === 1){
            state.deliveryRoutingLog.data = data
        }
        else{
            state.deliveryRoutingLog.data.push(...data)
        }
        state.deliveryRoutingLog.is_exist = data.length > 0 ? true : false,
        state.deliveryRoutingLog.meta.current_page = meta.current_page
        state.deliveryRoutingLog.meta.last_page = meta.last_page
    },
    CLEAR_ROUTING_LOG(state){
        state.deliveryRoutingLog.data = []
        state.deliveryRoutingLog.meta.current_page = 1
        state.deliveryRoutingLog.meta.last_page = 1
    },
    SET_ROUTING_FILTER(state, payload){
        state.routingFilter = payload
    },
    SET_TRUCKS(state, payload) {
        state.trucks = payload;
    },
    SET_MODEL_NUMBER_BY_TRUCKS(state, payload) {
        state.modelNumberByTrucks = payload;
    },
    SET_OPTION_SEARCH(state, payload) {
        state.optionsSearch = payload;
    },
    SET_SERVICE_CALL_DESCRIPTION(state, payload) {
        state.deliveryRouting.data[payload.truck].routes[payload.indexRoute + 1].service_call[payload.indexServiceCall].description = payload.value;
    },
    SET_DELIVERY_REMINDER_SENT(state, payload) {
        state.deliveryRouting.data[payload.indexTruck].routes[payload.indexTruckDest + 1].order[payload.indexOrder].is_delivery_reminder_sent = 1;
    },
}

export const actions = {
    getDeliveries({ state, commit }, payload) {
        let search = payload.search != null ? payload.search : ''
        let perpage = payload.perpage != null ? payload.perpage : ''
        return this.$axios.get(`order-schedule-delivery`, {
            params: {
                search: search,
                page: state.page,
                perpage: perpage
            }
        })
        .then(response => {
            commit('SET_DELIVERY_TABLE', response.data)
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    getDelivery({ commit }, payload) {
        return this.$axios.get(`/order-schedule-delivery-data/${payload}`)
        .then(response => {
            commit('SET_DELIVERY_DATA', response.data)
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    getDeliveryRouting({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/delivery/routing-v3`, payload)
            .then(response => {
                commit('SET_DELIVERY_ROUTING', response.data)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_DELIVERY_ROUTING', {average_time: null, data: []})
                reject(error.response)
            })
        })
    },
    getDeliveryRoutingGH({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/delivery/graphHopper`, {params: payload})
            .then(response => {
                commit('SET_DELIVERY_ROUTING', response.data)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_DELIVERY_ROUTING', {average_time: null, data: []})
                reject(error.response)
            })
        })
    },
    getDeliveryLog({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`deliveries/log`, {
                params: {
                    page : payload.page
                }
            })
            .then(response => {
                commit('SET_DELIVERY_LOG', response.data)
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    getDeliveryContent({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get('deliveries/content', {params: payload})
            .then(response => {
                commit('SET_DELIVERY_CONTENT', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    getDeliveryCount({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get('deliveries/total', {params: payload})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    getDeliveryCapacity({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get('delivery-cap', {params: payload})
            .then(response => {
                resolve(response.data)
                commit('SET_DELIVERY_CAPACITY', response)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    saveDelieryCapacity({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.post('delivery-cap/store', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
                new Error(error.response)
            })
        })
    },
    updateDelieryCapacity({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.put(`delivery-cap/update/${payload.id}`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
                new Error(error.response)
            })
        })
    },
    showDeliveryCapacity({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`delivery-cap/show/${payload}`)
            .then(response => {
                resolve(response.data)
                commit('SET_DELIVERY_CAPACITY_SELECTED', response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    deleteDeliveryCapacity({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.delete(`delivery-cap/delete/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    setDeliveryBook({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.post('delivery-book', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    getDeliveryMethodRouting({commit}, payload){
        return new Promise((resolve, reject) =>{
            return this.$axios.get(`delivery-method-routing`)
            .then(response => {
                commit('SET_DELIVERY_METHODS_ROUTING', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getEstimatedStop({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`delivery/estimated-stop`, {params: payload})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    postDeliveryRoute({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`delivery-route/store`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    loadDeliveryRoute({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`delivery-route/show`, {params: payload})
            .then(response => {
                commit('SET_SAVED_ROUTE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    reRouteTruck({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`delivery/routing/reroute-truck`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    fetchManualRouting({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`delivery/manual-routing`, payload)
            .then(response => {
                commit('SET_DELIVERY_ROUTING', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getDeliveryMethodLimit({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`delivery-method/limit`, {params: payload})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    postDeliveryTime({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`load-time/store`, payload)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    showDeliveryTime({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`load-time/show/${payload}`)
            .then(response => {
                commit('SET_DELIVERY_TIME_ITEM', response.data)
                resolve(response)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateDeliveryTime({}, payload){
        const {id, ...rest} = payload
            return new Promise((resolve, reject) => {
                this.$axios.put(`load-time/update/${id}`, rest).then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
            })
    },
    updateOrderAddress({}, payload){
        const {order_no, ...rest} = payload
            return new Promise((resolve, reject) => {
                this.$axios.put(`order/update-delivery-address/${order_no}`, rest).then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
            })
    },
    deleteDeliveryTime({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.delete(`load-time/delete/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    addCustomOrder({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`custom-order/store`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateCustomOrder({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`custom-order/update/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    deleteCustomOrder({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.delete(`custom-order/delete/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getUnscanItem({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`delivery/unscan-out/routing`, {params: payload})
            .then(response => {
                commit('SET_UNSCANNED_ITEMS', response.data.exist ? response.data : null)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    printRoute({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get('delivery-route/print-pdf', {params: payload})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updatePhoneNumber({}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`order/update-phone-number/${payload.id}`, { ship_phone_number: payload.value })
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error.response);
            })
        })
    },
    getDeliveryStatus({commit}){
        return new Promise((resolve, reject) => {
            this.$axios.get(`deliveries/status-list`)
            .then(response => {
                commit('SET_DELIVERY_STATUS', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    getDeliveryRoutingLog({commit, state}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get('delivery/routing-log/v2', {
                params: {
                    ...payload,
                    page: state.deliveryRoutingLog.meta.current_page
                }
            })
            .then(response => {
                commit('SET_ROUTING_LOG', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    restoreDeliveryRoutingLog({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`delivery/routing-log/apply/${payload}`)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    showDeliveryRoutingLog({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`delivery/routing-log/show/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    showOriginalRoute({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get('delivery-route/show-original', {params : payload})
            .then(response => {
                commit('SET_DELIVERY_ROUTING', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getTrucks({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get('delivery-route/get-truck', {
                params: {
                    ...payload,
                }
            })
            .then(response => {
                commit('SET_TRUCKS', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    addServiceCall({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post('service-call', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateServiceCall({}, { id, payload }) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`service-call/update/${id}`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })

    },
    getServiceCallByOrderNo({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`service-call/order/${payload}`)
            .then(response => {
                commit('SET_MODEL_NUMBER_BY_TRUCKS', response.data);
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getAllModelByTruck({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`delivery-route/get-truck/${payload}`)
            .then(response => {
                commit('SET_MODEL_NUMBER_BY_TRUCKS', response.data);
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getServiceCall({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`service-call/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },

    getScheduleDeliveryPDF({}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`schedule-delivery/get-pdf`, {
                params: payload
            }).then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error.response);
            })
        })
    },
    searchOrder({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get('delivery-route/search-order', { params: payload })
            .then((response) => {
                commit('SET_OPTION_SEARCH', response.data);
                resolve(response.data);
            }).catch((error) => {
                reject(error.response);
            })
        })
    },
    sendDeliveryReminderManual({}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('customer-notification/delivery-reminder-manual', payload)
            .then((response) => {
                resolve(response.data);
            }).catch((error) => {
                reject(error.response);
            })
        })
    },
    postUnroute({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post('delivery-route/unroute-stop', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch((error) => {
                reject(error.response)
            })
        })
    },
    rescheduleAllDeliveries({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/delivery-route/reschedule-all`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
}
