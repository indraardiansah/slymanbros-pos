export const state = () => ({
    fields: [
        {
            key: 'model',
            label: 'Model'
        },
        {
            key: 'qty',
            label: 'Qty',
            thStyle:{
                width: '115px'
            }
        },
        {
            key: 'installs',
            label: 'Install',
            thStyle:{
                width: '200px'
            }
        },
        {
            key: 'serial_number',
            label: 'Serial Number',
            thStyle:{
                width: '200px'
            }
        },
        {
            key: 'location',
            label: 'Location',
            thStyle:{
                width: '270px'
            }
        },
        {
            key: 'deliveryMethod',
            label: 'Delivery Method',
            thStyle:{
                width: '250px'
            }
        },
        {
            key: 'status',
            label: 'Status',
            thStyle:{
                width: '100px'
            }
        },
        {
            key: 'deliveryDate',
            label: 'Delivery Date',
            thStyle:{
                width: '170px'
            }
        },
        {
            key: 'customerLink',
            label: 'Customer Link',
            thStyle:{
                width: '200px'
            },
        },
    ],
    items: [],
    bookedDate: [],
    pastBookedDate: [],
    scheduleOff: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    delivery_capacity: 0
})

export const mutations = {
    SET_DELIVERY_DETAIL_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_DELIVERY_DETAIL_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'deliveryMethod',
                label: 'Delivery Method'
            },
            {
                key: 'model',
                label: 'Model'
            },
            {
                key: 'qty',
                label: 'Qty'
            },
            {
                key: 'deliveryDate',
                label: 'Delivery Date'
            },
            {
                key: 'requestTime',
                label: 'Request Time'
            },
            {
                key: 'status',
                label: 'Status'
            },
        ]
    },
    SET_DELIVERY_CAPACITY(state, payload){
        state.delivery_capacity = payload
    },
    SET_DELIVERY_DETAIL_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_DELIVERY_DETAIL_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_DELIVERY_DETAIL_PAGE(state, payload) {
        state.page = payload
    },
    SET_BOOKED_DATE(state, payload){
        let formatedDate = []
        payload.data.forEach(v =>{
            let date = new Date(v)
            formatedDate.push(date)
        })
        if(payload.past_date) state.pastBookedDate = formatedDate
        else state.bookedDate = formatedDate
    },
    SET_SCHEDULE_OFF(state, payload){
        state.scheduleOff = payload
    },
    SET_DELIVERY_METHOD_NULL(state, payload) {
        state.items[payload].deliveryMethod = null;
    }
}

export const actions = {
    getDeliveryDetails({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/order-schedule-delivery/${payload}?page=${state.page}`)
            .then(response => {
                commit('SET_DELIVERY_DETAIL_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getDeliveryMaxCurrentQuantity({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/delivery/current-delivery-quantity`,{
                params: payload
            })
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getTodayDeliveryCapacity({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/delivery-cap/today`,{
                params: payload
            })
            .then(response => {

                commit('SET_DELIVERY_CAPACITY', response.data.capacity)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    setScheduleDelivery({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/order-schedule-delivery/${payload}`, {
                schedule_delivery: state.items.filter(v => { return v.type === undefined})
            })
            .then((response) => {
                resolve(response)
            })
        })
    },
    setScheduleDeliveryBatch({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/order-schedule-delivery/${payload}/batch`, {
                schedule_delivery: state.items.filter(v => { return v.type === undefined})
            })
            .then((response) => {
                resolve(response)
            })
        })
    },
    setDeliveryMethod({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/order-schedule-delivery/${payload.id}`, {
                delivery_method_id: payload.delivery_method_id,
                delivery_method_name: payload.delivery_method_name
            })
            .then((response) => {
                resolve(response)
            })
        })
    },
    getPdfScheduleDelivery({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/order/delivery/get-pdf?order_no=${payload.order_no}&delivery_date=${payload.date}`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    subtmiDispatchTrack({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/dispatch-track`, {
                params: {
                    id: payload.id,
                    store_id: payload.store_id,
                    store_name: payload.store_name,
                    from_to: payload.from_to,
                    qty: payload.qty,
                    type: payload.type,
                    note: payload.note,
                    change_log: payload.change_log
                }
            })
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    removeDeliveryDate({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/order-schedule-delivery/change-status`, payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    updateDeliveryStatus({state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.put(`/schedule-delivery/${payload.id}`, {
                data: payload.data
            }).then(response =>{
                resolve(response.data)
                console.log(response.data)
            })
        })
    },
    getDeliveryBooked({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`/schedule/booked`, {params: payload})
            .then(response => {
                commit('SET_BOOKED_DATE', {data: response.data, past_date : payload.past_date == 1 ? true: false })
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    postPastDate({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.post(`order-schedule-delivery/past/${id}`, {...rest})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
                new Error(error.response)
            })
        })
    },
    updatePastDate({}, payload){
        const {id, notes} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`order-schedule-delivery/past/${id}`, {notes})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
                new Error(error.response)
            })
        })
    },
    postSerialNumber({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`sn`, payload)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    updateSerialNumber({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`sn/${id}`, rest)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    deleteSerialNumber({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.delete(`sn/${payload}`)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getScheduleOff({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`schedule/off`)
            .then(response => {
                commit('SET_SCHEDULE_OFF', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}
