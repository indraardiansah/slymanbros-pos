export const state = () => ({
    data: {
        delivery_method_id: null,
        delivery_method_name: null,
        is_default: null
    },
    fields: [
        {
            key: 'delivery_method_name',
            label: 'Delivery Method Name'
        },
        {
            key: 'action',
            label: 'Action'
        }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    deliveryType: [],
    selected: null,
    page: 1,
    errors: null
})

export const mutations = {
    SET_DELIVERY_METHOD_DATA(state, payload) {
        state.data = payload
    },
    SET_DELIVERY_METHOD_DATA_DEFAULT(state, payload) {
        state.data = {
            delivery_method_id: null,
            delivery_method_name: null,
            is_default: null
        }
    },
    SET_DELIVERY_METHOD_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_DELIVERY_METHOD_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_DELIVERY_METHOD_ITEMS(state, payload) {
        state.items = payload.data
    },
    SET_DELIVERY_METHOD_PAGE(state, payload) {
        state.page = payload
    },
    SET_DELIVERY_METHOD_ERRORS(state, payload) {
        state.errors = payload
    },
    SET_DELIVERY_TYPE(state, payload){
        state.deliveryType = payload
    },
    ADD_NOT_SET_DELIVERY_METHOD(state, payload){
        state.items.unshift({
            delivery_method_id: 'not-set',
            delivery_method_name: 'Not Set'
        })
    }
}

export const actions = {
    getDeliveryMethods({ state, commit }, payload) {
        let type = ''
        let perpage = 10
        if(payload){
            type = payload.type? payload.type: ''
            perpage = payload.perpage? payload.perpage: 10
        }
        return this.$axios.get(`/delivery`, {
            params: {
                page: state.page,
                type: type,
                perpage: perpage
            }
        })
        .then(response => {
            commit(payload == 'all' ? 'SET_DELIVERY_METHOD_ITEMS' : 'SET_DELIVERY_METHOD_TABLE', response.data)
            if(payload.not_set != undefined){
                if(payload.not_set) commit('ADD_NOT_SET_DELIVERY_METHOD')
            }
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    getDeliveryMethod({ commit }, payload) {
        return this.$axios.get(`/delivery/${payload}/edit`)
        .then((response) => {
            commit('SET_DELIVERY_METHOD_DATA', response.data)
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    postDeliveryMethod({ commit }, payload) {
        return this.$axios.post('/delivery', payload)
        .then((response) => {
            return response.data
        })
        .catch(error => {
            commit('SET_DELIVERY_METHOD_ERRORS', error.response.data.errors)
            new Error(error)
        })
    },
    putDeliveryMethod({ commit }, payload) {
        return this.$axios.put(`/delivery/${payload.delivery_method_id}`, payload)
        .then((response) => {
            commit('SET_DELIVERY_METHOD_ERRORS', null)
            return response.data
        })
        .catch(error => {
            commit('SET_DELIVERY_METHOD_ERRORS', error.response.data.errors)
            new Error(error)
        })
    },
    deleteDeliveryMethod({ dispatch }, payload) {
        return this.$axios.delete(`/delivery/${payload}`)
        .then((response) => {
            dispatch('getDeliveryMethods')
            .then(() => {
                return response.data
            })
        })
    },
    putDeliveryMethodDefault({ dispatch, commit }, payload) {
        return this.$axios.put(`/delivery/default/${payload.delivery_method_id}`, payload)
        .then((response) => {
            dispatch('getDeliveryMethods')
            .then(() => {
                return response.data
            })
        })
        .catch(error => {
            // console.log(error.response)
            commit('SET_DELIVERY_METHOD_ERRORS', error.response.data.errors)
            new Error(error)
        })
    },
    updateDeliveryType({}, payload){
        const {id, type_id} = payload
        return new Promise((resolve, reject) => {
            return this.$axios.put(`delivery/type/${id}`, {type: type_id})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getDeliveryType({commit}){
        return new Promise((resolve, reject) => {
            this.$axios.get(`delivery-type`)
            .then(response => {
                commit('SET_DELIVERY_TYPE', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}
