export const state = () => ({
    fields: [
        {
            key: 'day',
            label: 'Day'
        },
        {
            key: 'is_active',
            label: 'Status'
        },
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    loadTime: [],
    unloadTime: []
})

export const mutations = {
    SET_DELIVERY_TIMES_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_DELIVERY_TIMES_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'day',
                label: 'Day'
            },
            {
                key: 'is_active',
                label: 'Status'
            },
        ]
    },
    SET_DELIVERY_TIMES_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_DELIVERY_TIMES_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_DELIVERY_TIMES_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_DELIVERY_TIMES_PAGE(state, payload) {
        state.page = payload
    },
    SET_LOAD_TIME(state, payload){
        state.loadTime = payload
    },
    SET_UNLOAD_TIME(state, payload){
        state.unloadTime = payload
    }
}

export const actions = {
    getDeliveryTimes({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/delivery-times`, {params: payload})
            .then(response => {
                commit('SET_DELIVERY_TIMES_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    putDeliveryTime({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/delivery-times/${payload.id}`, {
                // id: payload.id,
                is_active : payload.is_active,
                is_active_service_call : payload.is_active_service_call,
            })
            .then((response) => {
                resolve(response)
            })
        })
    },
    putDeliveryTimeCapacity({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/delivery-times/change-capacity/${payload.id}`, payload)
            .then((response) => {
                resolve(response)
            })
        })
    },
    getLoadTimes({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`load-time`, {params: payload})
            .then(response => {
                commit('SET_LOAD_TIME', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateLoadTime({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            return this.$axios.put(`load-time/update/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getUnloadTimes({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`unload-time`, {params: payload})
            .then(response => {
                commit('SET_UNLOAD_TIME', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}
