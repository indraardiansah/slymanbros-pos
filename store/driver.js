export const state = () => ({
    drivers: [],
    drivers_data: {
        data: [],
        meta: null
    },
    errors: null,
    driver_selected: null
})

export const mutations = {
    SET_DRIVERS(state, payload) {
        state.drivers = payload
    },
    SET_DRIVERS_DEFAULT(state, payload) {
        state.drivers = []
    },
    SET_DRIVER_ERRORS(state, payload){
        state.errors = payload
    },
    SET_DRIVER_DATA(state, payload){
        const {data, ...meta} = payload
        state.drivers_data.data = data
        state.drivers_data.meta = meta
    },
    SET_DRIVER_SELECTED(state, payload){
        state.driver_selected = payload
    }
}

export const actions = {
    getDrivers({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/driver`, { params: payload })
            .then(response => {
                commit('SET_DRIVERS', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getDriversData({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`/driver/index`, {params: payload})
            .then(response => {
                commit('SET_DRIVER_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    postDriver({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`/driver/store`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    putDriver({}, payload){
        const{id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`/driver/update/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    deleteDriver({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/driver/delete/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getDriver({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`/driver/show/${payload}`)
            .then(response => {
                commit('SET_DRIVER_SELECTED', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}
