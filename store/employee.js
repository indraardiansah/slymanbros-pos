export const state = () => ({
    data: {
        user_id: null,
        first_name: null,
        last_name: null,
        default_store: null,
        passcode: null,
        email: null,
        phone_number: null,
        job_title: null,
        type: null,
        is_admin: null,
        extension_number: null,
        fullname: null,
        job_description: null,
        show_phone_invoice: null,
        type: null,
        store: [
            {
                store_id: null,
                store_name: null
            }
        ],
    },
    validation: [],
    fields: [
        {
            key: 'photo',
            label: ''
        },
        {
            key: 'first_name',
            label: 'Name'
        },
        // {
        //     key: 'last_name',
        //     label: 'Last Name'
        // },
        {
            key: 'job_title',
            label: 'Job Title'
        },
        {
            key: 'email',
            label: 'Email'
        },
        {
            key: 'phone_number',
            label: 'Mobile Phone',
            thStyle:{
                width: '200px !important'
            }
        },
        {
            key: 'extension_number',
            label: 'Direct Line',
            thStyle:{
                width: '210px !important'
            }
        },
        {
            key: 'is_sales_person',
            label: 'Sales Person'
        },
        // {
        //     key: 'user_store',
        //     label: 'Default Store'
        // },
        // {
        //     key: 'action',
        //     label: 'Action'
        // }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    errors: null,
    selected: null,
    page: 1,
})

export const mutations = {
    SET_EMPLOYEE_DATA(state, payload) {
        state.data = payload
    },
    SET_EMPLOYEE_DATA_DEFAULT(state, payload) {
        state.data = {
            user_id: null,
            first_name: null,
            last_name: null,
            default_store: null,
            passcode: null,
            email: null,
            phone_number: null,
            job_title: null,
            type: null,
            is_admin: null,
            extension_number: null,
            fullname: null,
            job_description: null,
            show_phone_invoice: null,
            type: null,
            store: [
                {
                    store_id: null,
                    store_name: null
                }
            ]
        }
    },
    SET_EMPLOYEE_TABLE(state, payload) {
        state.items = payload.data
        if (payload.type != 'frontend') {
            state.meta = payload.meta
        }
    },
    SET_EMPLOYEE_PAGE(state, payload) {
        state.page = payload
    },
    SET_EMPLOYEE_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_EMPLOYEE_ERRORS(state, payload){
        state.errors = payload
    },
    SET_EMPLOYEE_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_VALIDATION_MESSAGE(state, payload) {
        state.validation = payload
    },
    CLEAR_VALIDATION_MESSAGE(state, payload) {
        state.validation = []
    }
}

export const actions = {
    getEmployees({ commit, state }, payload) {
        let page = state.page
        let status = ''
        let search = ''
        let type = ''
        let perpage = 10
        if(payload){
            type = payload.type ? payload.type :''
            if (payload.type != 'frontend') {
                search = payload.search ? payload.search :''
                page = payload.page  ? payload.page : state.page
                status = payload.status ? payload.status :''
                perpage = payload.perpage ? payload.perpage :10
            }
        }
        return new Promise((resolve, reject) => {
            this.$axios.get(`/user/employees?page=${page}&status=${status}&q=${search}&perpage=${perpage}&type=${type}`)
            .then(response => {
                let data = Object.assign({type: type}, response.data)
                commit('SET_EMPLOYEE_TABLE', data)
                resolve()
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    saveEmployee({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/user', payload, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_EMPLOYEE_ERRORS', error.response.data.errors)
                commit('SET_SAVE_LOADING', false, {root:true})
                new Error(error)
            })
        })
    },
    getEmployee({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/user/employees/${payload}`)
            .then((response) => {
                commit('SET_EMPLOYEE_DATA', response.data.data)
                resolve(response.data.data)
            }).catch(error => {
                commit('SET_EMPLOYEE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    updateEmployee({ state, commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/user/employees/${payload.get('user_id')}`, payload, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_EMPLOYEE_ERRORS', error.response.data.errors)
                commit('SET_SAVE_LOADING', false, {root:true})
                new Error(error)
            })
        })
    },
    updatePassword({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/user/employees/change-password`, payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    destroyEmployee({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/user/employees/${payload}`)
            .then((response) => {
                dispatch('getEmployees', { status: 'active', search: '' }).then(() => {
                    resolve(response.data)
                })
            })
        })
    },
    sendEmailInvitation({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('user/employees/invite', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                if (error.response.status == 422) {
                    commit('SET_VALIDATION_MESSAGE', error.response.data.errors)
                }
                new Error(error)
            })
        })
    },
    postSort({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/user/employees/sort', {
                user : payload,
                type_update : 'location'
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_EMPLOYEE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    requestAccessPassword({ commit }, payload)  {
        return new Promise((resolve, reject) => {
            this.$axios.post('/user/request-access', {
                user_id: payload
            })
        })
    },
    updateSalesPerson({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/user/employees/sales-person', payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    getEmployeeCSV() {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/directory/export-employee`, {
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
            }).then((res) => {
                resolve(res.data)
            })
        })
    }
}