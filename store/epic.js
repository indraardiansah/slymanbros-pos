export const state = () => ({
    epic_log: [],
    epic_meta: null
})

export const mutations = {
    ASSIGN_EPIC_LOG(state, payload){
        state.epic_log = payload.data,
        state.epic_meta = payload.meta
    }
}

export const actions = {
    getEpicLog({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/warranty-epic-report/log`,{
                params: payload
            })
            .then((response) => {
                commit('ASSIGN_EPIC_LOG', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
}