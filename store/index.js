import vuexPersist from 'vuex-persist'

export const strict = false

export const state = () => ({
    api_token: '6zFRwz3zpJnEb3hq5me4fkeA1AnHYQGC3gy7UGpObN3VDpuZo8yF3yg3E4mQ',
    loading: false,
    isDisableButton: false,
    loadingSave: false,
    loadingPayment: false,
    // loadingTab: false,
    loadingTable: false,
    message: '',
    logoutMessage: '',
    sessionLocalStorage: null,
    switchAvailability: {
        pos: false,
        dataApp: false,
    },
    hostname: '',
    warrantyLeaderboardFilter: [],
    topSalesPersonFilter: [],
    notification: {
        data: {
            counter: 0,
            data: [],
            meta: null
        },
        setting: []
    },
    messageCenter: [],
    isShowBottomSheet: false,
    isShowNotification: false,
    isOffline: false
})

export const mutations = {
    SET_LOADING(state, payload) {
        state.loading = payload
    },
    SET_SAVE_LOADING(state, payload) {
        state.loadingSave = payload
    },
    SET_DISABLE_BUTTON(state, payload) {
        state.isDisableButton = payload
    },
    SET_LOADING_TABLE(state, payload) {
        state.loadingTable = payload
    },
    // SET_LOADING_TAB(state, payload) {
    //     state.loadingTab = payload
    // },
    SET_LOADING_PAYMENT(state, payload) {
        state.loadingPayment = payload
    },
    SET_TOKEN(state, payload) {
        state.api_token = payload
    },
    SET_MESSAGE(state, payload) {
        state.message = payload
    },
    SET_LOGOUT_MESSAGE(state, payload) {
        state.logoutMessage = payload
    },
    SET_USER_SESSION(state, payload) {
        // localStorage.setItem('userSession', JSON.stringify(payload))
        state.sessionLocalStorage = payload
    },
    RESTORE_MUTATION() {
        vuexPersist.RESTORE_MUTATION
    },
    SET_SWITCH_AVAILABILITY(state, payload) {
        state.switchAvailability[payload.key] = payload.value;
    },
    SET_HOST_NAME(state, payload) {
        state.hostname = payload
    },
    SET_WARRANTY_LEADERBOARD_FILTER(state, payload) {
        state.warrantyLeaderboardFilter.push(payload.start_month, payload.end_month)
    },
    CLEAR_WARRANTY_LEADERBOARD_FILTER(state) {
        state.warrantyLeaderboardFilter = []
    },
    SET_SALES_PERSON_FILTER(state, payload) {
        state.topSalesPersonFilter.push(payload.start_month, payload.end_month)
    },
    CLEAR_SALES_PERSON_FILTER(state) {
        state.topSalesPersonFilter = []
    },
    SET_NOTIFICATION_SETTING(state, payload) {
        state.notification.setting = payload
    },
    SET_NOTIFICATION_DATA(state, payload) {
        state.notification.data = payload
    },
    ADD_NOTIFICATION_DATA(state, payload) {
        state.notification.data.data.push(...payload.data)
        state.notification.data.meta = payload.meta
    },
    SET_SHOW_BOTTOM_SHEET(state, payload) {
        state.isShowBottomSheet = payload;
    },
    SET_SHOW_NOTIFICATION(state, payload) {
        state.isShowNotification = payload;
    },
    TOGGLE_SHOW_NOTIFICATION(state) {
        state.isShowNotification = !state.isShowNotification;
    },
    SET_MESSAGE_CENTER_DATA(state, payload) {
        state.messageCenter = payload;
    },
    ADD_MESSAGE_CENTER_DATA(state, payload) {
        state.messageCenter[payload.index].data.push(...payload.data);
        state.messageCenter[payload.index].meta = payload.meta;
    },
    SET_OFFLINE(state, payload){
        state.isOffline = payload
    }
}

export const actions = {
    async nuxtServerInit({ commit, dispatch }, context) {
        let cookieToken = context.app.$auth.$storage.getCookie('_token.local')
        let token = ''
        if (typeof cookieToken != 'undefined' && cookieToken != false) {
            token = context.app.$auth.$storage.getCookie('_token.local').split(' ')[1]
        }
        commit('SET_TOKEN', token)
        commit('SET_HOST_NAME', context.req.headers.host)
        if (context.app.$auth.$state.loggedIn) {
            await Promise.all([
                dispatch('store/getStores'),
                dispatch('getUserSetting'),
                dispatch('getUserNotification'),
                dispatch('getSwitchAvailability', { hostname: context.req.headers.host, email: context.app.$auth.$state.user.email }),
                dispatch('getSwitchAvailabilityDataApp', context.app.$auth.$state.user.email),
            ])
        }
    },
    checkIn({ }, payload) {
        const headers = { Authorization: `Bearer ${payload}` }
        return new Promise((resolve) => {
            return this.$axios.get('check-in', { headers })
                .then(response => {
                    resolve(response.data)
                })
        })
    },
    getSwitchAvailabilityDataApp({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get('https://api.data-slymanbros.slyman.com/api/check-availability', {
                headers: {
                    'x-token-x': ')wHSMph^wx^2C#S'
                },
                params: { email: payload }
            })
                .then(response => {
                    commit('SET_SWITCH_AVAILABILITY', { key:'dataApp', value: response.data });
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                    new Error(error.response)
                })
        })
    },
    getSwitchAvailability({ commit, }, payload) {
        const { hostname, email } = payload
        let url = ''
        if (hostname === '127.0.0.1:2020') url = 'https://api.slymanbros.slymanmedia.com/api/check-availability'
        else url = 'https://api.slymanbros.com/api/check-availability'
        return new Promise((resolve, reject) => {
            return this.$axios.get(url, {
                headers: {
                    'x-token-x': ')wHSMph^wx^2C#S'
                },
                params: { email: email }
            })
                .then(response => {
                    commit('SET_SWITCH_AVAILABILITY', { key: 'pos', value: response.data })
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                    new Error(error.response)
                })
        })
    },
    switchApp(_, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(payload.url, {
                headers: {
                    'x-token-x': ')wHSMph^wx^2C#S'
                },
                params: { email: payload.email }
            })
                .then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                    new Error(error.response)
                })
        })
    },
    getUserSetting({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`user/setting/show`, { progress: false })
                .then(response => {
                    commit('SET_NOTIFICATION_SETTING', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    updateUserSetting({ }, payload) {
        const { id, ...rest } = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`user/setting/update/${id}`, rest, { progress: false })
                .then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getUserNotification({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`user/message-center`, { params: payload, progress: false })
                .then(response => {
                    if (payload != undefined) commit('ADD_NOTIFICATION_DATA', response.data)
                    else commit('SET_NOTIFICATION_DATA', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    getMessageCenter({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`user/message-center/all`)
                .then((response) => {
                    commit('SET_MESSAGE_CENTER_DATA', response.data);
                    resolve(response.data)
                }).catch((error) => {
                    reject(error.response)
                })
        })
    },
    readNotification({ }, payload) {
        const { id, ...rest } = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`user/message-center/update/${id}`, rest, { progress: false })
                .then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    readAllNotifications({ }) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`user/message-center/read-all`)
                .then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
    pushNewNotification({ commit }, { id, params, index }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`user/message-center/priority/${id}`, {
                params
            }).then((response) => {
                commit('ADD_MESSAGE_CENTER_DATA', { index, data: response.data.data, meta: response.data.meta });
                resolve(response);
            }).catch((error) => {
                new Error(error.response);
                reject(error.response);
            })
        })
    },
}
