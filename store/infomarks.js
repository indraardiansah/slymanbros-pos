export const actions = {
    postInfoMarks({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.post('delivery-mark', payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    updateInfoMarks({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.put(`delivery-mark/${payload.id}`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    },
    deleteInfoMarks({}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.delete(`delivery-mark/${payload.id}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
            })
        })
    }
}