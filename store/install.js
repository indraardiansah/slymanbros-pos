export const state = () => ({
    data: {
        install_id: null,
        install_name: null,
        description: null,
        install_price: null,
        detail_install: [
            {
                id: null,
                install_id: null,
                category_name: null,
                subcategory_name: null,
                detail_category_name: null,
            }
        ]
            
        // category: null,
        // sub_category: null,
        // detail_category: null
    },
    fields: [
        {
            key: 'install_name',
            label: 'Name'
        },
        {
            key: 'description',
            label: 'Description'
        },
        {
            key: 'install_price',
            label: 'Price'
        },
        {
            key: 'show_detail',
            label: 'Category'
        },
        {
            key: 'action',
            label: 'Action'
        }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: [],
    existing: [],
    page: 1,
    errors: null
})

export const mutations = {
    SET_INSTALL_DATA(state, payload) {
        state.data = payload
    },
    SET_INSTALL_DATA_DEFAULT(state, payload) {
        state.data = {
            install_id: null,
            install_name: null,
            description: null,
            install_price: null,
            detail_install: [
                {
                    id: null,
                    install_id: null,
                    category_name: null,
                    subcategory_name: null,
                    detail_category_name: null,
                }
            ]
        }
    },
    SET_INSTALL_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_INSTALL_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'install_name',
                label: 'Name'
            },
            {
                key: 'description',
                label: 'Description'
            },
            {
                key: 'install_price',
                label: 'Price'
            },
            {
                key: 'show_detail',
                label: 'Category'
            },
            {
                key: 'action',
                label: 'Action'
            }
        ]
    },
    SET_INSTALL_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_INSTALL_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_INSTALL_ITEMS(state, payload) {
        state.items = payload.data
    },
    SET_INSTALL_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_INSTALL_PAGE(state, payload) {
        state.page = payload
    },
    SET_INSTALL_ERRORS(state, payload) {
        state.errors = payload
    },
    SET_EXISTING_INSTALL(state, payload){
        state.existing = payload
    },
    REMOVE_INSTALL_SELECTED(state, payload){
        state.selected.splice(payload, 1)
    },
    SET_INSTALL_SELECTED_DEFAULT(state, payload){
        state.selected = []
    }
}

export const actions = {
    getInstalls({ state, commit }, payload) {
        return this.$axios.get(`/install`, {
            params: {
                page: state.page,
                perpage: payload == null ? '' : payload.perpage
            }
        })
        .then(response => {
            commit('SET_INSTALL_TABLE', response.data)
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    getInstall({ commit }, payload) {
        return this.$axios.get(`/install/${payload}`)
        .then((response) => {
            commit('SET_INSTALL_DATA', response.data)
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    getInstallOrder({ commit }, payload) {
        return this.$axios.get(`/order/install`, {
            params: {
                product_id: payload
            }
        })
        .then((response) => {
            commit('SET_INSTALL_ITEMS', response)
            return response
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    postInstall({ commit }, payload) {
        return this.$axios.post('/install', payload)
        .then((response) => {
            commit('SET_INSTALL_ERRORS', null)
            return response.data
        })
        .catch(error => {
            // console.log(error.response)
            commit('SET_INSTALL_ERRORS', error.response.data.errors)
            new Error(error)
        })
    },
    putInstall({ commit }, payload) {
        return this.$axios.put(`/install/${payload.install_id}`, payload)
        .then((response) => {
            commit('SET_INSTALL_ERRORS', null)
            return response.data
        })
        .catch(error => {
            // console.log(error.response)
            commit('SET_INSTALL_ERRORS', error.response.data.errors)
            new Error(error)
        })
    },
    // putInstall({ rootState, state }) {
    //     return this.$axios.put(`/install/${state.data.install_id}`, state.data)
    //     .then((response) => {
    //         commit('SET_INSTALL_ERRORS', null)
    //         return response.data
    //     })
    //     .catch(error => {
    //         // console.log(error.response)
    //         commit('SET_INSTALL_ERRORS', error.response.data.errors)
    //         new Error(error)
    //     })
    // },
    deleteInstall({ rootState, dispatch }, payload) {
        return this.$axios.delete(`/install/${payload}`)
        .then((response) => {
            dispatch('getInstalls')
            .then(() => {
                return response.data
            })
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    }
}