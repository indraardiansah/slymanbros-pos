// Types
import { TabNotifications } from "@/types/consts/internal-notifications/tab-notifications";
import { TYPES } from "@/types/store/internalNotifications";
import { OptionTypes } from "@/types/consts/internal-notifications/option-types";

export const state = () => ({
    internalNotifications: TabNotifications,
    internalNotificationsTypes: [],
    optionTypes: OptionTypes,
    listRecipient: [],
})

export const mutations = {
    [TYPES.SET_PRIORITY](state, payload) {
        state.internalNotificationsTypes[payload.index].priority = payload.value;
    },
    [TYPES.SET_LIST_RECIPIENT](state, payload) {
        state.listRecipient = payload;
    },
    [TYPES.SET_INTERNAL_NOTIFICATIONS_TYPES](state, payload) {
        state.internalNotificationsTypes = payload;
    },
}

export const getters = {
    mapListRecipient: state => {
        return state.listRecipient.map((recipient) => {
            return {
                user_id: recipient.user_id,
                fullname: recipient.fullname,
                is_assign: recipient.is_assign,
            }
        })
    },
    mapInternalNotificationTypes: state => {
        return state.internalNotificationsTypes.map((internalNotificationType) => {
            let imageAsset = '';
            let wrapperClass = '';

            if (internalNotificationType.priority === 0) {
                wrapperClass = 'no-priority';
                imageAsset = '/icon/notifications/internal-notifications/priority-bell-all.svg';
            }
            if (internalNotificationType.priority === 1) {
                wrapperClass = 'low';
                imageAsset = '/icon/notifications/internal-notifications/priority-bell-low.svg';
            }
            if (internalNotificationType.priority === 2) {
                wrapperClass = 'high';
                imageAsset = '/icon/notifications/internal-notifications/priority-bell-high.svg';
            }
            if (internalNotificationType.priority === 3) {
                wrapperClass = 'urgent';
                imageAsset = '/icon/notifications/internal-notifications/priority-bell-urgent.svg';
            }

            return {
                id: internalNotificationType.id,
                name: internalNotificationType.name,
                priority: [0, internalNotificationType.priority],
                priorityForm: internalNotificationType.priority,
                subject: internalNotificationType.subject,
                body: internalNotificationType.body,
                isSubjectEditable: internalNotificationType.is_subject_editable,
                variable: internalNotificationType.variable,
                image_asset: imageAsset,
                wrapper_class: wrapperClass,
                recipients: internalNotificationType.recipients
            }
        })
    }
}

export const actions = {
    getInternalNotifications({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`internal-notification`)
                .then((response) => {
                    commit(TYPES.SET_INTERNAL_NOTIFICATIONS_TYPES, response.data);
                    resolve(response);
                }).catch((error) => {
                    new Error(error.response);
                    reject(error.response);
                })
        })
    },
    getListRecipient({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`internal-notification/list-recipient`)
                .then((response) => {
                    commit(TYPES.SET_LIST_RECIPIENT, response.data);
                    resolve(response);
                }).catch((error) => {
                    new Error(error.response);
                    reject(error.response);
                })
        })
    },
    updateInternalNotificationType({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.put(`/internal-notification/update/${payload.id}`, payload)
                .then((response) => {
                    dispatch('getInternalNotifications');
                    resolve(response);
                }).catch((error) => {
                    new Error(error.response);
                    reject(error.response);
                })
        })
    }
}
