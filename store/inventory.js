export const state = () => ({
    fields: [
        {
            key: 'location',
            label: 'Location'
        },
        {
            key: 'model',
            label: 'Model #'
        },
        {
            key: 'condition',
            label: 'Condition'
        },
        {
            key: 'brand',
            label: 'Brand'
        },
        {
            key: 'category',
            label: 'Category'
        },
        {
            key: 'subCategory',
            label: 'Sub Category'
        },
        {
            key: 'detailCategory',
            label: 'Detail Category'
        },
        {
            key: 'availableStock',
            label: 'Available Stock'
        },
        {
            key: 'pendingStock',
            label: 'Pending Stock'
        },
        {
            key: 'backOrderStock',
            label: 'Back Order Stock'
        },
        {
            key: 'averageCost',
            label: 'Average Cost'
        },
    ],
    items: [],
    items_order_report: [],
    items_back_order: [],
    items_pending:[],
    item_tagged_stock_detail_fields: ['date', 'order_no', 'product_model',  'quantity'],
    item_tagged_stock_detail: [],
    items_movement: [],
    stock_movement_model: null,
    meta: {
        total: null,
        perPage: null
    },
    total_in_stock: 0,
    pdfUrl: null,
    selected: null,
    page: 1,

    //STOCK
    stock: {
        avg_cost: 0,
        cost: 0,
        total_count: 0,
        stock: 0
    },
    post_stock: null,
    conditions: [],
    inventoryAdjustment: {
        data: [],
        inventory_information: null,
        meta: null
    },
    returnedInventory: {
        data: [],
        meta: null
    },
    previewTransfer: {
        data: [],
        meta: null
    },
    transferLog: {
        data: [],
        meta: null
    },
    inventoryMatchLog: {
        data: [],
        meta: null
    },
    matchData: [],
    loading: false
    //
})

export const mutations = {
    SET_INVENTORY_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_INVENTORY_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'location',
                label: 'Location'
            },
            {
                key: 'model',
                label: 'Model #'
            },
            {
                key: 'condition',
                label: 'Condition'
            },
            {
                key: 'brand',
                label: 'Brand'
            },
            {
                key: 'category',
                label: 'Category'
            },
            {
                key: 'subCategory',
                label: 'Sub Category'
            },
            {
                key: 'detailCategory',
                label: 'Detail Category'
            },
            {
                key: 'availableStock',
                label: 'Available Stock'
            },
            {
                key: 'pendingStock',
                label: 'Pending Stock'
            },
            {
                key: 'backOrderStock',
                label: 'Back Order Stock'
            },
            {
                key: 'averageCost',
                label: 'Average Cost'
            },
        ]
    },
    SET_INVENTORY_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_INVENTORY_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_INVENTORY_PAGE(state, payload) {
        state.page = payload
    },
    ASSIGN_STOCK(state, payload) {
        state.stock = payload
    },
    ASSIGN_POST_STOCK(state, payload) {
        state.post_stock = payload
    },
    SET_CURRENT_STOCK(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
        state.total_in_stock = payload.total_in_stock
    },
    SET_TAGGED_STOCK_DETAIL(state, payload) {
        state.item_tagged_stock_detail = payload.data
    },
    SET_AVAILABLE_SELECTED(state, payload){
        state.selected = payload
    },
    SET_BACKORDER_TABLE(state, payload){
        state.items_back_order = payload.data,
        state.meta = payload.meta
    },
    SET_PENDING_TABLE(state, payload){
        state.items_pending = payload.data,
        state.meta = payload.meta
    },

    SET_MOVEMENT_ITEMS(state, payload){
        state.items_movement = payload
    },
    SET_ORDER_REPORT_TABLE(state, payload){
        state.items_order_report = payload
    },
    SET_MOVEMENT_MODEL(state, payload){
        state.stock_movement_model = payload
    },
    SET_PDF_URL(state, payload) {
        state.pdfUrl = payload
    },
    SET_INVENTORY_ADJUSTMENT(state, payload){
        state.inventoryAdjustment = payload
    },
    SET_RETURNED_INVENTORY_DATA(state, payload){
        const {data, ...meta} = payload
        state.returnedInventory.data = data
        state.returnedInventory.meta = meta
    },
    SET_CONDITIONS_DATA(state, payload){
        state.conditions = payload
    },
    SET_PREVIEW_TRANSFER_DATA(state, payload){
        const {data, ...meta} = payload
        state.previewTransfer.data = data
        state.previewTransfer.meta = meta
    },
    SET_TRANSFER_LOG(state, payload){
        const {data, ...meta} = payload
        state.transferLog.data = data.map(item => {return {...item, _showDetails: false}})
        state.transferLog.meta = meta
    },
    SET_INVENTORY_MATCH_DATA(state, payload){
        state.matchData = payload
    },
    SET_INVENTORY_MATCH_LOG(state, payload){
        const {data, ...meta} = payload
        state.inventoryMatchLog.data = data.map(item => {return  {...item, _showDetails: false}})
        state.inventoryMatchLog.meta = meta
    },
    SET_LOADING(state, payload){
        state.loading = payload
    }
}

export const actions = {
    getInventories({ commit, state}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/load-inventory`,{
                params: {
                    page: state.page,
                    product_id: payload.product_id ? payload.product_id : '',
                    perpage: payload.perpage ? payload.perpage : 10
                }
            })
            .then(response => {
                commit('SET_INVENTORY_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
                new Error(error)
            })
        })
    },
    fillterCurentInventory({ commit, rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`load-inventory?location=${payload.location_id}&available_condition=${payload.available_condition}&brand=${payload.brand}&category_name=${payload.category_name}&subcategory_name=${payload.subcategory_name}&detail_category_name=${payload.detail_category_name}`)
            .then(response => {
                commit('SET_INVENTORY_TABLE', response.data)
                resolve(response.data)
            })
        })
    },
    getPendingStock({ commit, rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/load-cost`, {
                params: {
                    product_id: payload.product_id,
                    pkProduct: payload.pkProduct,
                    store_id: payload.store_id,
                    type: payload.type,
                    available_condition: payload.available_condition
                }
            })
            .then((response) => {
                commit('ASSIGN_STOCK', response.data.data)
                resolve(response.data)
            })
        })
    },
    addStockInventory({ state }) {
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/add-stock', state.post_stock)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getCurrentInventory({commit }, payload) {
        const {cancelToken, ...params} = payload
        commit('SET_LOADING', true)
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/current-inventory`, {
                params: params,
                cancelToken: cancelToken
            })
            .then((response) => {
                commit('SET_CURRENT_STOCK', response.data)
                resolve(response.data)
                commit('SET_LOADING', false)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateCurrentInventory({ rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/update-current-inventory', payload).then((response) => {
                resolve(response.data)
            })
        })
    },
    updateOrderReportStatus({ rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/move-pending-to-ordered-order-report', payload).then((response) => {
                resolve(response.data)
            })
        })
    },
    deleteCurrentInventory({ rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('inventory/delete-current-inventory', payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    getTaggedStock({ rootState, commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/tagged-stock`, {
                params: {
                    page: payload.page,
                    store_id: payload.store_id,
                    product_id: payload.product_id,
                    brand: payload.brand,
                    category_name: payload.category_name,
                    subcategory_name: payload.subcategory_name,
                    detail_category_name: payload.detail_category_name,
                    perpage: payload.perpage
                }
            }).then((response) => {
                commit('SET_CURRENT_STOCK', response.data)
                resolve(response.data)
            })
        })
    },
    getInventoryOrderReport({ rootState, commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/order-report`, {
                params: payload
            }).then((response) => {
                commit('SET_ORDER_REPORT_TABLE', response.data)
                resolve(response.data)
            })
        })
    },
    changeEstimatedAvailability({ commit, rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/change-estimated-availability`, payload)
            .then(response => {
                // console.log(payload)
                resolve(response.data)
            })
        })
    },
    movePendingToOrdered({ commit, rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/move-pending-to-ordered`, payload)
            .then(response => {
                // console.log(payload)
                resolve(response.data)
            })
        })
    },
    getBackOrderStock({ commit, rootState }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/load-back-order`)
            .then(response => {
                commit('SET_BACKORDER_TABLE', response.data)
                resolve(response.data)
            })
        })
    },
    getTaggedStockDetail({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/tagged-stock-detail`, {
                params: payload
            }).then(response => {
                commit('SET_TAGGED_STOCK_DETAIL', response)
                resolve(response)
            })
        })
    },
    fillterBackOrderStock({ commit, rootState, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/load-back-order`, {
                params: {
                    page: state.page,
                    store_id: payload.store_id,
                    product_id: payload.product_id,
                    brand: payload.brand,
                    category_name: payload.category_name,
                    subcategory_name: payload.subcategory_name,
                    detail_category: payload.detail_category,
                    perpage: payload.perpage
                }
            }).then(response => {
                commit('SET_BACKORDER_TABLE', response.data)
                resolve(response.data)
            })
        })
    },
    destroyBackOrderStock({ commit, rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/delete-back-order`, payload)
            .then(response => {
                resolve(response.data)
            })
        })
    },
    recievedBackOrderStock({ commit, rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/received-back-order`, payload)
            .then(response => {
                // console.log(payload)
                resolve(response.data)
            })
        })
    },
    getPendingInventory({ rootState, commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/load-pending-stock`).then((response) => {
                commit('SET_PENDING_TABLE', response.data)
                resolve(response.data)
            })
        })
    },
    fillterPendingInventory({ rootState, commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/load-pending-stock`, {
                params: {
                    page: state.page,
                    store_id: payload.store_id,
                    product_id: payload.product_id,
                    brand: payload.brand,
                    category_name: payload.category_name,
                    subcategory_name: payload.subcategory_name,
                    detail_category_name: payload.detail_category_name,
                    perpage: payload.perpage
                }
            }).then((response) => {
                commit('SET_PENDING_TABLE', response.data)
                resolve(response.data)
            })
        })
    },
    destroyPendingStock({ commit, rootState }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/delete-pending-stock`, payload)
            .then(response => {
                resolve(response.data)
            })
        })
    },
    getCSV({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/get-csv`, {
                params: payload
            }).then((response) => {
                commit('SET_PDF_URL',   response.data.data)
                resolve(response.data)
            })
        })
    },
    downloadCSV({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(payload.url,{
                responseType: 'blob', // important
            }
              ).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', payload.name+'.csv');
                document.body.appendChild(link);
                link.click();
              });
        })
    },
    getPDF({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/get-pdf`, {
                params: payload
            }).then((response) => {
                resolve(response.data)
            })
        })
    },
    getStockMovement({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/stock-movement`, {
                params: payload
            })
            .then((response) => {
                commit('SET_MOVEMENT_ITEMS', response.data.data)
                commit('SET_MOVEMENT_MODEL', response.data.model)
                resolve(response.data)
            })
        })
    },
    putEstimatedDate({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            return this.$axios.put(`inventory/stock-order/update/${id}`, {...rest})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    getCSVOrderReport({ commit }, payload) {
        return new Promise((resolve, reject) => {

            this.$axios.get(`/inventory/${payload.type}/get-csv`, {
                params: payload,
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
            }).then((res) => {
                commit('SET_PDF_URL', res.data.data)
                resolve(res.data)
            })
        })
    },
    updateCustomModel({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/update-model/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    updateAvailableQty({}, payload){
        const {id, quantity} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/update-available/${id}`, {quantity: quantity})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },

    //Inventory Adjustment
    getInventoryAdjustment({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/get-inventory-adjustment`, {params: payload})
            .then(response => {
                commit('SET_INVENTORY_ADJUSTMENT', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    updateQtyGeneral({}, payload){
        const {id, params, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/update-available/${id}`, rest, {params: params != undefined ? params : null})
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    tagInventory({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/tag/${id}`, rest)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    untagInventory({}, payload){
        const {id, params, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/untag/${id}`, rest, {params: params != undefined ? params : null})
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getOrderNewTag({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/new-tag`, {params: payload})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    postNewTag({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/new-tag`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    tagBlankOnOrder({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/tag-on-order/${id}`, {...rest})
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    tagBlankReserved({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/tag-reserved`, payload)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    tagBlankAvailable({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/tag-available`, payload)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    exportCsvReserved({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/reserved/csv`, {params: payload})
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    moveBulkOnOrder({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/move-pending-to-ordered-bulk`, payload)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    moveOrder({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/move-order`, payload) .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getReturnedInventoryPending({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get('inventory/return/pending', {params: payload})
            .then(response => {
                commit('SET_RETURNED_INVENTORY_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getReturnedInventoryResolved({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get('inventory/return/resolved', {params: payload})
            .then(response => {
                commit('SET_RETURNED_INVENTORY_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateInventoryReturnReason({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/return/update-reason/${payload.id}`, payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                reject(response.data)
            })
        })
    },
    moveReturnedInventory({}, payload){
        const { id, form } = payload;
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/return/update/${id}`, form, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getConditions({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`conditions`)
            .then(response => {
                commit('SET_CONDITIONS_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    checkTransfer({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/transfer/check`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    saveTransfer({}, payload){
        const {type, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/transfer?type=${type}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getPreviewTransfer({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/transfer`, {params: payload})
            .then(response => {
                commit('SET_PREVIEW_TRANSFER_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateQtyTransfer({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            this.$axios.put(`inventory/transfer/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    deleteTransfer({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.delete(`inventory/transfer/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    processTransfer({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/transfer/process`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getTransferLog({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/transfer/logs`, {params: payload})
            .then(response => {
                commit('SET_TRANSFER_LOG', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    postInventoryMatch({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/match`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getInventoryMatch({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/match`, payload)
            .then(response => {
                commit('SET_INVENTORY_MATCH_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    finishInventoryMatch({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/match/finish`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    deleteInventoryMatch({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.delete(`inventory/match/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    getInventoryMatchLog({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.get(`inventory/match/logs`, {params: payload})
            .then(response => {
                commit('SET_INVENTORY_MATCH_LOG', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    addPicturesAndDescription({}, payload) {
        const formData = new FormData();

        for (let index = 0; index < payload.formData.photoUpload.length; index++) {
            formData.append('photo_upload[]', payload.formData.photoUpload[index].file)
        }
        formData.append('description', payload.formData.description);

        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/picture/add/${payload.id}`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                resolve(response);
            }).catch(error => {
                reject(error);
            })
        })
    },
    updateDescription({}, {id, payload}) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/description/update/${id}`, payload)
            .then((response) => {
                resolve(response);
            }).catch(error => {
                reject(error);
            })
        })
    },
    updatePicturesAndDescription({}, payload) {
        const formData = new FormData();

        for (let index = 0; index < payload.formData.photoUpload.length; index++) {
            formData.append('photo_upload[]', payload.formData.photoUpload[index].file)
        }
        for (let index = 0; index < payload.formData.oldPictures.length; index++) {
            formData.append('old_pictures[]', payload.formData.oldPictures[index].image)
        }
        formData.append('description', payload.formData.description);
        formData.append('detail_id', payload.detailId);

        return new Promise((resolve, reject) => {
            this.$axios.post(`inventory/picture/update/${payload.id}`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then((response) => {
                resolve(response);
            }).catch(error => {
                reject(error);
            })
        })
    },
}
