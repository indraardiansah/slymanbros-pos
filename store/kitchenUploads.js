export const state = () => ({
    data: [],
    items: null,
    page: 1,
    error: null
})

export const mutations = {
    SET_KITCHEN_TABLE(state, payload) {
        state.data = payload
    },
    SET_KITCHEN_DATA(state, payload) {
        state.items = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_KITCHEN_ERROR(state, payload){
        state.error = payload
    }
}

export const actions = {
    getKitchenUploads({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/kitchen-user?page=${state.page}&status=${payload.status}`)
            .then((response) => {
                commit('SET_KITCHEN_TABLE', response.data)
                resolve(response.data)
            })
        })
    },
    getKitchenUpload({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/kitchen-user/${payload}`)
            .then((response) => {
                commit('SET_KITCHEN_DATA', response.data)
                resolve(response.data)
            })
        })
    },
    updateStatus({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/kitchen-user/${payload.id}`, {status: payload.status} )
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    editTagImage({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put('/kitchen-user/add-tag', payload, {
                // headers: {
                //     'Content-Type': 'multipart/form-data'
                // }
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_KITCHEN_ERROR', error.response.data.errors)
                new Error(error)
            })
        })
    },
}