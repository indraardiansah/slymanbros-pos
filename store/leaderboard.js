export const state = () => ({
    fields: [
        {
            key: 'rank',
            label: ''
        },
        {
            key: 'salesPerson',
            label: 'Sales Person'
        },
        {
            key: 'revenue',
            label: 'Gross Product Revenue'
        },
        {
            key: 'returnRevenue',
            label: 'Gross Product Return Revenue'
        },
        {
            key: 'netRevenue',
            label: 'Net Product Revenue'
        },
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
})

export const mutations = {
    SET_LEADERBOARD_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_LEADERBOARD_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'rank',
                label: ''
            },
            {
                key: 'salesPerson',
                label: 'Sales Person'
            },
            {
                key: 'revenue',
                label: 'Gross Product Revenue'
            },
            {
                key: 'returnRevenue',
                label: 'Gross Product Return Revenue'
            },
            {
                key: 'netRevenue',
                label: 'Net Product Revenue'
            },
        ]
    },
    SET_LEADERBOARD_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_LEADERBOARD_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_LEADERBOARD_PAGE(state, payload) {
        state.page = payload
    },
}

export const actions = {
    getLeaderboards({ commit, state }, payload) {
        let perpage = payload.perpage ? payload.perpage : 10
        return this.$axios.get(`/report/leaderboard`,{
            params: {
                page: state.page,
                start_month: payload.start_month,
                end_month: payload.end_month,
                month: payload.month,
                year: payload.year,
                perpage: perpage
            }
        }
        ).then(response => {
            // const response = {
            //     data: {
            //         data: [
            //             {
            //                 salesPerson: 'Bob Slyman',
            //                 revenue: 0,
            //                 returnRevenue: 0,
            //                 netRevenue: 0
            //             },
            //             {
            //                 salesPerson: 'Bobby Slyman',
            //                 revenue: 0,
            //                 returnRevenue: 0,
            //                 netRevenue: 0
            //             },
            //             {
            //                 salesPerson: 'Chris Slyman',
            //                 revenue: 0,
            //                 returnRevenue: 0,
            //                 netRevenue: 0
            //             },
            //             {
            //                 salesPerson: 'Cody Coleman',
            //                 revenue: 0,
            //                 returnRevenue: 0,
            //                 netRevenue: 0
            //             },
            //         ],
            //         meta: {
            //             total: 2,
            //             perPage: 10
            //         }
            //     }
            // }
            commit('SET_LEADERBOARD_TABLE', response.data)
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    }
}