export const state = () => ({
    fields: [
        {
            key: 'invoice_number',
            label: 'Order/Invoice Number'
        },
        {
            key: 'invoice_date',
            label: 'Order date'
        },
        {
            key: 'store',
            label: 'Store'
        },
        {
            key: 'sales_person',
            label: 'Salesperson'
        },
        {
            key: 'customer',
            label: 'Customer'
        },
        {
            key: 'sub_total',
            label: 'Sub total'
        },
        {
            key: 'warranty_total',
            label: 'Warranty'
        },
        {
            key: 'install_total',
            label: 'Install'
        },
        {
            key: 'tax_total',
            label: 'Tax'
        },
        {
            key: 'grand_total',
            label: 'Grand total'
        },
    ],
    items: [],
    newItemsCount: 0,
    meta: {
        total: null,
        per_page: 10
    },
    perpage: 10,
    selected: null,
    page: 1,
})

export const mutations = {
    SET_LIVE_FEED_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_LIVE_FEED_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_LIVE_FEED_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_LIVE_FEED_PAGE(state, payload) {
        state.page = payload
    },
    SET_LIVE_FEED_PERPAGE(state, payload) {
        state.perpage = payload
    },
    ADD_NEW_DATA(state, payload) {
        let data = state.items
        data = [payload, ...data]
        state.items = data
    },
    SET_NEW_ITEM_COUNT(state, payload) {
        state.newItemsCount += parseInt(payload)
    }
}

export const actions = {
    GET_LIVE_FEED({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/report/live-feed?page=${state.page}&perpage=${state.perpage}`,{
                params: payload
            }
            ).then(response => {
                commit('SET_LIVE_FEED_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
                reject(error.response)
            })
        })
        
    }
}