export const state = () => ({
    // data: {
    //     firstName: null,
    //     lastName: null,
    //     company: null,
    //     email: null,
    //     billingAddress: null,
    //     shippingAddress: [],
    //     phoneNumber: []
    // },
    fields: [
        {
            key: 'code',
            label: 'Location Code'
        },
        {
            key: 'name',
            label: 'Location Name'
        },
        {
            key: 'phoneNumber',
            label: 'Location Phone Number'
        },
        {
            key: 'faxNumber',
            label: 'Location Fax'
        },
        {
            key: 'address',
            label: 'Location Address'
        },
        {
            key: 'tax',
            label: 'Location Tax Percentage'
        },
        {
            key: 'manager',
            label: 'Manager'
        },
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
})

export const mutations = {
    // SET_LOCATION_DATA(state, payload) {
    //     state.data = payload
    // },
    // SET_LOCATION_DATA_DEFAULT(state, payload) {
    //     state.data = {
    //         firstName: null,
    //         lastName: null,
    //         company: null,
    //         email: null,
    //         billingAddress: null,
    //         shippingAddress: [],
    //         phoneNumber: []
    //     }
    // },
    SET_LOCATION_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_LOCATION_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'code',
                label: 'Location Code'
            },
            {
                key: 'name',
                label: 'Location Name'
            },
            {
                key: 'phoneNumber',
                label: 'Location Phone Number'
            },
            {
                key: 'faxNumber',
                label: 'Location Fax'
            },
            {
                key: 'address',
                label: 'Location Address'
            },
            {
                key: 'tax',
                label: 'Location Tax Percentage'
            },
            {
                key: 'manager',
                label: 'Manager'
            },
        ]
    },
    SET_LOCATION_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_LOCATION_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_LOCATION_PAGE(state, payload) {
        state.page = payload
    },
}

export const actions = {
    getLocations({ commit, state }, payload) {
        return this.$axios.get(`company-location?page=${state.page}`)
        .then(response => {
            // const response = {
            //     data: {
            //         data: [
            //             {
            //                 code: 15,
            //                 name: 'Slyman Bros (Fenton)',
            //                 phoneNumber: '636-277-9010',
            //                 faxNumber: '636-717-9000',
            //                 address: '1549 Fencorp Court, Fenton, MO',
            //                 // tax: 7.62,
            //                 manager: 'Jon Slyman'
            //             },
            //             {
            //                 code: 15,
            //                 name: 'Slyman Bros (Fenton)',
            //                 phoneNumber: '636-277-9010',
            //                 faxNumber: '636-717-9000',
            //                 address: '1549 Fencorp Court, Fenton, MO',
            //                 // tax: 7.62,
            //                 manager: 'Jon Slyman'
            //             },
            //         ],
            //         meta: {
            //             total: 2,
            //             perPage: 10
            //         }
            //     }
            // }
            // console.log(response)
            commit('SET_LOCATION_TABLE', response.data)
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    getLocation({ commit }, payload) {
        // return this.$axios.get('', {
        //     params: payload
        // })
        // .then(response => {
            return new Promise((resolve, reject) => {
                const response = {
                    data: {
                        code: 15,
                        name: 'Slyman Bros (Fenton)',
                        phoneNumber: '636-277-9010',
                        faxNumber: '636-717-9000',
                        address: '1549 Fencorp Court, Fenton, MO',
                        tax: 7.62,
                        manager: 'Jon Slyman'
                    },
                }
                commit('SET_LOCATION_DATA', response.data)
                resolve(response.data)
            }) 
        // })
        // .catch(error => {
        //     console.log(error.response)
        //     new Error(error)
        // })
    },
    postLocation({ commit }, payload) {
        // return this.$axios.post('', payload)
        // .then(response => {
            return new Promise((resolve, reject) => {
                console.log('POSTED')
                resolve()
            }) 
        // })
        // .catch(error => {
        //     console.log(error.response)
        //     new Error(error)
        // })
    }
}