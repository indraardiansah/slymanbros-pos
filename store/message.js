export const state = () => ({
    data: {
        message: null,
        date: null
    },
    fields: [
        {
            key: 'message',
            label: 'Website Message',
            thStyle: {
                width: '700px'
            }
        },
        {
            key: 'active_date',
            label: 'Active Date'
        },
        {
            key: 'action',
            label: 'Action'
        }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    errors: null
})

export const mutations = {
    SET_MESSAGE_DATA(state, payload) {
        state.data = payload
    },
    SET_MESSAGE_DATA_DEFAULT(state) {
        state.data = {
            title: '',
            keyword: '',
            description: '',
            url: ''
        }
    },
    SET_MESSAGE_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_MESSAGE_TABLE_DEFAULT(state) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_MESSAGE_PAGE(state, payload) {
        state.page = payload
    },
    SET_MESSAGE_ERRORS(state, payload){
        state.errors = payload
    }
}

export const actions = {
    getMessageData({ commit, state }, payload) {
        // let type_data = ''
        // let perpage = 10
        // if(payload){
        //     type_data = payload.type ? payload.type:''
        //     perpage = payload.perpage ? payload.perpage:10
        // }
        return new Promise((resolve, reject) => {
            this.$axios.get(`/website-message?page=${state.page}`)
            .then(response => {
                commit('SET_MESSAGE_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    saveMessage({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/website-message', payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_MESSAGE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    getMessage({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/website-message/${payload}/edit`)
            .then((response) => {
                commit('SET_MESSAGE_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    updateMessage({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/website-message/${payload.id}`, payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_MESSAGE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    destroyMessage({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/website-message/${payload}`).then((response) => {
                dispatch('getMessageData').then(() => {
                    resolve(response.data)
                })
            })
        })
    }
}