export const state = () => ({
    data: {
        title: '',
        keyword: '',
        description: '',
        url: ''
    },
    fields: [
        {
            key: 'title',
            label: 'Meta Title',
            thStyle:{
                width: '300px !important'
            }
        },
        {
            key: 'description',
            label: 'Meta Description',
        },
        {
            key: 'keyword',
            label: 'Keywords',
        },
        {
            key: 'url',
            label: 'URL',
            thStyle:{
                width: '200px !important'
            }
        },
        {
            key: 'action',
            label: 'Action',
            thStyle:{
                width: '100px !important'
            }
        }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    errors: null
})

export const mutations = {
    SET_META_DATA(state, payload) {
        state.data = payload
    },
    SET_META_DATA_DEFAULT(state) {
        state.data = {
            title: '',
            keyword: '',
            description: '',
            url: ''
        }
    },
    SET_META_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_META_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_META_PAGE(state, payload) {
        state.page = payload
    },
    SET_META_ERRORS(state, payload){
        state.errors = payload
    }
}

export const actions = {
    searchMeta({commit},payload){
        return new Promise((resolve, reject) =>{
            this.$axios.post(`/meta/search`, {
                search : payload,
                headers :{
                    'Accept': 'application/json',
                }
            })
            .then(response =>{
                commit('SET_META_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)  
            })
        })
    },
    getMetaData({ commit, state }, payload) {
        // let type_data = ''
        // let perpage = 10
        // if(payload){
        //     type_data = payload.type ? payload.type:''
        //     perpage = payload.perpage ? payload.perpage:10
        // }
        return new Promise((resolve, reject) => {
            this.$axios.get(`/meta?page=${state.page}`)
            .then(response => {
                commit('SET_META_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    saveMeta({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/meta', payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_META_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    getMeta({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/meta/${payload}/edit`)
            .then((response) => {
                commit('SET_META_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    updateMeta({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/meta/${payload.id}`, payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_META_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    destroyMeta({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/meta/${payload}`).then((response) => {
                dispatch('getMetaData').then(() => {
                    resolve(response.data)
                })
            })
        })
    }
}