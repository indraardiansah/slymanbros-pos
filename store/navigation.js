
export const state = () => ({
    navigation_selected: []
})

export const mutations = {
    SET_NAVIGATION_SELECTED(state, payload){
        state.navigation_selected = payload
    }
}

export const actions = {
    async getNavigation({commit}, navigationId){
        const response = await this.$axios.get(`navigation/${navigationId}`)
        commit('SET_NAVIGATION_SELECTED', response.data)
        return response
    }
}

export const getters = {
    isShowDeliveryRouting(state) {
        const tabDeliveryRouting = state.navigation_selected.child.find((child) => child.name === 'Routing' && child.status === true);
        return !!tabDeliveryRouting;
    }
}
