import { download, convertBlobUrl } from "../utils/download"

export const state = () => ({
  data: {
    customer: null,
    user: null,
    store_id: null,
    orders: null,
    notes: [],
    return_payment: []
  },
  out_of_state: null,
  detailData: [],
  fields: [
    {
      key: 'order_date',
      label: 'Date'
    },
    {
      key: 'order_no',
      label: 'Order'
    },
    {
      key: 'bill_name',
      label: 'Customer'
    },
    {
      key: 'bill_phone_number',
      label: 'Phone Number'
    },
    {
      key: 'total',
      label: 'Total',
      class: 'text-right'
    },
    // {
    //     key: 'scheduled',
    //     label: 'Scheduled Delivery'
    // },
    // {
    //     key: 'balance_due',
    //     label: 'Balance Due',
    //     class: 'text-right'
    // }
  ],
  items: [],
  meta: {
    total: null,
    perPage: null
  },
  selected: null,
  page: 1,
  errors: null,
  filter: {
    sales_selected: '',
    store_selected: '',
    date: null,
    sales_field_selected: '',
    stores_field_selected: '',
    has_reserved: false,
    has_no_tag: false
  },
  notes: [],
  urlPDF: '',

  //return
  field_return: ['date', 'order', 'model', 'quantity', 'refund_method', 'refund_amount', 'refund_reason', { key: 'store.store_name', label: 'Location' }, { key: 'additional_note', label: 'Note' }],
  items_return: [],

  //quote
  field_quote: [
    {
      key: 'date',
      label: 'Date'
    },
    {
      key: 'quote',
      label: 'Quote'
    },
    {
      key: 'order',
      label: 'Order'
    },
    {
      key: 'customer',
      label: 'Customer'
    },
    {
      key: 'sales_person',
      label: 'Sales Person',
      thStyle: 'width: 150px !important'
    },
    {
      key: 'store_location',
      label: 'Location'
    },
    {
      key: 'status',
      label: 'Status'
    },
    {
      key: 'total',
      label: 'Total'
    }
  ],
  items_quote: [],

  field_order_feed: ['order_date', 'order', 'customer', 'sales_person', 'total'],
  items_order_feed: [],
  recordPayment: {
    data: null,
    errors: null
  },
  order_feed_filter: {
    date: null,
    sales: '',
    location: ''
  },
  subOrder: null,
  parts: null,
  damageData: null,
  drivers: [],
  damageAssignments: [],
  productDamageList: []
})

export const mutations = {
  SET_ORDER_DATA(state, payload) {
    state.data = payload
  },
  SET_ORDER_FILTER(state, payload) {
    state.filter = payload
  },
  SET_ORDER_DATA_DEFAULT(state, payload) {
    state.data = {
      customer: null,
      user: null,
      store_id: null,
      orders: null,
      notes: [],
      return_payment: []
    }
  },
  SET_ORDER_DETAIL_DATA(state, payload) {
    state.detailData = payload.map(item => { return { ...item, _showDetails: true } })
  },
  SET_ORDER_IS_OUT_OF_STATE(state, payload) {
    state.out_of_state = payload
  },
  SET_ORDER_FIELDS(state, payload) {
    state.fields = payload
  },
  SET_ORDER_FIELDS_DEFAULT(state, payload) {
    state.fields = [
      {
        key: 'order_date',
        label: 'Date'
      },
      {
        key: 'order_no',
        label: 'Order'
      },
      {
        key: 'bill_name',
        label: 'Customer'
      },
      {
        key: 'bill_phone_number',
        label: 'Phone Number'
      },
      {
        key: 'total',
        label: 'Total',
        class: 'text-right'
      },
      // {
      //     key: 'scheduled',
      //     label: 'Scheduled Delivery'
      // },
      // {
      //     key: 'balance_due',
      //     label: 'Balance Due',
      //     class: 'text-right'
      // },
      // {
      //     key: 'completed',
      //     label: 'Completed'
      // },
    ]
  },
  SET_ORDER_TABLE(state, payload) {
    state.items = payload.data
    state.meta = payload.meta
  },
  SET_ORDER_SELECTED(state, payload) {
    state.selected = payload
  },
  SET_ORDER_PAGE(state, payload) {
    state.page = payload
  },
  SET_ORDER_ERRORS(state, payload) {
    state.errors = payload
  },
  SET_ORDER_RECORD_PAYMENT_DATA(state, payload) {
    state.recordPayment.data = payload
  },
  SET_ORDER_RECORD_PAYMENT_ERRORS(state, payload) {
    state.recordPayment.errors = payload
  },
  //return
  ASSIGN_RETURN_DATA(state, payload) {
    state.items_return = payload
  },
  ASSIGN_QUOTE_DATA(state, payload) {
    state.items_quote = payload
  },
  ASSIGN_ORDER_FEED_DATA(state, payload) {
    state.items_order_feed = payload
  },
  SET_NOTES_ORDER(state, payload) {
    state.notes = payload
  },
  SET_URL_PDF(state, payload) {
    state.urlPDF = payload
  },
  SET_ORDER_FEED_FILTER(state, payload) {
    state.order_feed_filter = {
      date: payload.date,
      sales: payload.sales,
      location: payload.location
    }
  },
  SET_SUBORDER_DATA(state, payload) {
    state.subOrder = payload
  },
  SET_PARTS_DATA(state, payload) {
    let reMapPayload = payload.data.map(item => {
      return {
        ...item,
        order_date: new Date(item.order_date),
        date_delivered: item.date_delivered ? new Date(item.date_delivered) : item.date_delivered
      }
    })
    state.parts = { data: reMapPayload }
  },
  SET_DAMAGE_DATA(state, payload) {
    let formatedPayload = payload.map(i => {
      return {
        ...i,
        _showDetails: i.picture.length > 0 ? true : false,
        date_accepted: new Date(i.date_accepted)
      }
    })
    state.damageData = formatedPayload
  },
  SET_DRIVER_DATA(state, payload) {
    state.drivers = payload;
  },
  SET_DAMAGE_ASSIGNMENT_DATA(state, payload) {
    state.damageAssignments = payload;
  },
  SET_PRODUCT_DAMAGE_LIST(state, payload) {
    state.productDamageList = payload
  }
}

export const actions = {
  getOrders({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order`, { params: payload })
              .then(response => {
                  commit('SET_ORDER_TABLE', response.data)
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  changeTagProduct({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          return this.$axios.post('/order/change-tag', payload)
              .then((response) => {
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  getOrder({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`order/${payload}`)
              .then(response => {
                  let order = Object.assign({
                      change_due: response.data.change_due,
                      balance_due: response.data.balance_due,
                      returned: response.data.returned,
                      return_payment: response.data.return_payment,
                      customer_credit: response.data.customer_credit,
                      damage_waiver : response.data.damage_waiver,
                      service_call : response.data.service_call
                  }, response.data.order)
                  commit('SET_ORDER_DETAIL_DATA', response.data.detail_order)
                  commit('SET_ORDER_DATA', order)
                  resolve(order)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  postOrder({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          return this.$axios.post('/order', state.data)
              .then((response) => {
                  commit('SET_ORDER_ERRORS', null)
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  postOrderRecordPayment({ commit, state }, payload) {
      let payment = Object.assign({ type: payload }, state.recordPayment.data)
      return new Promise((resolve, reject) => {
          return this.$axios.post('/order/payment', payment)
              .then((response) => {
                  commit('SET_ORDER_RECORD_PAYMENT_ERRORS', null)
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_RECORD_PAYMENT_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  changeOrderRecordPayment({ commit, state }, payload) {
      let payment = Object.assign(payload, state.recordPayment.data)
      return new Promise((resolve, reject) => {
          this.$axios.post(`order/payment/change`, payment)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
                  reject(error.data)
              })
      })
  },
  postRepaymentCreditLine({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          return this.$axios.post('/order/pay-credit-line-debt', payload)
              .then((response) => {
                  commit('SET_ORDER_ERRORS', null)
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  // new Error(error)
                  reject(error.response.data)
              })
      })
  },
  putOrder({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.put(`order/${payload}`, {
              data: state.data,
              product_orders: state.detailData,
              out_of_state: state.out_of_state
          })
              .then(response => {
                  // commit('SET_ORDER_ERRORS', null)
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  putShippingOrder({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.put(`order/${payload}`, {
              data: state.data,
              product_orders: state.detailData,
              out_of_state: state.out_of_state,
              update_address: true
          })
              .then(response => {
                  // commit('SET_ORDER_ERRORS', null)
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  addNewProductToOrder({ commit, }, payload) {
      return new Promise((resolve, reject) => {
          return this.$axios.post('/order/add-new-product', payload)
              .then((response) => {
                  commit('SET_ORDER_ERRORS', null)
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  deleteOrder({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.delete(`order/${payload}`)
              .then((response) => {
                  resolve(response.data)
              })
      })
  },
  completeOrder({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.post('order/complete', { order_no: payload.order_no, status: payload.status })
              .then((response) => {
                  resolve(response.data)
              })
      })
  },
  addNotes({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.post('order-notes', payload)
              .then((response) => {
                  commit('SET_ORDER_ERRORS', null)
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  editNotes({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`order-notes/${payload}`)
              .then((response) => {
                  resolve(response.data.data)
              })
      })
  },
  updateNotes({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.put(`order-notes/${payload.id}`, { note: payload.note })
              .then((response) => {
                  commit('SET_ORDER_ERRORS', null)
                  resolve(response.data.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors.note[0])
                  new Error(error)
              })
      })
  },
  deleteNotes({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.delete(`order-notes/${payload}`)
              .then((response) => {
                  resolve(response.data.data)
              })
      })
  },
  sendMail({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.post('/order/send-mail', payload)
              .then((response) => {
                  resolve(response.data)
              })
      })
  },
  getPdfWithoutSpec({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order/invoice`, {
              headers: {
                  'Accept': 'application/pdf',
                  "Access-Control-Allow-Origin": "*"
              },
              mode: 'no-cors',
              responseType: 'text',
              params: { ...payload }
          })
              .then((response) => {
                  commit('SET_URL_PDF', response.data.data)
                  resolve(response.data)
              })
      })
  },
  getPdfWithoutModel({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order/invoice-with-spec-without-model`, {
              headers: {
                  'Accept': 'application/pdf',
                  "Access-Control-Allow-Origin": "*"
              },
              mode: 'no-cors',
              responseType: 'text',
              params: { ...payload }
          })
              .then((response) => {
                  commit('SET_URL_PDF', response.data.data)
                  resolve(response.data)
              })
      })
  },
  getPdfWithSpec({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order/invoice-with-spec`, {
              headers: {
                  'Accept': 'application/pdf',
                  "Access-Control-Allow-Origin": "*"
              },
              mode: 'no-cors',
              responseType: 'text',
              params: { ...payload }
          })
              .then((response) => {
                  commit('SET_URL_PDF', response.data.data)
                  resolve(response.data)
              })
      })
  },
  getPdfWithoutPrice({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order/invoice-without-price`, {
              headers: {
                  'Accept': 'application/pdf',
                  "Access-Control-Allow-Origin": "*"
              },
              mode: 'no-cors',
              responseType: 'text',
              params: { ...payload }
          })
              .then((response) => {
                  commit('SET_URL_PDF', response.data.data)
                  resolve(response.data)
              })
      })
  },

  //RETURN FEED
  getReturnFeed({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/return-feed?from=${payload.from}&to=${payload.to}&refund_method=${payload.refund_method}&location=${payload.store}`).then((response) => {
              commit('ASSIGN_RETURN_DATA', response.data)
              resolve(response.data)
          })
      })
  },

  //QUOTE FEED
  getQuoteFeed({ commit, state }, payload) {
      let page = payload.page ? payload.page : state.page
      let from = payload.from == null ? '' : payload.from
      let to = payload.to == null ? '' : payload.to
      let perpage = payload.perpage == null ? '' : payload.perpage
      return new Promise((resolve, reject) => {
          this.$axios.get(`/quote-feed?page=${page}&from=${from}&to=${to}&location=${payload.location}&sales=${payload.sales}&perpage=${perpage}&type=${payload.type}`).then((response) => {
              commit('ASSIGN_QUOTE_DATA', response.data)
              resolve(response.data)
          })
      })
  },

  //ORDER FEED
  getOrderFeed({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order-feed`, { params: payload })
              .then((response) => {
                  commit('ASSIGN_ORDER_FEED_DATA', response.data)
                  resolve(response.data)
              })
      })
  },

  getSubOrder({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/sub-order/${payload}`)
              .then(response => {
                  commit('SET_SUBORDER_DATA', response.data)
                  resolve(response.data)
              })
      })
  },
  putSubOrder({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.put(`/sub-order/${payload}`, state.subOrder)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  payInstallWarranty({}, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.post(`/sub-order/${payload.order_no}/payment`, payload)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  changeStatusSubOrder({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.put(`/sub-order/${payload}/status`)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  createParts({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.post(`/order/part/${payload.order_no}`, payload.data)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  getParts({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order/part/${payload}`)
              .then(response => {
                  commit('SET_PARTS_DATA', response.data)
                  resolve(response.data)
              })
      })
  },
  putParts({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.put(`/order/part/${payload.id}`, payload)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  deleteParts({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.delete(`/order/part/${payload}`)
              .then((response) => {
                  resolve(response.data)
              })
      })
  },
  getDamageAssignment({commit}) {
    return new Promise((resolve, reject) => {
        this.$axios.get(`/damage-assignment`)
            .then(response => {
                commit('SET_DAMAGE_ASSIGNMENT_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
    })
  },
  getDamage({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order/damage-allowance/${payload}`)
              .then(response => {
                  commit('SET_DAMAGE_DATA', response.data.data)
                  commit('SET_DRIVER_DATA', response.data.drivers)
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  createDamage({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.post(`/order/damage-allowance/${payload.order_no}`, payload.data)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  getProductDamage({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`/order/damage-allowance/${payload}/products`)
              .then(response => {
                  commit('SET_PRODUCT_DAMAGE_LIST', response.data)
                  resolve(response.data)
              })
              .catch(error => {
                  new Error(error)
              })
      })
  },
  deleteDamage({ commit }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.delete(`/order/damage-allowance/${payload}`)
              .then((response) => {
                  resolve(response.data)
              })
      })
  },
  putDamage({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.put(`/order/damage-allowance/${payload.id}`, payload)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  putDamageAllowanceReport({ commit, state }, payload) {
      return new Promise((resolve) => {
          this.$axios.put(`/order/damage-allowance/report/${payload.id}`, payload)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  commit('SET_ORDER_ERRORS', error.response.data.errors)
                  new Error(error)
              })
      })
  },
  saveDamageMemo({ }, payload) {
      const { id, credit_memo } = payload
      return new Promise((resolve, reject) => {
          this.$axios.post(`order/damage-allowance/credit-memo/add/${id}`, { credit_memo })
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  reject(error.response)
                  new Error(error.response)
              })
      })
  },
  deleteDamageMemo({ }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.delete(`order/damage-allowance/credit-memo/delete/${payload}`)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  reject(error.response)
                  new Error(error.response)
              })
      })
  },
  updateModelOrder({ }, payload) {
      const { id, product } = payload
      return new Promise((resolve, reject) => {
          this.$axios.put(`order/detail-order/update/${id}`, { product: product })
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  reject(error.response)
                  new Error(error.response)
              })
      })
  },
  getOnOrderDetail({ }, payload) {
      return new Promise((resolve, reject) => {
          this.$axios.get(`order/on-order-detail/${payload}`)
              .then(response => {
                  resolve(response.data)
              })
              .catch(error => {
                  reject(error)
              })
      })
  },
    saveOrderCustomNotification({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.post(`order/custom-notification/store/${payload.id}`, payload.form)
            .then((response) => {
                dispatch(`getOrder`, payload.idOrder);
                resolve(response.data)
            }).catch((error) => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    updateOrderCustomNotification({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.put(`order/custom-notification/update/${payload.id}`, payload.form)
            .then((response) => {
                dispatch(`getOrder`, payload.idOrder);
                resolve(response.data)
            }).catch((error) => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    downloadImage({}, payload){
        const { url, type, name } = payload
        return new Promise((resolve, reject) => {
            return this.$axios.get(`${url}`,
            {
                responseType: type === 'all' ? 'json' : 'blob',
                params: {
                    type
                }
            })
            .then(response => {
                const res = response.data
                if(type === 'all') download(res.url, res.name)
                else convertBlobUrl(res, name)
                resolve(res)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    createQuoteFromOrder({}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post(`quote/create/${payload}`)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}

