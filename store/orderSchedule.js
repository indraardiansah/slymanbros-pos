export const state = () => ({
    data: '',
    selected : null,
    page: 1,
    fileUrl: null,
    items_schedule_delivery: {
        data: [],
        meta: null
    },
    field_schedule_delivery: [
        { key: 'status', label: '', thStyle: {width: '150px'}},
        { key: 'order', label: 'Order #', thStyle: { width: '8%'}},
        { key: 'delivery_method', label: 'Delivery Method', thStyle: { width: '20%'}},
        { key: 'location', label: 'Inv. Location', thStyle: { width: '13%'}},
        { key: 'sales_person', label: 'Sales Person'},
        'customer', 
        { key: 'of_pieces', label: '# of Pieces'},
        'zip_code', 
        { key: 'delivery', label: 'Delivery Date'}
    ],
    preview:{
        fields: [
            {
                key: 'product_model',
                label: 'Model',
                stickyColumn: true, 
                isRowHeader: true,
                variant: 'light'
            },
            {
                key: 'qty',
                label: 'Qty'
            },
            {
                key: 'ship_phone_number',
                label: 'Delivery Mobile Phone Number',
            },
            {
                key: 'ship_name',
                label: 'Delivery First and Last Name'
            },
            {
                key: 'ship_address',
                label: 'Delivery Address'
            },
            {
                key: 'ship_city',
                label: 'Delivery City'
            },
            {
                key: 'ship_state',
                label: 'Delivery State'
            },
            {
                key: 'ship_postal_code',
                label: 'Delivery Zip Code'
            },
            {
                key: 'order_no',
                label: 'Order #'
            },
            {
                key: 'delivery_date',
                label: 'Delivery Date'
            },
            {
                key: 'note1',
                label: 'Notes 1'
            },
            {
                key: 'note2',
                label: 'Notes 2'
            },
            {
                key: 'note3',
                label: 'Notes 3'
            },
            {
                key: 'mfg',
                label: 'Mfg'
            },
            {
                key: 'billing_phone',
                label: 'Billing Phone Number'
            },
            {
                key: 'billing_name',
                label: 'Billing First and Last Name'
            },
            {
                key: 'store_name',
                label: 'Location'
            },
            {
                key: 'install',
                label: 'Install'
            },
            {
                key: 'sales_person',
                label: 'Sales Person'
            },
        ],
        items: [],
        meta: null
    },
    countNewItem : null
})

export const mutations = {
    ASSIGN_SHOW_DATA(state, payload) {
        state.data = payload
    },
    ASSIGN_SCHEDULE_DELIVERY_ORDER(state, payload) {
        state.items_schedule_delivery = payload
    },
    ASSIGN_SHOW_SCHEDULE_DELIVERY(state, payload) {
        state.data = payload
    },
    SET_SELECTED_DELIVERY(state, payload){
        state.selected = payload
    },
    SET_SCHEDULE_DELIVERY_PAGE(state, payload){
        state.page = payload
    },
    SET_FILE_URL(state, payload){
        state.fileUrl = payload
    },
    SET_PREVIEW_DATA(state, payload){
        state.preview.items = payload.data
        state.preview.meta = payload.meta
    },
    SET_DELIVERY_COUNTER(state, payload){
        state.countNewItem = payload.not_exported
    }
}

export const actions = {
    //SCHEDULE DELIVERY ORDER
    getScheduleDeliveryFeed({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/schedule-delivery`, {
                params: {
                    page: state.page,
                    // delivery_date: payload.date,
                    // skip: payload.skip ? payload.skip : null,
                    add: payload.add ? payload.add : null,
                    start: payload.date.start,
                    end: payload.date.end,
                    delivery_method: payload.delivery_method,
                    perpage: payload.perpage,
                    sales: payload.sales ? payload.sales : null,
                    location: payload.location ? payload.location : null,
                    q: payload.q
                }
            })
            .then((response) => {
                commit('ASSIGN_SCHEDULE_DELIVERY_ORDER', response.data)
                resolve(response.data)
            })
        })
    },
    getDeliveryCounter({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/schedule-delivery`)
            .then((response) => {
                commit('SET_DELIVERY_COUNTER', response.data.meta)
                resolve(response.data)
            }).catch((e)=>{
                commit('SET_DELIVERY_COUNTER', {
                    "meta": {
                        "current_page": 1,
                        "from": null,
                        "last_page": 1,
                        "path": "http://api.slymanbros.slymanmedia.com/api/schedule-delivery",
                        "per_page": 10,
                        "to": null,
                        "total": 0,
                        "not_exported": 0
                    }
                })
                resolve({
                    "meta": {
                        "current_page": 1,
                        "from": null,
                        "last_page": 1,
                        "path": "http://api.slymanbros.slymanmedia.com/api/schedule-delivery",
                        "per_page": 10,
                        "to": null,
                        "total": 0,
                        "not_exported": 0
                    }
                })
            })
        })
    },
    showDelivery({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/schedule-delivery/${payload}`).then((response) => {
                commit('ASSIGN_SHOW_SCHEDULE_DELIVERY', response.data)
                resolve(response.data)
            })
        })
    },
    updateScheduleDelivery({ rootState, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/schedule-delivery/${state.data.order_no}`, {
                data: state.data
            }).then((response) => {
                resolve(response.data)
            })
        })
    },
    postNotes({}, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/schedule-delivery-notes', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateNote({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/edit-schedule-delivery-notes`, payload, {
                data: state.data
            }).then((response) => {
                resolve(response.data)
            })
        })
    },
    destroyNote({ rootState, state, commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/schedule-delivery-notes/${payload.id}`).then((response) => {
                resolve(response.data)
            })
        })
    },
    loadTruckDelivery({ rootState, commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/schedule-delivery-load-truck?delivery_date=${payload}`).then((response) => {
                resolve(response.data)
            })
        })
    },
    postLoadTruckDelivery({ rootState, commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`/schedule-delivery-load-truck`, payload).then((response) => {
                resolve(response.data)
            })
        })
    },
    // postNotes({ rootState, state }, payload) {
    //     return new Promise((resolve, reject) => {
    //         this.$axios.post(`/schedule-delivery-notes`, payload, {
    //         }, {
    //             headers: {
    //                 'Content-Type': 'application/json',
    //                 Authorization: 'Bearer ' + rootState.api_token
    //             }
    //         }).then((response) => {
    //             console.log(payload)
    //             resolve(response.data)
    //         })
    //     })
    // }
    getCsvPreview({commit}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`schedule-delivery/preview`, {
                params: {
                    start: payload.date.start,
                    end: payload.date.end,
                    delivery_method: payload.delivery_method  ? payload.delivery_method : '',
                    sales: payload.sales ? payload.sales : '',
                    location: payload.location ? payload.location : '',
                    q: payload.q ? payload.q : ''
                }
            })
            .then(response =>{
                commit('SET_PREVIEW_DATA', response.data)
                resolve(response.data)
            })
            .catch(error =>{
                new Error(error)
            })
        })
    },
    getCSV({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/schedule-delivery/get-csv`, {
                params: payload,
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
            }).then((res) => {
                commit('SET_FILE_URL', res.data.data)
                resolve(res.data)
            })
        })
    },
    getCSVAlt({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/schedule-delivery/get-csv-alt`, {
                params: payload,
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
            }).then((res) => {
                commit('SET_FILE_URL', res.data.data)
                resolve(res.data)
            })
        })
    },
    downloadCSV({ commit }, payload) {
        let type = payload && typeof payload.type != 'undefined' ? 'company':''
        return new Promise((resolve, reject) => {
            this.$axios.get(payload.url,{
                responseType: 'blob', // important
            }
              ).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                if (type == 'company') {
                    link.setAttribute('download', 'employees.csv');
                } else {
                    link.setAttribute('download', payload.start+'-'+payload.end+'.csv');
                }
                document.body.appendChild(link);
                link.click();
              });
        })
    }
}