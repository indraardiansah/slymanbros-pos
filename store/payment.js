export const state = () => ({
    items: [],
    payment_methods: [],
    fields: ['payment_date', 'sales_person', { key: 'order_no', label: 'Order Number' }, 'location', 'payment_method', 'amount'],
})

export const mutations = {
    SET_PAYMENT_ITEMS(state, payload) {
        state.items = payload
    },
    SET_PAYMENT_ITEMS_DEFAULT(state, payload) {
        state.items = []
    },
    SET_PAYMENT_METHOD_DATA(state, payload) {
        state.payment_methods = payload
    },
    SET_PAYMENT_METHOD_DEFAULT(state, payload) {
        state.payment_methods = []
    },
}

export const actions = {
    getPaymentMethods({ state, commit }, payload) {
        return this.$axios.get(`/payment/payment-list`)
            .then(response => {
                commit('SET_PAYMENT_METHOD_DATA', response.data)
                return response.data
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
    },
    getPaymentData({ state, commit }, payload) {
        return this.$axios.get(`/report/payment-order-report`, {
            params: payload
        })
            .then(response => {
                commit('SET_PAYMENT_ITEMS', response.data)
                return response.data
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
    },
}
