import { reject } from "lodash"

export const state = () => ({
    users: {
        data: [],
        meta: {
            total: 0,
            per_page: 10
        }
    },
    inactive: [],
    errors : null,
    navigation: [],
    feature: [],
    user_feature: [],
    page: 1
})

export const mutations = {
    SET_USERS(state, payload) {
        state.users = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_FEATURE(state, payload) {
        state.feature = payload
    },
    SET_FEATURE_DEFAULT(state, payload) {
        state.feature = []
    },
    SET_NAVIGATION(state, payload) {
        state.navigation = payload
    },
    SET_NAVIGATION_DEFAULT(state, payload) {
        state.navigation = []
    },
    SET_NAVIGATION_ERRORS(state, payload){
        state.errors = payload
    },
    SET_INACTIVE_NAVIGATION(state, payload){
        state.inactive = payload
    },
    SET_USER_FEATURE(state, payload){
        state.user_feature = payload
    }
}

export const actions = {
    getUsers({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`user-permission`, {
                params: {
                    search: payload && typeof payload.search != 'undefined' ? payload.search:'',
                    page: state.page,
                    type: payload && typeof payload.type != 'undefined' ? payload.type:'paginate',
                    perpage: payload == null ? '' : payload.perpage
                }
            })
            .then((response) => {
                commit('SET_USERS', response.data)
                resolve(response.data)
            })
        })
    },
    getNavigation({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get('user-permission/navigation')
            .then((response) => {
                commit('SET_NAVIGATION', response.data.data)
                resolve(response.data)
            })
        })
    },
    getFeaturePermission({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get('user-permission/features')
            .then((response) => {
                commit('SET_FEATURE', response.data)
                resolve(response.data)
            })
        })
    },
    postPermission({ commit }, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post('/user-permission', payload)
            .then((response) => {
                resolve(response.data)
                commit('SET_NAVIGATION_ERRORS', null)
            })
            .catch(error => {
                commit('SET_NAVIGATION_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    getInactiveMenu({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/user-permission/get-user-menu/${payload}`)
            .then((response) => {
                commit('SET_INACTIVE_NAVIGATION', response.data.data)
                resolve(response.data)
            })
        })
    },
    getUserFeature({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/user-permission/get-user-feature/${payload}`)
            .then((response) => {
                commit('SET_USER_FEATURE', response.data)
                resolve(response.data)
            })
        })
    },
    setHouseDealPerimission({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.put(`user-permission/house-deal-button/${payload.user_id}`, {status: payload.status})
            .then(response => {
                resolve()
            })
        })
    }
}