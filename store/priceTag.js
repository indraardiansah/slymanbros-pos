export const state = () => ({
    location: null,
    templatePreview: null,
    list_location: [],
    logs_filter:{
        type: [],
        results: []
    },
    template_filter: {
        size: [],
        color: []
    },
    item_product: {
        data: [],
        meta: null
    },
    item_esl: {
        data: [],
        meta: null
    },
    item_draft: {
        data: [],
        meta: null
    },
    item_gateway: {
        data: [],
        meta: null
    },
    item_history: {
        data: [],
        meta: null
    },
    item_template: {
        data: [],
        meta: null
    },
    templateList: [],
    fields: {
        location: [
            {
                key: 'name',
                label: 'Store Name'
            },
            {
                key: 'address'
            },
            {
                key: 'current_location'
            }
        ],
        product: [
            {
                key: 'checkbox',
                thStyle: {
                    width: '80px'
                }
            },
            {
                key: 'id',
                label: 'ID'
            },
            {
                key: 'label3',
                label: 'Number'
            },
            {
                key: 'label4',
                label: 'Name',
                thStyle: {
                    width: '350px'
                }
            },
            {
                key: 'label5',
                label: 'Name Abbr'
            },
            {
                key: 'label6',
                label: 'Price ($)'
            },
            {
                key: 'status',
                label: 'Status'
            },
            {
                key: 'action',
            }
        ],
        esl: [
            {
                key: 'checkbox',
                thStyle: {
                    width: '80px'
                }
            },
            {
                key: 'mac',
                label: 'Mac Address'
            },
            {
                key: 'screenSize',
                label: 'Size(")',
                thStyle: {
                    width: '80px'
                }
            },
            {
                key: 'battery',
                label: 'Battery level(%)'
            },
            {
                key: 'correspond.gatewayMac',
                label: 'Gateway binding'
            },
            {
                key: 'type',
                label: 'Status',
                thStyle: {
                    width: '100px'
                }
            },
            {
                key: 'product.id',
                label: 'Model number'
            },
            {
                key: 'correspond',
                label: 'Last communication time'
            },
            {
                key: 'lastupdate',
                label: 'Last update time'
            },
            {
                key: 'action'
            }
        ],
        gateway: [
            {
                key: 'name'
            },
            {
                key: 'mac',
                label: 'Mac Address'
            },
            {
                key: 'mode',
                label: 'Status'
            },
            {
                key: 'updateTime',
                label: 'Last update time'
            },
            {
                key: 'subModel',
                label: 'Model'
            },
            {
                key: 'wifiVersion',
                label: 'Wifi Firmware Version'
            },
            {
                key: 'bleVersion',
                label: 'Ble Firmware Version'
            },
            {
                key: 'action'
            }
        ],
        history: [
            {
                key: 'labelMac',
                label: 'ESL Mac Address'
            },
            {
                key: 'commodity',
                label: 'ID'
            },
            {
                key: 'createTime',
                label: 'Update Time'
            },
            {
                key: 'operation',
                label: 'Operation Type'
            },
            {
                key: 'result',
                label: 'Operation Result'
            },
            {
                key: 'operator'
            }
        ]
    },
    productSelected: null
})

export const getters = {
    listLocation(state){
        return state.list_location
    },
    productItem(state){
        return state.item_product
    },
    eslItem(state){
        return state.item_esl
    },
    eslDraftData(state){
        return state.item_draft
    },
    gatewayItem(state){
        return state.item_gateway
    },
    historyItem(state){
        return state.item_history
    },
    templateList(state){
        return state.templateList
    },
    templateItem(state){
        return state.item_template
    },
    fields(state){
        return state.fields
    },
    logsFilter(state){
        return state.logs_filter
    },
    templateFilter(state){
        return state.template_filter
    },
    templatePreview(state){
        return state.templatePreview
    },
    productSelected(state){
        return state.productSelected
    },
}

export const mutations = {
    SET_LIST_LOCATION(state, payload){
        state.list_location = payload
    },
    SET_LOCATION_SELECTED(state, payload){
        state.location = payload
    },
    SET_PRODUCT_DATA(state, payload){
        const {rows, ...rest} = payload
        state.item_product.data = rows
        state.item_product.meta = rest
    },
    SET_ESL_DATA(state, payload){
        const {list, ...rest} = payload
        state.item_esl.data = list
        state.item_esl.meta = rest
    },
    SET_TEMPLATE_LIST(state, payload){
        state.templateList = payload
    },
    SET_DRAFT_DATA(state, payload){
        const {data, ...rest} = payload
        state.item_draft.data = data
        state.item_draft.meta = rest
    },
    SET_GATEWAY_DATA(state, payload){
        const {list, ...rest} = payload
        state.item_gateway.data = list
        state.item_gateway.meta = rest
    },
    SET_LOGS_FILTER(state, payload){
        const {type, results} = payload
        type.unshift({id: 0, name: 'All operation types'})
        results.unshift({id:0, name: 'All operation results'})
        state.logs_filter.type = type
        state.logs_filter.results = results
    },
    SET_HISTORY_DATA(state, payload){
        const {list, ...rest} = payload
        state.item_history.data = list
        state.item_history.meta = rest
    },
    SET_TEMPLATE_FILTER(state, payload){
        const {size, color} = payload
        size.unshift({id: 0, name: 'All sizes'})
        color.unshift({id:0, name: 'All colors'})
        state.template_filter.size = size
        state.template_filter.color = color
    },
    SET_TEMPLATE_DATA(state, payload){
        const {rows, ...rest} = payload
        let formatedRows = rows.map(o => {
            return {
                ...o,
                demoData: JSON.parse(o.demoData)
            }
        })
        state.item_template.data = formatedRows
        state.item_template.meta = rest
    },
    SET_TEMPLATE_PREVIEW(state, payload){
        state.templatePreview = payload
        if(payload != undefined){
            let formatData = JSON.parse(payload.demoData)
            state.templatePreview['demoData'] = formatData
        }
    },
    SET_PRODUCT_SELECTED(state, payload){
        state.productSelected = payload
    }
}

export const actions = {
    async authPriceTag(){
        const response = await this.$axios.get('price-tag/auth')
        return response
    },
    async getPriceTagStores({commit}, payload){
        // let selectFirstStore = payload.set_first_store != undefined && payload.set_first_store ? true : false
        const response = await this.$axios.get('price-tag/store', {
            params: {
                search : payload != undefined ? payload.search : ''
            }
        })
        if(response){
            commit('SET_LIST_LOCATION', response.data.list)
            // if(selectFirstStore){
            //     commit('SET_LOCATION_SELECTED', response.data.list[0])
            // }
        }
    },
    getOverviewGateway({state}){
        const response = this.$axios.get(`price-tag/overview/gateway`, {
            params: {
                store_id: state.location.id
            }
        })
        return response
    },
    getOverviewEsl({state}){
        const response = this.$axios.get(`price-tag/overview/esl`, {
            params: {
                store_id: state.location.id
            }
        })
        return response
    },
    getOverviewUpdate({state}){
        const response = this.$axios.get(`price-tag/overview/last-update`, {
            params: {
                store_id: state.location.id
            }
        })
        return response
    },
    getOverviewBattery({state}){
        const response = this.$axios.get(`price-tag/overview/battery`, {
            params: {
                store_id: state.location.id
            }
        })
        return response
    },
    async getPriceTagProduct({commit, state}, payload){
        const response = await this.$axios.get(`price-tag/product`, {
            params: {
                store_id: state.location.id,
                ...payload
            }
        })
        if(response){
            commit('SET_PRODUCT_DATA', response.data)
        }
        return response
    },
    addPriceTagProduct({state}, payload){
        const response = this.$axios.post(`price-tag/product`, {...payload, storeUuid : state.location.uuid})
        return response
    },
    updatePriceTagProduct({state}, payload){
        const response = this.$axios.put(`price-tag/product`, {...payload}, {params: {store_id: state.location.id}})
        return response
    },
    async getEslTemplatePreview({commit, state}, payload){
        const response = await this.$axios.get(`price-tag/product/show-esl`, {params : {store_id : state.location.id, ...payload}})
        if(response){
            commit('SET_TEMPLATE_PREVIEW', response.data.body.data[0])
        }
        return response
    },
    async deleteEslProduct({state}, payload){
        const response = await this.$axios.delete(`price-tag/product/esl/${payload}`, {params : {store_id : state.location.id}})
        return response
    },
    changeLed({state}, payload) {
        const response = this.$axios.get(`price-tag/led/${payload.mac}`, {params: {store_id: state.location.id, color: payload.color}})
        return response
    },
    refreshProduct({state}, payload){
        const response = this.$axios.get(`price-tag/product/refresh`, {params: {store_id: state.location.id, ...payload}})
        return response
    },
    deleteProduct({state}, payload){
        const response = this.$axios.delete(`price-tag/product/${payload}`, {params: {store_id: state.location.id}})
        return response
    },
    deleteProductBatch({state}, payload){
        const response = this.$axios.post(`price-tag/product/delete/batch`, {...payload, store_id: state.location.id, _method: 'delete' })
        return response
    },
    async getEslList({state, commit}, payload){
        const response = await this.$axios.get(`price-tag/esl`, {params: {...payload, store_id: state.location.id}})
        if(response){
            commit('SET_ESL_DATA', response.data)
        }
    },
    singleBindEsl({}, payload){
        const response = this.$axios.get(`price-tag/esl/bind`, {params: {...payload}})
        return response
    },
    bindAllEsl({state}){
        const response = this.$axios.get(`price-tag/esl/bind-all`, {params: {store_id: state.location.id}})
        return response
    },
    unboundRebindEsl({state}){
        const response = this.$axios.get(`price-tag/esl/unbound`, {params: {store_id: state.location.id}})
        return response
    },
    getProductByMac({state}, payload){
        const response = this.$axios.get(`price-tag/esl/product`, { params: {...payload, store_id: state.location.id}})
        return response
    },
    async getTemplateList({state, commit}, payload){
        const response = await this.$axios.get(`price-tag/template/list`, { params: {...payload, store_id: state.location.id}})
        if(response){
            commit('SET_TEMPLATE_LIST', response.data.body)
        }
        return response
    },
    singleApplyTemplate({state}, payload){
        const response = this.$axios.post(`price-tag/template/apply`, {...payload, store_id: state.location.id, storeUuid : state.location.uuid})
        return response
    },
    createBulkEsl({state}, payload){
        const response = this.$axios.post(`price-tag/esl/draft`, {...payload, storeUuid: state.location.uuid})
        return response
    },
    async getEslDraft({commit}, payload){
        const response = await this.$axios.get('price-tag/esl/draft', {params: {...payload}})
        if(response){
            commit('SET_DRAFT_DATA', response.data)
        }
        return response
    },
    deleteEslDraft({}, payload){
        const response = this.$axios.delete(`price-tag/esl/draft/${payload.id}`, {params: {all: payload.all}})
        return response
    },
    addEsl({}){
        const response = this.$axios.post(`price-tag/esl`)  
        return response
    },
    deleteEslDevice({state}, payload){
        const response = this.$axios.delete(`price-tag/esl/${payload}`, {params: {store_id: state.location.id}})
        return response
    },
    deleteEslBatch({state}, payload){
        const response = this.$axios.post(`price-tag/esl/delete/batch`, {...payload, store_id: state.location.id, _method: 'delete' })
        return response
    },
    async getGateway({state, commit}, payload){
        const response = await this.$axios.get(`price-tag/gateway`, {params: {...payload, store_id: state.location.id}})
        if(response){
            commit('SET_GATEWAY_DATA', response.data)
        }
        return response
    },
    createGateway({state}, payload){
        const response = this.$axios.post(`price-tag/gateway`, {...payload, storeId: state.location.id})
        return response
    },
    updateGateway({state}, payload){
        const response = this.$axios.put(`price-tag/gateway`, {...payload, storeId: state.location.id})
        return response
    },
    deleteGateway({}, payload){
        const response = this.$axios.delete(`price-tag/gateway/${payload}`)
        return response
    },
    rebootGateway({}, payload){
        const response = this.$axios.get(`price-tag/gateway/reboot`, {params: {...payload}})
        return response
    },
    async getLogsFilter({commit}){
        const response = await this.$axios.get(`price-tag/logs/filter`)
        if(response){
            commit('SET_LOGS_FILTER', response.data)
        }
    },
    async getHistory({commit, state}, payload){
        const response = await this.$axios.get(`price-tag/logs`, {params: {...payload, store_id: state.location.id}})
        if(response){
            commit('SET_HISTORY_DATA', response.data)
        }
        return response
    },
    async getTemplateFilter({commit}){
        const response = await this.$axios.get(`price-tag/template/filter`)
        if(response){
            commit('SET_TEMPLATE_FILTER', response.data)
        }
        return response
    },
    async getTemplateData({commit, state}, payload){
        const response = await this.$axios.get(`price-tag/template`, {params: {...payload, store_id: state.location.id}})
        if(response){
            commit('SET_TEMPLATE_DATA', response.data.body)
        }
        return response
    },
    applyTemplateAll({state}, payload){
        const response = this.$axios.get(`price-tag/template/apply`, {params: {...payload, store_id : state.location.id, storeUuid: state.location.uuid}})
        return response
    },
    async getActiveStore({commit}){
        const response = await this.$axios.get('user/esl/active')
        commit('SET_LOCATION_SELECTED', response.data.store)
    },
    async setActiveStore({}, payload){
        const response = await this.$axios.post('user/esl/active', {store: payload})
        return response
    }
}