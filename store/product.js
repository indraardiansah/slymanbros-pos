export const state = () => ({
    data: {
        product_id: null,
        barcode: null,
        short_description: null,
        cost: null,
        is_taxed: null,
        msrp_price: null,
        category_name: null,
        subcategory_name: null,
        detail_category_name: null,
        id: null,
        max_height: null,
        max_width: null
    },
    fields: [
        {
            key: 'short_description',
            label: 'Product Name',
            thStyle:{
                width: '500px !important'
            }
        },
        {
            key: 'product_id',
            label: 'Model #'
        },
        {
            key: 'barcode',
            label: 'Barcode'
        },
        {
            key: 'cost',
            label: 'Cost'
        },
        {
            key: 'msrp_price',
            label: 'Price'
        },
        {
            key: 'is_taxed',
            label: 'Taxed'
        },
        // {
        //     key: 'category_name',
        //     label: 'Category'
        // },
        // {
        //     key: 'subcategory_name',
        //     label: 'Sub Category'
        // },
        // {
        //     key: 'detail_category_name',
        //     label: 'Detail Category'
        // },
        // {
        //     key: 'show_detail',
        //     label: 'Show'
        // },
        // {
        //     key: 'action',
        //     label: 'Action'
        // }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    settings: {
        max_height: null,
        max_width: null
    },
    errors: null,
    stock: null,
    searchProduct:null,
    product_lowest_price: null,
    priceHistory: {
        fields : [
            {
                key: 'date_created', 
                sortable: true, 
                label: 'Date Sold'
            },
            {
                key: 'quantity', 
                sortable: false, 
                // label: 'Date Sold'
            },
            {
                key: 'order_no', 
                sortable: true, 
                label: 'Order No'
            },
            {
                key: 'store_name', 
                label: 'Location',
                sortable: true,
            },
            {
                key: 'fullname', 
                label: 'Sales Person',
                sortable: true,
            },
            {
                key: 'price', 
                label: 'Sold Price',
                sortable: true,
            },
            {
                key: 'delivery_date', 
                label: 'Delivery Date',
                sortable: true,
            }
        ],
        items: [],
        meta: {
            total: null,
            perPage: null,
            current_page: null,
            last_page: null,
            from: null,
            to: null
        },
        page: 1
    },
    productDetail: null,
    listProduct: []
})

export const mutations = {
    SET_SETTING_DATA(state, payload) {
        state.data = payload
    },
    SET_SETTING_DATA_DEFAULT(state, payload) {
        state.data = {
            id: null,
            max_height: null,
            max_width: null
        }
    },
    SET_PRODUCT_DATA(state, payload) {
        state.data = payload
    },
    SET_PRODUCT_LOWEST_PRICE(state, payload) {
        state.product_lowest_price = payload
    },
    SET_PRODUCT_DATA_DEFAULT(state, payload) {
        state.data = {
            product_id: null,
            barcode: null,
            short_description: null,
            cost: null,
            is_taxed: null,
            msrp_price: null,
            category_name: null,
            subcategory_name: null,
            detail_category_name: null
        }
    },
    SET_PRODUCT_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_PRODUCT_TABLE_DEFAULT(state) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
    },
    SET_PRODUCT_PAGE(state, payload) {
        state.page = payload
    },
    SET_PRODUCT_ERRORS(state, payload) {
        state.errors = payload
    },
    SET_PRODUCT_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_PRODUCT_SETTINGS(state, payload) {
        state.settings = payload
    },
    SET_PRODUCT_STOCK_ORDER(state, payload) {
        state.stock = payload
    },
    SET_SEARCH_PRODUCT_DATA(state, payload){
        state.searchProduct = payload
    },
    SET_LIST_PRODUCT(state, payload){
        state.listProduct = payload
    },
    SET_PRICE_HISTORY_TABLE(state, payload){
        state.priceHistory.items = payload.data
        state.priceHistory.meta.perPage = payload.per_page
        state.priceHistory.meta.total = payload.total
        state.priceHistory.meta.current_page = payload.current_page
        state.priceHistory.meta.last_page = payload.last_page
        state.priceHistory.meta.from = payload.from
        state.priceHistory.meta.to = payload.to
    },
    SET_PRICE_HISTORY_PAGE(state, payload){
        state.priceHistory.page = payload
    },
    SET_SEARCH_PRODUCT_DETAIL(state, payload){
        state.productDetail = payload
    }
}

export const actions = {
    getProducts({ commit, state }, payload) {
        return this.$axios.get('/product', {
            params: {
                page: state.page,
                product_id: typeof payload.product_id == 'undefined' ? '' : payload.product_id,
                type: typeof payload.type == 'undefined' ? '' : 'all',
                perpage: payload == null ? '' : payload.perpage
            }
        })
        .then((response) => {
            commit('SET_PRODUCT_TABLE', response.data)
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    getProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/product/${payload}`)
            .then((response) => {
                commit('SET_PRODUCT_DATA', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getLowestPrice({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`https://api.data-slymanbros.slyman.com/api/price/lowest-price`, {
                params: payload
            })
            .then((response) => {
                commit('SET_PRODUCT_LOWEST_PRICE', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getSearchProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/product/${payload}/show`)
            .then((response) => {
                if(response.data.data == null){
                    commit('SET_SEARCH_PRODUCT_DATA', null)
                    commit('SET_SEARCH_PRODUCT_DETAIL', null)
                }else{
                    commit('SET_SEARCH_PRODUCT_DATA', response.data.data.length != 0 ? response.data.data:null)
                    commit('SET_SEARCH_PRODUCT_DETAIL', response.data.data.length != 0 ? response.data.product:null)
                }
                
                // commit('SET_PRICE_HISTORY_TABLE', response.data.data.length != 0 ? response.data.data.detail_order:null)
                // console.log(response.data.data.length)
                // commit('SET_PRODUCT_DATA', response.data.product)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getPriceHistory({commit, state}, payload){
        return new Promise((resolve, reject) =>{
            const {page, perpage, ...rest} =  payload
            this.$axios.post(`/product/price-history`, rest, {params: {page, perpage}}).then((response) =>{
                commit('SET_PRICE_HISTORY_TABLE', response.data.data)
                resolve(response.data.data)
            })
        })
    },
    getProductOrder({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/product/order`, {params: {product_id : payload.product_id, search_type: payload.search_type}})
            .then((response) => {
                commit('SET_PRODUCT_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getProductStockOrder({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get('order/product-stock', {
                params: payload
            })
            .then(response => {
                commit('SET_PRODUCT_STOCK_ORDER', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    postProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/product', payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                // console.log(error.response)
                commit('SET_PRODUCT_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    putProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/product/${payload.product_id}`, payload).then((response) => {
                commit('SET_PRODUCT_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_PRODUCT_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    deleteProduct({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/product/${payload}`)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getProductSetting({ commit }) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/product/setting`)
            .then((response) => {
                commit('SET_PRODUCT_SETTINGS', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    postProductSetting({ state, commit }, payload) {
        return this.$axios.post('/product/setting', state.data)
        .then((response) => {
            commit('SET_PRODUCT_ERRORS', null)
            return response.data
        })
        .catch(error => {
            commit('SET_PRODUCT_ERRORS', error.response.data.errors)
            new Error(error)
        })
    },

    //SEARCH PRODUCT
    autoCompleteProduct({ commit }, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.get(`product`, {params: payload})
            .then((response) =>{
                commit('SET_PRODUCT_DATA', response.data)
                resolve(response.data)
            })
        })
    },
    searchProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/product/${payload}`)
            .then((response) => {
                resolve(response.data.data)
            })
        })
    },
    submitProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/product/search', payload).then((response) => {
                resolve(response.data)
            })
        })
    },
    getListProduct({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/v2/product/list?model=${payload}`)
            .then((response) => {
                resolve(response.data)
                commit('SET_LIST_PRODUCT', response.data)
            })
        })
    },
}