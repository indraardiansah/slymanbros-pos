export const state = () => ({
    data:{
        id: null,
        name: null
    },
    provinces: []
})

export const mutations = {
    SET_PROVINCE_DATA(state, payload) {
        state.data = payload
    },
    SET_PROVINCE_DATA_DEFAULT(state, payload) {
        state.data = {
            id: null,
            name: null
        }
    },
    SET_STATE_DATA(state, payload) {
        state.provinces = payload
    }
}

export const actions = {
    getProvinces({ state, commit }, payload) {
        return this.$axios.get(`/state`)
        .then(response => {
            commit('SET_PROVINCE_DATA', response.data.data)
            commit('SET_STATE_DATA', response.data.data)
            return response.data.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
}