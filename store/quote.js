export const state = () => ({
    quoteInvalid: false,
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    out_of_state: null,
    quote_status_selected: null,
    page: 1,
    quote: null,
    detail_quote: [],
    urlPDF: '',
    errors: null,
    tablePosition: null
})

export const mutations = {
    SET_QUOTES_ITEM(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_QUOTES_ITEM_DEFAULT(state, payload){
        state.items = []
    },
    SET_QUOTES_STATUS_SELECTED(state, payload) {
        state.quote_status_selected = payload
    },
    SET_QUOTE_PAGE(state, payload) {
        state.page = payload
    },
    SET_QUOTE(state, payload) {
        state.quote = payload
    },
    SET_DETAIL_QUOTE(state, payload) {
        state.detail_quote = payload
    },
    SET_NEW_PRODUCT(state, payload) {
        state.detail_quote.push(payload)
    },
    SET_QUOTE_SELECTED(state, payload){
        state.selected = payload
    },
    CLEAR_QUOTE(state) {
        state.quote = null
        state.detail_quote = []
    },
    SET_URL_PDF(state, payload) {
        state.urlPDF = payload
    },
    SET_QUOTE_IS_OUT_OF_STATE(state, payload) {
        state.out_of_state = payload
    },
    SET_QUOTE_ERRORS(state, payload){
        state.errors = payload
    },
    SET_TABLE_POSITION(state, payload){
        state.tablePosition = payload
    },
    SET_QUOTE_INVALID(state, payload){
        state.quoteInvalid = payload
    }
}

export const actions = {
    getQuotes({ commit, state}, payload) {
        let page = payload.page ? payload.page : state.page
        let search = payload.search == null ? '' : payload.search
        let perpage = typeof payload.perpage != 'undefined' ? payload.perpage:''
        return new Promise((resolve, reject) => {
            this.$axios.get(`/quote-pending?page=${page}&search=${search}&type=${payload.type}&perpage=${perpage}`)
            .then(response => {
                commit('SET_QUOTES_ITEM', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    createQuote({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`quote-pending`, payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch((error) => {
                alert(error.response)
            })
        })
    },
    getQuote({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`quote-pending/${payload}/edit`)
            .then((response) => {
                let quote = Object.assign({
                    balance_due: response.data.balance_due,
                    change_due: response.data.change_due
                }, response.data.quote)
                commit('SET_QUOTE_INVALID', response.data.invalid)
                commit('SET_QUOTE', quote)
                commit('SET_DETAIL_QUOTE', response.data.detail_quote)
                resolve(response.data)
            })
        })
    },
    updateQuote({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`quote-pending/${payload}`, { quote: state.quote, detail_quote: state.detail_quote, out_of_state: state.out_of_state })
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    updateAddressQuote({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`quote-pending/${payload}`, { quote: state.quote, detail_quote: state.detail_quote, out_of_state: state.out_of_state, update_address: true })
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    changeExpiredDate({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('quote-pending/change-expired-date', payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    changeStatus({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`quote-pending/${state.quote.quote_no}/status`, payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    getQuoteStatus({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`quote-pending/${payload}/check-status`)
            .then((response) => {
                commit('SET_QUOTES_STATUS_SELECTED', response.data.quote_status)
                resolve(response.data)
            })
        })
    },
    deleteQuote({ state }) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`quote-pending/${state.quote.quote_no}`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    addQuoteNotes({commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('quote-pending/add-notes', {
                notes: payload.notes,
                quote_no: state.quote.quote_no,
                user_id: payload.user_id
            })
            .then((response) => {
                commit('SET_QUOTE_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_QUOTE_ERRORS', error.response.data.errors)
            })
        })
    },
    deleteQuoteNote({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/quote-pending/notes/${payload.id}`).then((response) => {
                resolve(response.data)
            })
        })
    },
    updateQuoteNote({commit, state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/quote-pending/notes/${payload.id}`, payload, {
                // data: state.data
            }).then((response) => {
                commit('SET_QUOTE_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_QUOTE_ERRORS', error.response.data.errors.note[0])
            })
        })
    },
    sendEmail({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('quote-pending/email', payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    getPdfWithoutSpec({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/quote-pending/get-pdf`, {
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
                params: {...payload}
            })
            .then((response) => {
                commit('SET_URL_PDF', response.data.data)
                resolve(response.data)
            })
        })
    },
    getPdfWithSpec({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/quote-pending/get-pdf-with-spec`, {
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
                params: {...payload}
            })
            .then((response) => {
                commit('SET_URL_PDF', response.data.data)
                resolve(response.data)
            })
        })
    },
    getPdfWithoutModelNumber({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/quote-pending/get-pdf-without-model-number`, {
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
                params: {...payload}
            })
            .then((response) => {
                commit('SET_URL_PDF', response.data.data)
                resolve(response.data)
            })
        })
    },
    getPdfWithoutPrice({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/quote-pending/get-pdf-without-price`, {
                headers: {
                    'Accept': 'application/pdf',
                    "Access-Control-Allow-Origin": "*"
                },
                mode: 'no-cors',
                responseType: 'text',
                params: {...payload}
            })
            .then((response) => {
                commit('SET_URL_PDF', response.data.data)
                resolve(response.data)
            })
        })
    },

    //DASHBOARD
    findQuotes({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/find-quote?quote_no=${payload.quote_no}`)
            .then((response) => {
                commit('SET_QUOTES_ITEM', response.data)
                resolve(response.data)
            })
        })
    },

    //Send Stripe Invoice
    sendStripeInvoice({}, payload){
        const {id, ...rest} = payload
        const response = this.$axios.get(`blockchyp/invoice/${id}`, {params: {...rest}})
        return response
    },
    async getTerminal({}, payload){
        const response = await this.$axios.get('blockchyp/get-terminal', {params: payload})
        return response
    },
    createPaymentIntent({}, payload){
        const response = this.$axios.post('/blockchyp/create-payment-intent', payload)
        return response
    },
    saveQuotePendingCustomNotification({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.post(`quote-pending/custom-notification/store/${payload.id}`, payload.form)
            .then((response) => {
                dispatch(`getQuote`, payload.idQuote);
                resolve(response.data)
            }).catch((error) => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    updateQuotePendingCustomNotification({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.put(`quote-pending/custom-notification/update/${payload.id}`, payload.form)
            .then((response) => {
                dispatch(`getQuote`, payload.idQuote);
                resolve(response.data)
            }).catch((error) => {
                new Error(error.response)
                reject(error.response)
            })
        })
    }

}
