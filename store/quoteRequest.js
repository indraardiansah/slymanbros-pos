export const state = () => ({
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    quote: null,
    detail_quote: [],
    urlPDF: '',
    errors: []
})

export const mutations = {
    SET_QUOTES_ITEM(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_QUOTES_ITEM_DEFAULT(state, payload){
        state.items = []
    },
    SET_QUOTE_PAGE(state, payload) {
        state.page = payload
    },
    SET_QUOTE(state, payload) {
        state.quote = payload
    },
    SET_DETAIL_QUOTE(state, payload) {
        state.detail_quote = payload
    },
    SET_NEW_PRODUCT(state, payload) {
        state.detail_quote.push(payload)
    },
    SET_QUOTE_SELECTED(state, payload){
        state.selected = payload
    },
    CLEAR_QUOTE(state) {
        state.quote = null
        state.detail_quote = []
    },
    SET_URL_PDF(state, payload) {
        state.urlPDF = payload
    },
    SET_QUOTE_ERRORS(state, payload){
        state.errors = payload
    }
}

export const actions = {
    getRequestQuotes({ commit, state}, payload) {
        let page = payload.page ? payload.page : state.page
        let search = payload.search == null ? '' : payload.search
        let type = payload.type ? payload.type:''
        let perpage = payload.perpage  ? payload.perpage: 10
        return new Promise((resolve, reject) => {
            this.$axios.get(`/quote-request?page=${page}&search=${search}&type=${type}&perpage=${perpage}`)
            .then(response => {
                commit('SET_QUOTES_ITEM', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getRequestQuote({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/quote-request/${payload}`)
            .then((response) => {
                commit('SET_QUOTE', response.data.data)
                commit('SET_DETAIL_QUOTE', response.data.data.detail)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    assignSalesPerson({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/quote-request', {
                id: payload.id,
                sales_person: payload.sales_person,
                tax_exempted: payload.tax_exempted
            }).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_QUOTE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    }
}