export const state = () => ({
    data: [],
    fields: [
        {
            key: 'name',
            label: 'Name'
        },
        {
            key: 'amount',
            label: 'Amount'
        },
        {
            key: 'action',
            label: 'Action'
        }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    perpage: 5,
    errors: null,
    newRebates: false,
    rebateSelected: []
})

export const mutations = {
    SET_REBATE_DATA(state, payload) {
        state.data = payload
    },
    SET_REBATE_DATA_DEFAULT(state) {
        state.data = []
        // state.data = {
        //     title: '',
        //     keyword: '',
        //     description: '',
        //     url: ''
        // }
    },
    SET_REBATE_TABLE(state, payload) {
        state.items = payload.data
        if (payload.meta) {
            state.meta = payload.meta
        }
    },
    SET_REBATE_TABLE_DEFAULT(state) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_REBATE_PAGE(state, payload) {
        state.page = payload
    },
    SET_REBATE_PERPAGE(state, payload) {
        state.perpage = payload
    },
    SET_REBATE_ERRORS(state, payload){
        state.errors = payload
    },
    SET_NEW_REBATES(state, payload){
        state.newRebates = payload
    },
    SET_REBATE_SELECTED(state, payload){
        state.rebateSelected = payload
    }
}

export const actions = {
    getRebateData({ commit, state }, payload) {
        // let type_data = ''
        // let perpage = 10
        let order_no = ''
        let type = ''
        if(payload){
            // type_data = payload.type ? payload.type:''
            // perpage = payload.perpage ? payload.perpage:10
            order_no = typeof payload.order_no != 'undefined' ? payload.order_no:''
            type = typeof payload.type != 'undefined' ? payload.type:payload
        }
        console.log(type)
        return new Promise((resolve, reject) => {
            this.$axios.get(`/instant-rebate?page=${state.page}&perpage=${state.perpage}&type=${type}&order_no=${order_no}`)
            .then(response => {
                if(type == 'all'){
                    commit('SET_REBATE_TABLE', response.data)
                } else {
                    commit('SET_REBATE_TABLE', response.data)
                }
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getRebate({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/instant-rebate/${payload}/edit`)
            .then((response) => {
                commit('SET_REBATE_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    getCustomRebate({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/instant-rebate/custom/${payload}/edit`)
            .then((response) => {
                commit('SET_REBATE_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    saveRebate({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/instant-rebate', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_REBATE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    updateRebate({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/instant-rebate/${payload.id}`, payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_REBATE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    updateCustomRebate({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/instant-rebate/custom/${payload.id}`, payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_REBATE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    destroyRebate({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/instant-rebate/${payload}`).then((response) => {
                dispatch('getRebateData', '').then(() => {
                    resolve(response.data)
                })
            })
        })
    },
    saveCustomRebate({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/instant-rebate/custom', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_REBATE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
}