export const state = () => ({
    validate: []
})

export const mutations = {
    SET_ERRORS(state, payload) {
        state.validate = payload
    },
    CLEAR_ERROR(state) {
        state.validate = []
    }
}

export const actions = {
    sendResetPassword({ commit }, payload) {
        commit('CLEAR_ERROR')
        return new Promise((resolve, reject) => {
            this.$axios.post(`user/forget-password`, {
                email: payload
            }).then(response => {
                resolve(response.data)
            })
            .catch(error => {
                if (error.response.status == 422) {
                    commit('SET_ERRORS', error.response.data.errors)
                }
                new Error(error)
            })
        })
    },
    changePassword({ commit }, payload) {
        commit('CLEAR_ERROR')
        return new Promise((resolve, reject) => {
            this.$axios.post(`user/reset-password`, payload)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                if (error.response.status == 422) {
                    commit('SET_ERRORS', error.response.data.errors)
                }
                new Error(error)
            })
        })
    }
}