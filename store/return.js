export const state = () => ({
    detailData: []
})

export const mutations = {
    SET_ORDER_DETAIL_DATA(state, payload) {
        state.detailData = payload
    },
}

export const actions = {
    getReturnedData({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/order/return/${payload}`)
            .then((response) => {
                commit('SET_ORDER_DETAIL_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    submitReturned({ state }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/order/return', {
                returned: state.detailData,
                ...payload
            })
            .then((response) => {
                resolve(response.data)
            })
        })
    }
}