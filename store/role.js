export const state = () => ({
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    page: 1,
    role: [],
    navigations: []
})

export const mutations = {
    SET_ROLE_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_ROLE_DATA(state, payload) {
        state.role = payload
    },
    SET_PAGE(state, payload) {
        state.page = payload
    },
    SET_ROLE_MENU(state, payload) {
        state.navigations = payload
    }
}

export const actions = {
    getRoles({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`role`)
            .then(response => {
                commit('SET_ROLE_TABLE', response.data)
                resolve()
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getRole({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`role/${payload}/edit`)
            .then((response) => {
                commit('SET_ROLE_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    createRoles({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('role', payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    updateRole({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`role/${payload.id}`, payload)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    deleteRole({state}, payload){
        return new Promise((resolve, reject) =>{
            this.$axios.delete(`role/delete/${payload}`)
            .then(response =>{
                resolve(response.data)
            })
            .catch(error =>{
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getMenu({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/role/get-user-menu/${payload}`)
            .then((response) => {
                commit('SET_ROLE_MENU', response.data.data)
                resolve(response.data)
            })
        })
    },
}