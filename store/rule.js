export const state = () => ({
    ruleData: [],
})

export const mutations = {
    SET_RULE_DATA(state, payload){
        state.ruleData.push(payload)
    },
    CLEAR_RULE(state, payload){
        state.ruleData = []
    },
    REMOVE_RULE(state, payload){
        state.ruleData.splice(payload, 1)
    }
}

export const getters = {
    ruleData: (state) =>  state.ruleData
}

export const actions = {
    getRule({commit}, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post('rule/show', payload)
            .then(response => {
                commit('SET_RULE_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
                reject(error.response)
            })
        })
    },
    updateRule({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            return this.$axios.put(`rule/update/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error.response)
                reject(error.response)
            })
        })
    }
}
