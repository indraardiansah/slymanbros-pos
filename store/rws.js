export const state = () => ({
    brands: [],
    progressData: [],
    log: []
})

export const mutations = {
    SET_BRAND_DATA(state, payload) {
        state.brands = payload
    },
    SET_PROGRESS_DATA(state, payload){
        state.progressData = payload
    },
    SET_RWS_LOG(state, payload) {
        state.log = payload
    },
    UPDATE_PROGRESS_DATA(state, payload){
        let index = state.progressData.findIndex(function(item) {
            return item.brand_id == payload.brand
        })
        if(index != -1){
            state.progressData[index].status = payload.status
        }
    }
}

export const actions = {
    getBrandRWS({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`v2/brand`)
            .then((response) => {
                commit('SET_BRAND_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    getRWSseed({commit}, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`product/rws-seed`, {
                params: {
                    brands : payload.brand,
                    mode: payload.mode == '1' ? 'all' : 'price_only'
                }
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    getProgress({commit}, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('product/rws/progress')
            .then(response => {
                commit('SET_PROGRESS_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    deleteUpdatedProgress({commit}, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get('product/delete/rws-progress')
            .then(response => {
                // commit('SET_PROGRESS_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    },
    getRWSLog({commit}, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`product/rws/log`)
            .then((response) => {
                commit('SET_RWS_LOG', response.data)
                resolve(response.data)
            })
            .catch(error => {
                new Error(error)
            })
        })
    }
}