export const state = () => ({
    fieldsSales: [
        'sales_person',
        {
            key: 'revenue', label: 'Qualified Commission Revenue'
        },
        {
            key: 'total_profit_margin', label: 'Profit'
        },
        {
            key: 'profit_margin_percent', label: 'Total Profit'
        },
        {
            key: 'total_warranty', label: 'Total Warranty Revenue'
        },

    ],
    fields: [
        // {
        //     key: 'createdDate',
        //     label: 'Created Date'
        // },
        {
            key: 'order',
            label: 'Order #'
        },
        {
            key: 'customer',
            label: 'Customer'
        },
        {
            key: 'revenue',
            label: 'Qualified Commission Revenue'
        },
        {
            key: 'paidDate',
            label: 'Paid In Full Date'
        },
        {
            key: 'profit_margin',
            label: 'Total Profit'
        },
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
})

export const mutations = {
    SET_SALE_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_SALE_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            // {
            //     key: 'createdDate',
            //     label: 'Created Date'
            // },
            {
                key: 'order',
                label: 'Order #'
            },
            {
                key: 'customer',
                label: 'Customer'
            },
            {
                key: 'revenue',
                label: 'Qualified Commission Revenue'
            },
            {
                key: 'paidDate',
                label: 'Paid In Full Date'
            },
            {
                key: 'profit_margin',
                label: 'Total Profit'
            },
        ]
    },
    SET_SALE_FIELDS_UNDER(state, payload) {
        state.fields = [
            {
                key: 'createdDate',
                label: 'Created Date'
            },
            {
                key: 'order',
                label: 'Order #'
            },
            {
                key: 'customer',
                label: 'Customer'
            },
            {
                key: 'revenue',
                label: 'Total'
            },
            {
                key: 'profit_margin',
                label: 'Total Profit'
            },
            {
                key: 'paidDate',
                label: 'Paid In Full Date'
            },
        ]
    },
    SET_SALE_FIELD_SALES_UNDER(state, payload) {
        state.fieldsSales = [
            'sales_person',
            {
                key: 'revenue', label: 'Total'
            },
            {
                key: 'total_profit_margin', label: 'Profit'
            },
            {
                key: 'profit_margin_percent', label: 'Total Profit'
            },
        ]
    },
    SET_SALE_FIELD_SALES_DEFAULT(state, payload) {
        state.fieldsSales = [
            'sales_person',
            {
                key: 'revenue', label: 'Qualified Commission Revenue'
            },
            {
                key: 'total_profit_margin', label: 'Profit'
            },
            {
                key: 'profit_margin_percent', label: 'Total Profit'
            },
            {
                key: 'total_warranty', label: 'Total Warranty Revenue'
            },
        ];
    },
    SET_SALE_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_SALE_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_SALE_PAGE(state, payload) {
        state.page = payload
    },
}

export const actions = {
    getSales({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`report/my-sales`, {params: payload})
            .then(response => {
                commit('SET_SALE_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    }
}
