export const state = () => ({
    data: {
        // firstName: null,
        // lastName: null,
        // company: null,
        // phone_number: null,
        // email: null,
        // billingAddress: null,
        // shippingDefault: null,
        // shippingAddress: [],
        // phoneNumber: [],
        // tax_exempt: false
    },
    fields: [
        // {
        //     key: 'id',
        //     label: 'SalesPerson Code'
        // },
        // {
        //     key: 'name',
        //     label: 'Name'
        // },
        // {
        //     key: 'phoneNumber',
        //     label: 'Phone Number'
        // },
        // {
        //     key: 'billingAddress',
        //     label: 'Billing Address	'
        // },
        // {
        //     key: 'shippingAddress',
        //     label: 'Delivery Address (Default)'
        // },
        // {
        //     key: 'email',
        //     label: 'Email'
        // },
    ],
    store: null,
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    errors: null
})

export const mutations = {
    SET_SALES_PERSON_DATA(state, payload) {
        state.data = payload
    },
    SET_SALES_PERSON_DATA_DEFAULT(state, payload) {
        // state.data = {
        //     firstName: null,
        //     lastName: null,
        //     company: null,
        //     email: null,
        //     billingAddress: null,
        //     shippingAddress: [],
        //     phoneNumber: [],
        //     tax_exempt: false
        // }
    },
    SET_SALES_PERSON_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_SALES_PERSON_FIELDS_DEFAULT(state, payload) {
        // state.fields = [
        //     {
        //         key: 'id',
        //         label: 'SalesPerson Code'
        //     },
        //     {
        //         key: 'name',
        //         label: 'Name'
        //     },
        //     {
        //         key: 'phoneNumber',
        //         label: 'Phone Number'
        //     },
        //     {
        //         key: 'billingAddress',
        //         label: 'Billing Address	'
        //     },
        //     {
        //         key: 'shippingAddress',
        //         label: 'Delivery Address (Default)'
        //     },
        //     {
        //         key: 'email',
        //         label: 'Email'
        //     },
        // ]
    },
    SET_SALES_PERSON_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_SALES_PERSON_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_SALES_PERSON_ITEMS(state, payload) {
        state.items = payload.data
    },
    SET_SALES_PERSON_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_SALES_PERSON_PAGE(state, payload) {
        state.page = payload
    },
    SET_SALES_PERSON_ERRORS(state, payload) {
        state.errors = payload
    },
    SET_SALES_PERSON_STORE(state, payload) {
        state.store = payload
    }
}

export const actions = {
    getSalesPersons({ commit }, payload) {
        let type = typeof payload != 'undefined' && typeof payload.type != 'undefined' ? payload.type:''
        let store_id = typeof payload != 'undefined' && typeof payload.store_id != 'undefined' ? payload.store_id:''
        let isNotCommit = typeof payload != 'undefined' && typeof payload.isNotCommit != 'undefined' ? payload.isNotCommit : false
        return new Promise((resolve, reject) => {
            this.$axios.get(`user/sales-person?type=${type}&store_id=${store_id}`)
            .then(response => {
                if(!isNotCommit){
                    commit('SET_SALES_PERSON_ITEMS', response.data)
                }
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getSalesPersonStore({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get('user/store')
            .then(response => {
                commit('SET_SALES_PERSON_STORE', response.data)
                payload ? commit('SET_SALES_PERSON_SELECTED', payload):commit('SET_SALES_PERSON_SELECTED', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    }
}