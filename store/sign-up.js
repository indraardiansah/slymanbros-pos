export const state = () => ({
    items: [],
    meta: {
        total: null,
        per_page: null
    },
    errors: []
})

export const mutations = {
    SET_STORE_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_STORE_ITEMS(state, payload) {
        state.items = payload.data
    },
    SET_USER_STORE(state, payload) {
        state.user_store = payload
    },
    SET_ERRORS(state, payload) {
        state.errors = payload
    }
}

export const actions = {
    getStores({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/invited-stores`, {
                params: {
                    page: state.page,
                    type: payload || ''
                }
            })
            .then(response => {
                commit(payload == 'all' ? 'SET_STORE_ITEMS' : 'SET_STORE_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getEmail({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/invited-employee/${payload}`)
            .then((response) => {
                resolve(response.data)
            })
        })
    },
    registerInvited({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/invited-employee', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                if (error.response.status == 422) {
                    commit('SET_ERRORS', error.response.data.errors)
                }
                new Error(error)
            })
        })
    },
    validateLogin({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('v-login', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                resolve(error.response)
            })
        })
    },
    socialAuth({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`social-login/${payload}`)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                resolve(error.response)
            })
        })
    },
    requestPINLogin({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('login/request-pin', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                resolve(error.response)
            })
        })
    },
    requestLoginWithPin({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('login/pin', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                resolve(error.response)
            })
        })
    }
}