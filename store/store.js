export const state = () => ({
    data:{
        store_code: null,
        store_name: null,
        store_phone_number: null,
        store_fax: null,
        store_address:null,
        store_tax_percentage:null,
        store_postal_code:null,
    },
    sales_person: [],
    fields: [
        {
            key: 'store_code',
            label: 'Location Code',
            thStyle:{
                width: '130px'
            }
        },
        {
            key: 'store_name',
            label: 'Location Name',
            thStyle:{
                width:'300px'
            }
        },
        {
            key: 'slug',
            label: 'Slug',
        },
        {
            key: 'store_phone_number',
            label: 'Location Phone Number',
        },
        {
            key: 'store_fax',
            label: 'Location Fax'
        },
        {
            key: 'store_address',
            label: 'Location Address',
            thStyle:{
                width: '300px !important'
            }
        },
        {
            key:'manager',
            label:'Manager'
        }
        // {
        //     key: 'store_tax_percentage',
        //     label: 'Store Tax Percentage'
        // },
        // {
        //     key: 'action',
        //     label: 'Action'
        // }
    ],
    items: [],
    meta: {
        total: null,
        per_page: null
    },
    selected: null,
    page: 1,
    errors: null
})

export const mutations = {
    SET_STORE_DATA(state, payload) {
        state.data = payload
    },
    SET_STORE_MANAGERS(state, payload) {
        state.sales_person = payload
    },
    SET_STORE_DATA_DEFAULT(state, payload) {
        state.data = {
            store_code: null,
            store_name: null,
            store_phone_number: null,
            store_fax: null,
            store_address: null,
            store_tax_percentage: null,
            store_postal_code: null,
        }
    },
    SET_STORE_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_STORE_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'store_code',
                label: 'Location Code'
            },
            {
                key: 'store_name',
                label: 'Location Name'
            },
            {
                key: 'store_phone_number',
                label: 'Location Phone Number'
            },
            {
                key: 'store_fax',
                label: 'Location Fax'
            },
            {
                key: 'store_address',
                label: 'Location Address'
            },
            {
                key: 'store_tax_percentage',
                label: 'Location Tax Percentage'
            }
        ]
    },
    SET_STORE_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_STORE_ITEMS(state, payload) {
        state.items = payload.data
    },
    SET_STORE_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_STORE_PAGE(state, payload) {
        state.page = payload
    },
    SET_STORE_ERRORS(state, payload){
        state.errors = payload
    }
}

export const actions = {
    getStores({ state, commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/store`, {
                params: {
                    page: state.page,
                    type: payload == null ? '' : payload.type,
                    perpage: payload == null ? '' : payload.perpage
                }
            })
            .then(response => {
                commit(payload == 'all' ? 'SET_STORE_ITEMS' : 'SET_STORE_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getStore({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`store/${payload}`)
            .then(response => {
                commit('SET_STORE_DATA', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getManagers({ commit }, payload) {
        return new Promise((resolve, reject) => {
            let manager = typeof payload != 'undefined' ? payload:''
            return this.$axios.get(`user/sales-person?manager=${manager}`)
            .then(response => {
                commit('SET_STORE_MANAGERS', response.data.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    assignManager({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('store/edit', payload)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    postStore({ commit }, payload){
        return new Promise((resolve, reject) => {
            this.$axios.post('/store', payload)
            .then((response) => {
                commit('SET_STORE_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_STORE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    selectWarehouse({ state }, payload) {
        return this.$axios.put(`store/${payload.store_id}`, state.data)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    updateRequestLocationDefault({ state }, payload) {
        return this.$axios.put(`store/update-request-default/${payload.store_id}`, payload)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    },
    updateStore({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`store/update/${payload.store_id}`, payload)
            .then(response => {
                commit('SET_STORE_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_STORE_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    deleteStore({ commit }, payload){
        return this.$axios.delete(`/store/${payload}`)
        .then((response) => {
            return response.data
        })
    }
}