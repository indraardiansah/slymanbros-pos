export const actions = {
    async getConnectionToken({}, payload){
        const response = await this.$axios.post('/stripe/connection-token', payload)
        return response
    },
    createPaymentIntent({}, payload){
        const response = this.$axios.post('/stripe/create-payment-intent', payload)
        return response
    },
    capturePaymentIntent({}, payload){
        const response = this.$axios.post('/stripe/capture-payment-intent', payload)
        return response
    },
    cancelPaymentIntent({}, payload){
        const response = this.$axios.post('/stripe/cancel-payment-intent', payload)
        return response
    },
    confirmPaymentIntent({}, payload){
        const response = this.$axios.post('/stripe/confirm-payment-intent', payload)
        return response
    }
}