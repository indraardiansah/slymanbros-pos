export const state = () => ({
    terminal: {
        data: [],
        meta: null
    },
    terminal_selected: null
})
export const mutations = {
    SET_TERMINAL_DATA(state, payload){
        state.terminal = payload
    },
    SET_TERMINAL_SELECTED(state, payload){
        state.terminal_selected = payload
    }
}

export const actions = {
    async getTerminals({commit}, payload){
        const response = await this.$axios.get('terminal', {params: payload})
        const {data, ...meta} = response.data
        let terminal = {
            data,
            meta: meta
        }
        commit('SET_TERMINAL_DATA', terminal)
        return response
    },
    async getTerminal({commit}, payload){
        const response = await this.$axios.get(`terminal/${payload}`)
        commit('SET_TERMINAL_SELECTED', response.data)
        return response
    },
    async postTerminal({}, payload){
        const response = await this.$axios.post('terminal', payload)
        return response
    },
    async putTerminal({}, payload){
        const response = await this.$axios.put(`terminal/${payload.id}`, payload)
        return response
    },
    async deleteTerminal({}, payload){
        const response = await this.$axios.delete(`terminal/${payload}`)
        return response
    }
}