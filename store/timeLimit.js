export const state = () => ({
    timeLimits: [],
    fields: [
        {
            key: 'hour',
            label: 'Hour',
            thStyle: { width: '200px' }
        },
        {
            key: 'minute',
            label: 'Minute',
        }
    ],
    timeLimitSelected: null,
})

export const mutations = {
    SET_TIMELIMITS(state, payload) {
        state.timeLimits = payload
    },
    SET_TIMELIMITS_DEFAULT(state) {
        state.timeLimits = []
    },
    SET_DRIVER_SELECTED(state, payload) {
        state.timeLimitSelected = payload
    }
}

export const actions = {
    getTimeLimits({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/delivery-route/time-limit`)
                .then(response => {
                    commit('SET_TIMELIMITS', response.data)
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    putTimeLimit({ }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/delivery-route/time-limit/update/${payload.id}`, { hour: payload.hour, minute: payload.minute })
                .then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error.response)
                })
        })
    },
}
