export const state = () => ({
    data: {
        truck_id: null,
        truck_name: null
    },
    fields: [
        {
            key: 'truck_name',
            label: 'Truck Leader'
        },
        {
            key:'truck_is_active',
            label:'Status'
        },
        {
            key: 'action',
            label: 'Action'
        }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: null,
    page: 1,
    errors: null
})

export const mutations = {
    SET_TRUCK_DATA(state, payload) {
        state.data = payload
    },
    SET_TRUCK_DATA_DEFAULT(state, payload) {
        state.data = {
            truck_id: null,
            truck_name: null
        }
    },
    SET_TRUCK_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_TRUCK_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_TRUCK_PAGE(state, payload) {
        state.page = payload
    },
    SET_TRUCK_ERRORS(state, payload){
        state.errors = payload
    }
}

export const actions = {
    getTrucks({ commit, state }, payload) {
        let type_data = ''
        let perpage = 10
        if(payload){
            type_data = payload.type ? payload.type:''
            perpage = payload.perpage ? payload.perpage:10
        }
        return new Promise((resolve, reject) => {
            this.$axios.get(`/truck?page=${state.page}&type=${type_data}&perpage=${perpage}`)
            .then(response => {
                commit('SET_TRUCK_TABLE', response.data)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    saveTruck({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post('/truck', payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                // console.log(error.response)
                commit('SET_TRUCK_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    getTruck({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/truck/${payload}/edit`)
            .then((response) => {
                commit('SET_TRUCK_DATA', response.data.data)
                resolve(response.data)
            })
        })
    },
    updateTruck({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/truck/${payload.truck_id}`, payload).then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                commit('SET_TRUCK_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    destroyTruck({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/truck/${payload}`).then((response) => {
                dispatch('getTrucks').then(() => {
                    resolve(response.data)
                })
            })
        })
    }
}