export const state = () => ({
    upc_list: {
        data: [],
        meta: null
    }
})

export const mutations = {
    SET_UPC_LIST(state, payload){
        const {data, ...rest} = payload
        state.upc_list.data = data
        state.upc_list.meta = rest
    }
}

export const actions = {
    getUpcCodes({commit}, payload){
        return new Promise((resolve, reject) => {
            return this.$axios.get(`upc`, { params: payload})
            .then(response => {
                commit('SET_UPC_LIST', response.data)
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    },
    updateUpcCode({}, payload){
        const {id, ...rest} = payload
        return new Promise((resolve, reject) => {
            return this.$axios.put(`upc/update/${id}`, rest)
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                reject(error.response)
            })
        })
    }
}