export const state = () => ({
    user_mode : null
})

export const mutations = {
    SET_USER_MODE(state, payload) {
        state.user_mode = payload
    },
}

export const actions = {
    
}