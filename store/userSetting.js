export const state = () => ({
    userSettingNotifications: [],
})

export const mutations = {
    SET_USER_SETTING_NOTIFICATIONS(state, payload) {
        state.userSettingNotifications = payload;
    },
}

export const actions = {
    getSubcriptions({ commit }) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/user/setting/show-subscription`)
            .then(response => {
                commit('SET_USER_SETTING_NOTIFICATIONS', response.data);
                resolve(response.data)
            })
            .catch(error => {
                reject(error);
                new Error(error)
            })
        })
    },
    updateSubcription({ dispatch }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.post(`user/setting/update-subscription`, payload)
            .then(() => {
                dispatch('getSubcriptions');
                resolve();
            })
            .catch((error) => {
                reject(error);
                new Error(error)
            })
        })
    }
}
