export const state = () => ({
    data: {
        warranty_id: null,
        warranty_code: null,
        warranty_name: null,
        warranty_price: null,
        product_from_range: null,
        product_to_range: null,
        // value: [
        //     0,
        //     500
        // ]
    },
    fields: [
        {
            key: 'warranty_code',
            label: 'Warranty Code'
        },
        {
            key: 'warranty_name',
            label: 'Warranty Name'
        },
        {
            key: 'warranty_price',
            label: 'Warranty Price'
        },
        {
            key: 'product_price_range',
            label: 'Product Price Range'
        },
        {
            key: 'action',
            label: 'Action'
        }
    ],
    items: [],
    meta: {
        total: null,
        perPage: null
    },
    selected: [],
    existing: [],
    page: 1,
    errors: null
})

export const mutations = {
    SET_WARRANTY_DATA(state, payload) {
        state.data = payload
    },
    SET_WARRANTY_DATA_DEFAULT(state, payload) {
        state.data = {
            warranty_id: null,
            warranty_code: null,
            warranty_name: null,
            warranty_price: null,
            product_from_range: null,
            product_to_range: null,
        }
    },
    SET_WARRANTY_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_WARRANTY_FIELDS_DEFAULT(state, payload) {
        state.fields = [
            {
                key: 'warranty_code',
                label: 'Warranty Code'
            },
            {
                key: 'warranty_name',
                label: 'Warranty Name'
            },
            {
                key: 'warranty_price',
                label: 'Warranty Price'
            },
            {
                key: 'product_price_range',
                label: 'Product Price Range'
            },
            {
                key: 'action',
                label: 'Action'
            }
        ]
    },
    SET_WARRANTY_TABLE(state, payload) {
        state.items = payload.data
        state.meta = payload.meta
    },
    SET_WARRANTY_TABLE_DEFAULT(state, payload) {
        state.items = []
        state.meta = {
            total: null,
            perPage: null
        }
        state.selected = null
        state.page = 1
    },
    SET_WARRANTY_ITEMS(state, payload) {
        state.items = payload.data
    },
    SET_WARRANTY_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_WARRANTY_PAGE(state, payload) {
        state.page = payload
    },
    SET_WARRANTY_ERRORS(state, payload){
        state.errors = payload
    },
    SET_EXISTING_WARRANTY(state, payload){
        state.existing = payload
    },
    SET_WARRANTY_SELECTED_DEFAULT(state, payload){
        state.selected = []
    }
}

export const actions = {
    getWarranties({ state, commit }, payload) {
        return new Promise((resolve, reject) => {
            let perpage = payload && typeof payload.perpage != 'undefined' ? payload.perpage:''
            let category_name = payload && typeof payload.category_name != 'undefined' ? payload.category_name:''
            let subcategory_name = payload && typeof payload.subcategory_name != 'undefined' ? payload.subcategory_name:''
            let detail_category_name = payload && typeof payload.detail_category_name != 'undefined' ? payload.detail_category_name:''
            let sortBy = payload && typeof payload.sortBy != 'undefined' ? payload.sortBy:''
            let sortDesc = payload && typeof payload.sortDesc != 'undefined' ? payload.sortDesc:''
            let search = payload && typeof payload.search != 'undefined' ? payload.search:''

            this.$axios.get(`/warranty`, {
                params: {
                    page: state.page,
                    perpage: perpage,
                    category_name: category_name,
                    subcategory_name: subcategory_name,
                    detail_category_name: detail_category_name,
                    sortBy: sortBy,
                    sortDesc: sortDesc,
                    search: search,
                }
            })
            .then(response => {
                commit('SET_WARRANTY_TABLE', response.data)
                resolve()
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getWarranty({ commit }, payload) {
        return new Promise((resolve, reject) => {
            return this.$axios.get(`/warranty/${payload}`)
            .then((response) => {
                // let category = response.data.data.categories
                // let categories = []
                // if (category != null && category != '') {
                //     categories = category.split('#')
                // }

                // let responseData = response.data.data
                // let data = {
                //     categories: categories,
                //     date_created: responseData.date_created,
                //     date_updated: responseData.date_updated,
                //     product_from_range: responseData.product_from_range,
                //     product_to_range: responseData.product_to_range,
                //     warranty_code: responseData.warranty_code,
                //     warranty_id: responseData.warranty_id,
                //     warranty_name: responseData.warranty_name,
                //     warranty_price: responseData.warranty_price
                // }

                commit('SET_WARRANTY_DATA', response.data.data)
                resolve(response.data.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    getWarrantyOrder({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.get(`/order/warranty`, {
                params: {
                    // price: payload
                    product_id: payload.id,
                    price: payload.price
                }
            })
            .then((response) => {
                commit('SET_WARRANTY_ITEMS', response)
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    },
    postWarranty({ commit }, payload) {
        return new Promise( (resolve, reject) => {
            this.$axios.post('/warranty', payload)
            .then((response) => {
                commit('SET_WARRANTY_ERRORS', null)
                resolve (response.data)
            })
            .catch(error => {
                commit('SET_WARRANTY_ERRORS', error.response.data.errors)
                reject(error.response)
            })
        } )
    },
    putWarranty({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.put(`/warranty/${payload.warranty_id}`, payload)
            .then((response) => {
                commit('SET_WARRANTY_ERRORS', null)
                resolve(response.data)
            })
            .catch(error => {
                // console.log(error.response)
                commit('SET_WARRANTY_ERRORS', error.response.data.errors)
                new Error(error)
            })
        })
    },
    destroyWarranty({ commit }, payload) {
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/warranty/${payload}`)
            .then((response) => {
                resolve(response.data)
            })
            .catch(error => {
                console.log(error.response)
                new Error(error)
            })
        })
    }
}