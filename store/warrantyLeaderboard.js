export const state = () => ({
    fields: [
        {
            key: 'rank',
            label: ''
        },
        {
            key: 'sales_person',
            label: 'Sales Person'
        },
        {
            key: 'gross_revenue',
            label: 'Gross Warranty Revenue'
        },
        {
            key: 'return_revenue',
            label: 'Gross Warranty Return Revenue'
        },
        {
            key: 'revenue',
            label: 'Net Warranty Revenue'
        },
    ],
    items: [],
    // meta: {
    //     total: null,
    //     perPage: null
    // },
    selected: null,
    page: 1,
})

export const mutations = {
    SET_LEADERBOARD_FIELDS(state, payload) {
        state.fields = payload
    },
    SET_LEADERBOARD_TABLE(state, payload) {
        state.items = payload.data
        // state.meta = payload.meta
    },
    SET_LEADERBOARD_SELECTED(state, payload) {
        state.selected = payload
    },
    SET_LEADERBOARD_PAGE(state, payload) {
        state.page = payload
    },
}
export const actions = {
    getLeaderboards({ commit, state }, payload) {
        let perpage = payload.perpage ? payload.perpage : 10
        return this.$axios.get(`/report/warranty-report`,{
            params: {
                page: state.page,
                start_month: payload.start_month,
                end_month: payload.end_month,
                perpage: perpage
            }
        }
        ).then(response => {
            commit('SET_LEADERBOARD_TABLE', response.data)
        })
        .catch(error => {
            console.log(error.response)
            new Error(error)
        })
    }
}