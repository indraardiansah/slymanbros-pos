export const OptionTypes = [
    {
        value: 1,
        text: 'Low'
    },
    {
        value: 2,
        text: 'High'
    },
    {
        value: 3,
        text: 'Urgent'
    },
]
