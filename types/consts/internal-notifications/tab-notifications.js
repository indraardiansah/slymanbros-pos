export const TabNotifications = [
    {
        id: 1,
        name: "All Priority",
        image_asset: `/icon/notifications/internal-notifications/priority-bell-all.svg`,
        priority: 0,
    },
    {
        id: 2,
        name: "Urgent Priority",
        image_asset: `/icon/notifications/internal-notifications/priority-bell-urgent.svg`,
        priority: 3,
    },
    {
        id: 3,
        name: "High Priority",
        image_asset: `/icon/notifications/internal-notifications/priority-bell-high.svg`,
        priority: 2,
    },
    {
        id: 4,
        name: "Low Priority",
        image_asset: `/icon/notifications/internal-notifications/priority-bell-low.svg`,
        priority: 1,
    },
]
