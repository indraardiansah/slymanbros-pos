export const __arrayToString = (array) => {
  const filteredArray = array.filter((item) => item !== '');

  if (filteredArray.length === 0) {
    return '-';
  }

  if (filteredArray.length === 1) {
    return array[0];
  }

  if (filteredArray.length > 1) {
    return array.join();
  }
}
