export const download = (url, name) => {
    const a = document.createElement('a')
    a.href = url
    a.target = "_blank";
    a.setAttribute('download', name)
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
}

export const convertBlobUrl = (response, name) => {
    const fileUrl = URL.createObjectURL(response)
    download(fileUrl, name)
}
