export const __endingNumber = (value) => {
    const last = value.charAt(value.length - 1);
    let endingNumber = 'th';

    if (parseInt(last) === 1) {
        endingNumber = 'st';
    }
    if (parseInt(last) === 2) {
        endingNumber = 'nd';
    }
    if (parseInt(last) === 3) {
        endingNumber = 'rd';
    }

    return endingNumber;
}
