export const __titleCase = (value) => {
    if (value === undefined) return;
    return value.replace(/^-*(.)|-+(.)/g, (_, c, d) => c ? c.toUpperCase() : ' ' + d.toUpperCase())
}
