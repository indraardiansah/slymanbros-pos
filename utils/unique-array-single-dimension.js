export const uniqueArraySingleDimension = (value, index, self) => {
    return self.indexOf(value) === index;
}
